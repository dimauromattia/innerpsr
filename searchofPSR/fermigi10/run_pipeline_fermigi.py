'''
This script create the SED for each source in the fit3.fits catalog of each roi and test PL, PLE, LP shapes.
For each of these functional form calculate the TS and finally it print on a file sources that have TScurv=TS_ple-TS_pl>16
'''
from fermipy import utils
utils.init_matplotlib_backend()
from fermipy.gtanalysis import GTAnalysis
import argparse
import numpy as np
from fermipy.castro import CastroData
import astropy.io.fits as pyfits


#FIRST ITERATION#
print ''
print 'FIRST ITERATION'
print ''

usage = "usage: %(prog)s [config file]"
description = "Run fermipy analysis chain."
parser = argparse.ArgumentParser(usage=usage,description=description)

parser.add_argument('--config', default = 'config.yaml')
parser.add_argument('--input', default = None)

args = parser.parse_args()

gta = GTAnalysis(args.config)
#gta = GTAnalysis.create(args.input,args.config) #if you take input files from a yaml file.

gta.setup()

gta.free_sources(free=True)
gta.print_model()
gta.optimize()
gta.fit()
gta.print_model()

gta.delete_sources(minmax_ts=[None,36],exclude=['isodiff','galdiff'])

gta.print_model()

gta.write_roi('fit0',make_plots=True,save_model_map=True)


# find sources
print ''
print 'FINDING SOURCES IN THE FIRST ITERATION sqrt_ts_threshold=6.0,min_separation=0.4'
print ''

#SAVING NAMES OF SOURCES FROM STEP 0
namesourcesfit0 = []
for t in range(len(gta.roi.sources)-2):
	namesourcesfit0.append(gta.roi.sources[t].name)
print "Sources in step0",namesourcesfit0

#COUNTING FREE PARAMETRS
parvec = np.array(gta.get_free_param_vector())
parvecTrue = parvec==True
numfreeparam = len(parvec[parvecTrue])
print "Free parameters",numfreeparam

#FIXING SHAPE IF NUMBER FREE PARAMETRS IS >30
if numfreeparam>30:
	print ""
	print "Number of free parametrs larger than threshold fixing shape"
	print ""
	for t in range(len(gta.roi.sources)-2):
		gta.free_shape(gta.roi.sources[t].name,free=False)

parvec = np.array(gta.get_free_param_vector())
parvecTrue = parvec==True
numfreeparam = len(parvec[parvecTrue])
print "Free parameters",numfreeparam

sdict = gta.find_sources(sqrt_ts_threshold=6.0,min_separation=0.4,tsmap_fitter='tsmap')

gta.print_model()

# Write the current state of the ROI model -- this will generate XML
# model files for each component as well as an output analysis
# dictionary in numpy and yaml formats
gta.write_roi('fit1find')


#REFITTING NEW SOURCES
print ""
print "Refitting new sources"
print ""
gta.free_sources(free=False)
#gta.free_source('isodiff', free=False, pars=None)
gta.print_model()

namesourcesrefit = []
for t in range(len(gta.roi.sources)-2):
	check=0
	for u in range(len(namesourcesfit0)):
		if gta.roi.sources[t].name==namesourcesfit0[u]:
			check = 1
			#print check
	if check==0:
		namesourcesrefit.append(gta.roi.sources[t].name)
print "New sources",namesourcesrefit

for t in range(len(namesourcesrefit)):
	print "Refitting new source",namesourcesrefit[t]
	gta.print_model()
	gta.free_sources(skydir=gta.roi[namesourcesrefit[t]].skydir,distance=[1.5],free=True)
	gta.free_source('isodiff', free=False, pars=None)
	gta.free_source('galdiff', free=True, pars=None)
	gta.print_model()	
	gta.fit()
	gta.write_roi('fit1',make_plots=True,save_model_map=True)
	gta.free_sources(free=False)
	gta.print_model()

print "Finished refitting procedure"

gta.free_sources(free=True)
gta.free_source('isodiff', free=False, pars=None)
gta.print_model()

#gta.fit(min_fit_quality=2,optimizer='NEWMINUIT',reoptimize=True)
#gta.fit()
# Iteratively optimize all components in the ROI
gta.optimize()
gta.delete_sources(minmax_ts=[None,10],exclude=['isodiff','galdiff'])

# Write the current state of the ROI model -- this will generate XML
# model files for each component as well as an output analysis
# dictionary in numpy and yaml formats
gta.write_roi('fit1',make_plots=True,save_model_map=True)

gta.print_model()

#SECOND ITERATION#
print ''
print 'SECOND ITERATION sqrt_ts_threshold=4.0,min_separation=0.3'
print ''

#SAVE SOURCE NAME 
namesourcesfit1 = []
for t in range(len(gta.roi.sources)-2):
	namesourcesfit1.append(gta.roi.sources[t].name)
print "Sources in step1",namesourcesfit1

#COUNTING FREE PARAMETRS
parvec = np.array(gta.get_free_param_vector())
parvecTrue = parvec==True
numfreeparam = len(parvec[parvecTrue])
print "Free parameters",numfreeparam

#FIXING SHAPE IF NUMBER FREE PARAMETRS IS >30
if numfreeparam>30:
	print ""
	print "Number of free parametrs larger than threshold fixing shape"
	print ""
	for t in range(len(gta.roi.sources)-2):
		gta.free_shape(gta.roi.sources[t].name,free=False)

#COUNTING FREE PARAMETRS
parvec = np.array(gta.get_free_param_vector())
parvecTrue = parvec==True
numfreeparam = len(parvec[parvecTrue])
print "Free parameters",numfreeparam

# find sources
sdict = gta.find_sources(sqrt_ts_threshold=4.0,min_separation=0.3,tsmap_fitter='tsmap')

gta.print_model()

#REFITTING NEW SOURCES
print ""
print "Refitting new sources"
print ""
gta.free_sources(free=False)
#gta.free_source('isodiff', free=False, pars=None)
gta.print_model()

namesourcesrefit = []
for t in range(len(gta.roi.sources)-2):
	check=0
	for u in range(len(namesourcesfit1)):
		if gta.roi.sources[t].name==namesourcesfit1[u]:
			check = 1
	if check==0:
		namesourcesrefit.append(gta.roi.sources[t].name)
print "New sources",namesourcesrefit

for t in range(len(namesourcesrefit)):
	print "Refitting new source",namesourcesrefit[t]
	gta.print_model()
	gta.free_sources(skydir=gta.roi[namesourcesrefit[t]].skydir,distance=[1.5],free=True)
	gta.free_source('isodiff', free=False, pars=None)
	gta.free_source('galdiff', free=True, pars=None)
	gta.print_model()	
	gta.fit()
	gta.write_roi('fit1',make_plots=True,save_model_map=True)
	gta.free_sources(free=False)
	gta.print_model()

gta.free_sources(free=True)
gta.free_source('isodiff', free=False, pars=None)
gta.print_model()

#ADD CHECK ON NUMBER OF PARAMETERS
#COUNTING FREE PARAMETRS
parvec = np.array(gta.get_free_param_vector())
parvecTrue = parvec==True
numfreeparam = len(parvec[parvecTrue])
print "Free parameters",numfreeparam

if numfreeparam<90:
	gta.fit()

# Write the current state of the ROI model -- this will generate XML
# model files for each component as well as an output analysis
# dictionary in numpy and yaml formats
gta.delete_sources(minmax_ts=[None,10],exclude=['isodiff','galdiff'])
gta.write_roi('fit2',make_plots=True,save_model_map=True)

gta.print_model()


#RESIDUAL MAP
#gta.residmap(prefix=u'residualfinal_pl', make_plots=True, write_fits=True)


#DELETE TOO FAINT SOURCES
gta.delete_sources(minmax_ts=[None,10],exclude=['isodiff','galdiff'])

# Write the current state of the ROI model -- this will generate XML
# model files for each component as well as an output analysis
# dictionary in numpy and yaml formats
gta.write_roi('fitcatedge',make_plots=True,save_model_map=True)

veceliminate=[]
#DELETE SOURCES IN THE EDGE
for s in range(len(gta.roi.sources)-2):
	print gta.roi.sources[s].name
	offset_edge = gta.roi[gta.roi.sources[s].name]['offset_roi_edge'][0]
	if offset_edge>-0.5:
		print "source %s in the edge of the ROI (offset_edge=%.2f) so eliminated"%(gta.roi.sources[s].name,offset_edge)
		veceliminate.append(s)
		#gta.delete_source(name=gta.roi.sources[s].name,delete_source_map=False)
	elif offset_edge<=-0.5:
		print "source %s in the edge of the ROI (offset_edge=%.2f) so kept"%(gta.roi.sources[s].name,offset_edge)

for t in range(len(veceliminate)):
	print "Eliminating %s"%gta.roi.sources[veceliminate[t]-t].name
	gta.delete_source(name=gta.roi.sources[veceliminate[t]-t].name,delete_source_map=False)
	
gta.print_model()

# Write the current state of the ROI model -- this will generate XML
# model files for each component as well as an output analysis
# dictionary in numpy and yaml formats
gta.write_roi('fit3TS10',make_plots=True,save_model_map=True)



####################
####SED RUN########
####################

gta.load_roi('fitcatedge')
gta.write_roi('fit3find',make_plots=True,save_model_map=True)
################################################
#TEST DIFFERENT SPECTRA
#gta.load_roi('fit1')
#gta.load_roi('fit2find')

vectors = []
for s in range(len(gta.roi.sources)-2):
   offset_edge = gta.roi[gta.roi.sources[s].name]['offset_roi_edge'][0]
   if gta.roi[gta.roi.sources[s].name]['ts']>16. and offset_edge<-0.5:
	vectors.append(s)
print "Sources to consider",vectors

logLsp = open(gta.workdir + '/listsources_LogL_spectra.txt', 'w')
logLsptouse = open(gta.workdir + '/listsources_LogL_spectra_touse.txt', 'w')

for s in range(len(gta.roi.sources)-2):
#s=0
   gta.load_roi('fit3find')
   check=0
   for t in range(len(vectors)):
	if s == vectors[t]:
		check = 1
   if check==1:
	print " "
	print " "
	print "Source %s" %gta.roi.sources[s].name
	print " "
	print " "

	print " "
	print "TEST POWER LAW"
	print " "
	gta.set_source_spectrum(gta.roi.sources[s].name,spectrum_type='PowerLaw',update_source=True,spectrum_pars=None)
	gta.print_model()
	gta.free_source(gta.roi.sources[s].name, free=True)
	gta.print_model()


	gta.free_sources(free=False)
	gta.free_sources(skydir=gta.roi[gta.roi.sources[0].name].skydir,distance=[2.0],free=True)

	gta.print_model()
	#gta.sed(gta.roi.sources[0].name, loge_bins=None, fix_background=False, write_fits=True, write_npy=True, optimizer=dict(optimizer='NEWTON',min_fit_quality=2))
	gta.sed(gta.roi.sources[0].name, loge_bins=None, fix_background=False, write_fits=True, write_npy=True)

	#gta.free_sources(free=False)
	#gta.free_sources(skydir=gta.roi[gta.roi.sources[s].name].skydir,distance=[0.5],free=True)
	#gta.print_model()
	#optimizeout = gta.optimize()
	#gta.print_model()

	gta.free_sources(free=False)
	gta.free_sources(skydir=gta.roi[gta.roi.sources[s].name].skydir,distance=[2.0],free=True)
	gta.print_model()
	optimizerespl = gta.fit()
	gta.print_model()
	loglikepl = optimizerespl.get('loglike')
	indexpl = gta.roi[gta.roi.sources[s].name]['params']['Index']
	print " "
	print "LogL PL",loglikepl
	print " "
	gta.write_roi('fit3find',make_plots=True,save_model_map=True)

	print " "
	print 'PLSuperExpCutoff'
	print " "
	gta.set_source_spectrum(gta.roi.sources[s].name,spectrum_type='PLSuperExpCutoff',update_source=True,spectrum_pars=None)
	gta.print_model()
	gta.free_source(gta.roi.sources[s].name, free=True)
		
	gta.free_sources(free=False)
	gta.free_sources(skydir=gta.roi[gta.roi.sources[s].name].skydir,distance=[2.0],free=True)

	gta.print_model()

	#fitresple = gta.fit()
	optimizeresple = gta.fit()
	gta.print_model()
	loglikeple = optimizeresple.get('loglike')
	indexple = gta.roi[gta.roi.sources[s].name]['params']['Index1']
	cutoff = gta.roi[gta.roi.sources[s].name]['params']['Cutoff']
	print " "
	print "LogL PLE",loglikeple,loglikeple-loglikepl
	print " "
	
	if 2.*(loglikeple-loglikepl)>9.:
		gta.write_roi('fit3find',make_plots=True,save_model_map=True)


	print " "
	print "TEST LOG PARABOLA"
	print " "
	gta.set_source_spectrum(gta.roi.sources[s].name,spectrum_type='LogParabola',update_source=True,spectrum_pars=None)
	gta.print_model()
	gta.free_source(gta.roi.sources[s].name, free=True)

	gta.free_sources(free=False)
	gta.free_sources(skydir=gta.roi[gta.roi.sources[s].name].skydir,distance=[2.0],free=True)

	gta.print_model()
	
	#fitreslp = gta.fit()
	optimizereslp = gta.fit()
	gta.print_model()
	loglikelp = optimizereslp.get('loglike')
	indexlp = gta.roi[gta.roi.sources[s].name]['params']['alpha']
	beta = gta.roi[gta.roi.sources[s].name]['params']['beta']
	print " "
	print "LogL LP",loglikelp
	print " "

	if 2.*(loglikelp-loglikepl)>9. and 2.*(loglikelp-loglikepl)>2.*(loglikeple-loglikepl)+9.:
		gta.write_roi('fit3find',make_plots=True,save_model_map=True)

	TScurvlp = 2.*(loglikelp-loglikepl)
	TScurvple = 2.*(loglikeple-loglikepl)
	
	print " "
	print "TScurv",TScurvlp,TScurvple
	print " "
	
	print " "
	print "Spectral shape of %s is %s"%(gta.roi.sources[s].name,gta.roi[gta.roi.sources[s].name]['SpectrumType'])
	print " "

	logLsp.write('%s   %.1f  %.1f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f \n'%(gta.roi.sources[s].name,gta.roi[gta.roi.sources[s].name]['glon'],gta.roi[gta.roi.sources[s].name]['glat'],loglikepl,loglikelp,loglikeple,TScurvlp,TScurvple,indexpl[0],indexpl[1],indexple[0],indexple[1],cutoff[0],cutoff[1],indexlp[0],indexlp[1],beta[0],beta[1]))
	logLsptouse.write('%.1f  %.1f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f  %.3f \n'%(gta.roi[gta.roi.sources[s].name]['glon'],gta.roi[gta.roi.sources[s].name]['glat'],loglikepl,loglikelp,loglikeple,TScurvlp,TScurvple,indexpl[0],indexpl[1],indexple[0],indexple[1],cutoff[0],cutoff[1],indexlp[0],indexlp[1],beta[0],beta[1]))

logLsp.close()
logLsptouse.close()
################################################
