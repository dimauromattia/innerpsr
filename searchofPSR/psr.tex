\documentclass[12pt,twoside,letterpaper,onecolumn,english]{article}

\usepackage[top=1.00in, bottom=1.00in, left=1.in, right=1.in]{geometry}
\usepackage{graphicx, color}
\usepackage[square,numbers,sort&compress]{natbib}   % short numerical refs
\usepackage[small,compact]{titlesec}   % to squeeze the doc a bit
\setlength{\parskip}{0in}
\setlength{\parindent}{1em}
\setlength{\parsep}{0pt}
\setlength{\headsep}{0pt}
\setlength{\topskip}{0pt}
\setlength{\topmargin}{0pt}
\setlength{\topsep}{0pt}
\setlength{\partopsep}{0pt}
\setlength{\bibsep}{0.0pt}
\usepackage{hyperref}
\pdfoutput=1
\usepackage{titlesec}

\usepackage{multicol}

%\titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}

\newcommand{\FIXME}[1]{{\em Comment:  }{\color{red}{#1}}}
\newcommand{\MATTIA}[1]{{\em Comment:  }{\color{blue}{#1}}}

\def \aap  {A\&A}
\def \aaps  {A\&AS}
\def \aj  {AJ}
\def \apj  {ApJ}
\def \apjs  {ApJS}
\def \apjl  {ApJL}
\def \apss  {AP\&SS}
\def \araa  {ARA\&A}
\def \jcap  {JCAP}
\def \prd {Phy. Rev. D}
\def \ssr {SSRv}
\def \mnras {MNRAS}
\def \nat {Nature}
\def \physrep {Phys. Rept.}
\def \pasj {PASJ}
\def \etal {et~al.~}
\def \rmxaa {RMxAA}
\def \jgr {JGR}
\def \pasp {PASP}
\def \qjras {QJRAS}

\font\myfont=cmr20 at 19pt
\font\author=cmr20 at 13pt
\font\captionf=cmr10 at 10pt


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a few author defined macros like:
\def\beq{\begin{equation}}
\def\eeq{\end{equation}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\title{Characterizing the Galactic population of pulsars with the {\it Fermi} Large Area Telescope.}
%\title{{\myfont Characterizing the Galactic population of pulsars with the {\it Fermi} Large Area Telescope}. \\ {\author Mattia Di Mauro, Eric Charles, Seth Digel, Matthew Wood, Regina Caputo, Paul Rey, Julia Deneva, Elizabeth Ferrara,....} }%Title in 16pt
%\author{Mattia Di Mauro, Eric Charles, Seth Digel, Matthew Wood, Regina Caputo, Paul Rey, Julia Deneva, Elizabeth Ferrara,....} 
% We don't need title or author list in the narrative
%\title{\vspace{-0.5 in}}
%\date{\vspace{-13ex}}

\begin{document}

%\maketitle

%\address{W. W. Hansen Experimental Physics Laboratory, Kavli Institute for Particle Astrophysics and Cosmology, Department of Physics and SLAC National Accelerator Laboratory, Stanford University \\
%Stanford, CA 94305, USA \\
%E-mail: mdimauro@slac.stanford.edu}


%\begin{abstract}
%\end{abstract}

%%%%%%%%%%%%%%%%% now a standard article style for the most part

\paragraph{1\ \ Scientific rationale}
\label{sec:intro}
Pulsars (PSRs) are the largest population of Galactic $\gamma$-ray sources~\cite{Acero:2015hja}.  
They are associated with regions of massive star formation in the spiral arms 
of the Milky Way and therefore have generally been considered to form a `disk' population. 
One unexpected discovery from the {\it Fermi}-LAT data is that the central part of the 
Milky Way is brighter in the GeV energy range than predicted by models of 
diffuse interstellar $\gamma$-ray emission (IEMs) and point-source catalogs 
(see, e.g.,~\cite{TheFermi-LAT:2015kwa} and references therein).
This excess has been well characterized spatially ($20^{\circ}$ extent around the Galactic Center (GC) and 
well modeled with a spherically symmetric generalized Navarro-Frenk-White density profile) and 
spectrally (peaked at a few GeV).
However, its origin has been the subject of debate.
%In the last seven years many groups analyzing {\it Fermi}-LAT data and
%using the predictions from a variety of interstellar emission models
%(IEMs) and point-source catalogs have reported the detection of an
%excess at GeV energies with $20^{\circ}$ extent around the Galactic center
%(GC) (see, e.g.,~\cite{TheFermi-LAT:2015kwa} and references therein).
%This excess is well modeled with a spherically symmetric generalized
%Navarro-Frenk-White (NFW) density profile with index $1.25$ and its
%energy spectrum is peaked at a few GeV.  
Recently, evidence of the
existence of an unresolved population of sources in the inner
$20^\circ$ of the Galaxy with a total flux and spatial distribution
consistent with the GC excess has been published~\cite{Bartels:2015aea,Lee:2014mza}. 
These faint sources have been
interpreted as a population of PSRs located in the Galactic bulge and the
brightest sources in this population should already have been detected
and included in {\it Fermi}-LAT catalogs~\cite{Cholis:2014lta}.  This PSR
population is hypothesized to be distinct from the population in the Galactic disk 
%and its spiral arms and 
from which we detect the nearest PSR sample 
%\FIXME{Not sure whether most local sample means largest
%sample or the nearest pulsars} 
in radio and $\gamma$
rays~\cite{2005AJ....129.1993M,TheFermi-LAT:2013ssa}.  {\bf The results
  from Refs.~\cite{Bartels:2015aea,Lee:2014mza} and the spectral
  energy distribution (SED) of PSRs make a Galactic bulge population
  of PSRs one of the best-motivated interpretations of this excess.}

The region toward the Galactic plane is the brightest in
$\gamma$ rays, and has the highest concentration of unassociated
sources, i.e., detected sources with no known counterparts at other
wavelengths.  In the third {\it Fermi}-LAT source catalog (3FGL),
based on 4 years of Pass 7 REP data,
% for the $0.1$-$300$ GeV energy range, 
%of the 1300 sources detected at Galactic latitudes
%$|b|<20^{\circ}$~\cite{Acero:2015hja}, 35\% are extragalactic (mainly
%blazars), 10\% are PSRs, 5\% are supernovae, and nearly half are unassociated.
of the 1700 sources detected at Galactic latitudes
$|b|<30^{\circ}$~\cite{Acero:2015hja}, 40\% are extragalactic (mainly
blazars), 10\% are PSRs, 4\% are supernovae, and 40\% are unassociated.
%This fraction is much larger than in the extragalactic sky ($|b|>20^{\circ}$) where on average is 20-25\%. 
The majority of unassociated sources detected along the Galactic plane are
very likely from the Milky Way and most of them are probably
PSRs. 
{\bf Based on the density of detected blazars 
%detected at $|b|>20^{\circ}$ 
and on consideration that 60\% 
of associated Galactic sources are PSRs we estimate that among unassociated sources 
at $|b|<30^{\circ}$ 300 should be PSRs }
%{\bf Among the unassociated sources around 150 are expected to be blazars 
%(at $|b|>20^{\circ}$ unassociated sources, that are expected to be mostly blazars, 
%are about one third of detected blazars)
%and using the same relative numbers of associated 3FGL objects
%among the remaining 480 unassociated sources
%around 300 should be PSRs.}  
%\FIXME{How did you work out that number?}



We propose to take advantage of the large and growing volume of LAT data
after more than 8 years of operation 
%\FIXME{Maybe call it `more than 8 years'} 
and the large improvement in energy and
spatial resolution brought by Pass 8~\cite{2013arXiv1303.3514A} to
extract PSR candidates among sources detected at $|b|<30^{\circ}$
%in the entire sky 
%\FIXME{Maybe clarify whether you mean already cataloged sources or the results from your own all-sky analysis of 8 %years of data.}
using SED-based selection criteria that we have developed to spectrally distinguish between PSRs and blazars (the largest $\gamma$-ray source population) (\S~2 to \S~4).  
We will then use these candidates together with the efficiency for $\gamma$-ray PSR detection
and characterization that we will derive from Monte Carlo simulations to infer for
the first time the spatial distributions and luminosity functions of the 
disk and bulge $\gamma$-ray PSR populations (\S~5).  
This will test the PSR interpretation of the GC excess with unequaled
precision. 
This work will be based on the best-available LAT source catalogs 
complemented with our analyses of LAT data plus simulation studies. 
%Finally, our list of PSR candidates will be valuable in itself
%for future pulsation searches in the radio.
In addition to quantitatively defining the PSR populations, 
another result of this work will be a list of $\gamma$-ray sources
that are candidates for timing searches in radio observations.

%be used in radio followup searches of pulsation bringing the $\gamma$-ray pulsar
%population to increase significantly in the next few years.\FIXME{I
%think we will want to rephrase the last sentence}.

\paragraph{2\ \ Energy Spectra of PSRs and Blazars}
\label{sec:analysis}

In the 3FGL catalog, blazars are the most numerous source population
with 1700 objects out a total of roughly 3000 detected
sources~\cite{Acero:2015hja}.  The SEDs of 90\% of blazars in the 3FGL
are modeled with a power-law (PL) function while the remaining 10\%
that have significant spectral curvature are modeled with a
Log-Parabola (LP)~\footnote{See
  \url{http://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/source\_models.html}
  for descriptions of the spectral models discussed here.}.
%\FIXME{Can `that have significant spectral curvature' be omitted?}
On the other hand, of the 167 PSRs reported in the 3FGL, 115 are
parametrized with a power law with an exponential cutoff (PLE) because
they have significant spectral curvature and cutoff energies of a few GeV.
Thus, the shape of the energy spectrum is a promising observable that
can be used to distinguish PSRs from blazars. 

We have used the data set and the analysis described in the next
section to test both the PL and PLE spectral shapes and derive
observables to discriminate PSRs from blazars.  In particular, we
derive the likelihood value for the PL ($\mathcal{L}_{\rm{PL}}$) and
PLE shapes ($\mathcal{L}_{\rm{PLE}}$) and define the test statistic
($TS$) for a curved spectrum as:
$TS^{\rm{PLE}}_{\rm{curv}}=2\cdot(\log{\mathcal{L}_{\rm{PLE}}} -
\log{\mathcal{L}_{\rm{PL}}})$. We also use the spectral index and
energy cutoff when fit to a PLE spectrum ($\Gamma$ and $E_{\rm{cut}}$)
as discriminating variables.

%The 3FGL contains 167 PSR sources.  
For blazars we pre-select sources
with significance of spectral curvature, as given in the 3FGL
by the \texttt{Signif\_Curve} parameter, larger than 3.  This cut
selects 218 blazars (only 13\% of the entire sample).  In the left
panel of Fig.~\ref{fig:indexcutpsrblaz} we show the values of
$\Gamma$ and $\log_{10}{(E_{\rm{cut}})}$ for blazars and PSRs (separated
into young and Millisecond PSRs or MSPs), detected with
$TS^{\rm{PLE}}_{\rm{curv}}>9$.  Of the 218 blazars, 153 have
$TS^{\rm{PLE}}_{\rm{curv}}>9$ and among them only 12 have $\Gamma<2.0$
and $\log_{10}{(E_{\rm{cut}}[\rm{MeV}])}<4$.
%thus less than $1\%$ of the entire blazar population in the 3FGL catalog.  
On the other hand, of the 167 PSRs, about 88\% have
$TS^{\rm{PLE}}_{\rm{curv}}>9$ and 80\% have PLE parameters in the
selection region (given by the cyan band in
Fig.~\ref{fig:indexcutpsrblaz}).  Young PSRs and MSPs have the same energy
cutoff ($\log_{10}{(E_{\rm{cut}}[\rm{MeV}])}=3.46\pm0.25$ versus
$\log_{10}{(E_{\rm{cut}}[\rm{MeV}])}=3.42\pm0.22$) and young PSRs have
slightly softer indexes ($\Gamma=1.48\pm 0.45$ versus $\Gamma=1.20\pm
0.49$) but both populations are well within the selection criteria.
The second-most numerous population of $\gamma$-ray sources are
supernovae.  Among the 72 supernovae detected in the 3FGL 44 have an
SED modeled with a PL and 28 with an LP.  We applied our analysis to
them and found that only 12 pass the PSR selection criteria.  {\bf The
  selection criteria $TS^{\rm{PLE}}_{\rm{curv}}>9$ and $\Gamma<2.0$
  and $\log_{10}{(E_{\rm{cut}}[\rm{MeV}])}<4$ are very efficient at
  distinguishing PSRs from other source types.  We will use these
  criteria to select PSR candidates among the sources detected in our
  analysis of the full data set.}

%PSR 138 16 3 TOT 157


\begin{figure}[t]
	\centering
	\includegraphics[width=0.495\columnwidth]{CutoffIndex_3FGL_PSRYMFSRQ_compl_TS9.pdf}
        \includegraphics[width=0.495\columnwidth]{CutoffIndex_new_PSR_compl_TS9.pdf}
\vspace*{-10mm}
\caption{Left panel: $\Gamma$ and $\log_{10}{E_{\rm{cut}}[\rm{MeV}]}$ of 3FGL PSRs (MSPs are shown
  as blue points and young PSRs as black points) and of blazars in the
  3FGL with \texttt{Signif\_Curve}$>3$ (red points).  Right panel: same as in the left panel
  but for new $\gamma$-ray PSRs; see \S~4 for details.}
%\caption{{\captionf Left panel: $\Gamma$ and $\log_{10}{E_{\rm{cut}}[\rm{MeV}]}$ of 3FGL PSRs (MSPs are shown
%  as blue points and young PSRs as black points) and of blazars in the
%  3FGL with \texttt{Signif\_Curve}$>3$ (red points).  Right panel: same as in the left panel
%  but for new $\gamma$-ray PSRs; see \S~4 for details.}}
\label{fig:indexcutpsrblaz} 
\end{figure}



\paragraph{3\ \ Data Selection and Analysis Details}
\label{sec:details}

We plan to use all the available data in the energy range $0.3-
500$~GeV and at $|b|<30^{\circ}$.  Since we want to detect the
emission from point sources, we will use the \texttt{P8R2\_SOURCE\_V6} event class and instrument response functions.

We are going to employ the \texttt{Fermipy} python
package~\footnote{See \url{http://fermipy.readthedocs.io/}}, which
automates {\it Fermi}-LAT Science Tools analysis and 
provides a set of high-level tools for performing $\gamma$-ray
analysis such as computing the SED of a source, generating $TS$~\footnote{The $TS$ is defined as twice the difference in log-likelihood between the null hypothesis (i.e., no source present) 
and the test hypothesis: $TS = 2 \log\mathcal{L}_{\rm test} -
  \log\mathcal{L}_{\rm null}$.}  map for a region of interest (ROI), finding
new source candidates and localizing a source.


We will search for PSR candidates using the criteria reported in \S~2.
If the {\it Fermi}-LAT 8-year source Catalog (4FGL) is available in time for 
this project, we will use all the information from the catalog about the sources it contains.  
Otherwise the 4FGL is not
available we will start our analysis by using \texttt{Fermipy} to
find new sources with respect to the 3FGL catalog.  This would involve
dividing the sky into several hundred $10^\circ \times 10^\circ$ ROIs
and analyzing each in parallel.  In each ROI our initial model would
include the brighter 3FGL sources (i.e., those with $TS>36$) together
with publicly released Galactic IEM and isotropic templates ({\it
  gll\_iem\_v06.fits} and {\it
  iso\_P8R2\_SOURCE\_V6\_v06.txt}~\footnote{For descriptions of these
  templates see
  \url{http://fermi.gsfc.nasa.gov/ssc/data/access/lat/BackgroundModels.html})}
as a starting point).  We will omit 3FGL sources with $TS$ in the
range $[25, 36]$ to avoid thresholding effects from
variable sources.  Feasibility tests (see \S~4) have shown that 
we recover almost all of these sources in our analysis.
With multiple iterations of source finding we will first derive a list
of candidate new sources detected with $TS>16$, and 
then refit each ROI with this new source list to obtain
detection significances and SED parameters for all the sources in the
ROI.  

For each source detected with $TS>25$, from the 4FGL or with our
pipeline, we will extract the discriminating variables described in
\S~2.  {\bf The goal of this part of the analysis will
  be to have for each source $TS^{\rm{PLE}}_{\rm{curv}}$, the photon index ($\Gamma$) and
  energy cutoff ($E_{\rm{cut}}$) for the cases of PLE SEDs. These
  parameters will be used to select PSR-like sources among our list of
  detected sources as explained in \S~2.}



\paragraph{4\ \ Feasibility}

To alleviate concerns that our selection criteria will not work as
well on fainter sources, and to demonstrate the robustness of our
analysis pipeline, we have applied the analysis pipeline and selection
criteria to the list of 210 known $\gamma$-ray PSRs~\footnote{See
  \url{https://confluence.slac.stanford.edu/display/GLAMCOG/Public+List+of+LAT-Detected+Gamma-Ray+Pulsars}. 10 3FGL PSRs are not included in this list.}.
In particular we focus on the 53 new PSRs that were identified since
the publication of the 3FGL,
%\FIXME{You say previously that 3FGL has 167.  $210 - 167 = 43$, not 53}
of which 25 are unassociated 3FGL sources
and 28 are new sources with respect to the 3FGL.  We extract the discriminating variables and
apply the PSR candidate selection criteria as described in \S~2 (see
right panel of Fig.~\ref{fig:indexcutpsrblaz}).  Of these PSRs, 33 of 53
have $TS^{\rm{PLE}}_{\rm{curv}}>9$ and only 1 of them has SED parameters
incompatible with our selection criteria.  {\bf Our method
  thus works very efficiently also for recently discovered and relatively faint PSRs.}

%53 0 32 31 17 3  //  159 24 28

As a second feasibility study, we have analyzed an ROI with a width of $12^{\circ}$ and 
centered at
$(l, b)=(340^{\circ},-20^{\circ})$.  With 8 years of LAT data we
detect 15 sources with $TS>25 $ of which 9 are 3FGL sources and 6 are
new sources.  Of these 15 sources, 6 have $TS^{\rm{PLE}}_{\rm{curv}}>9$,
and among those one (3FGL J1730.5+0023) does not satisfy the PSR-like
criteria ($E_{\rm{cut}}=21$ GeV) and in fact is a blazar in the 3FGL. The other five
are PSR-like source candidates and are either unassociated in
3FGL or new sources.


%\begin{table}[t]
%\center
%\begin{tabular}{ccccccc}
%Name & $l$[deg]        & $b$[deg]       & $TS$  &  $TS^{\rm{PLE}}_{\rm{curv}}$  &  $\Gamma$  &  $E_{\rm{cut}}$[MeV] \\
%\hline
%3FGL J1730.5+0023  &  23.8 & 18.1  &  470.8 & 16.1 & $1.92 \pm 0.11$ &  $21400 \pm 3860$ \\
%3FGL J1730.6-0357  & 19.8  &16.0  & 223 & 30.7  & $0.84 \pm  0.46$ &  $1650 \pm 540$ \\
%3FGL J1659.0-0142  & 17.6 & 23.9  &  147 & 10.3 & $0.99 \pm 0.78$ &  $3180 \pm 2600$ \\
%PS J1737.4-0317   &21.3 & 14.8  &  30 & 13.0  & $1.35 \pm  0.80$ & $1400 \pm 300$ \\
%PS J1656.5-0410  & 15.0  & 23.2  &  48 & 14.1 & $1.15 \pm 0.70$ & $1410 \pm 280$ \\
%3FGL J1653.6-0158 &  16.6 & 24.9  &  3073 & 121.2 & $1.76 \pm0.09$ &  $3200 \pm 430$ \\
%\hline
%\end{tabular}
%\caption{{\captionf Name, position, $TS$, $TS^{\rm{PLE}}_{\rm{curv}}$, $\Gamma$ and 
%  $E_{\rm{cut}}$ for sources detected with $TS^{\rm{PLE}}_{\rm{curv}}>9$ for one ROI centered in %$(l,b)=(-20^{\circ},-20^{\circ})$.
%\FIXME{Be sure to add in the caption that this is only for one ROI!}}
%}}
%\label{tab:psrcand}
%\end{table}


\paragraph{5\ \ Efficiency and Maximum Likelihood Analysis}

We propose to employ the analysis explained above to find PSR-like
sources.  We will bin these detected PSR-like
sources in $l$, $b$ and energy flux ($S$): $N^{\rm{real}}(l,b,S)$.  We will also
derive the efficiency for the detection of PSR-like sources,
$\omega(l,b,S)$ by simulating PSRs with different luminosities and
positions in the Galaxy and using the analysis pipeline and selection
criteria described above. 
Given the number of simulated ($N^{\rm{true}}$) and detected
($N^{\rm{det}}$) PSRs for each $l$, $b$ and $S$ bin, the efficiency is
given by: $\omega(l,b,S)=N^{\rm{det}}/N^{\rm{true}}$.  We then derive
the expected distribution of observed PSRs ($N^{\rm{exp}}_{\rm{PSR}}$)
for a given model of PSRs ($N^{\rm{model}}_{\rm{PSR}}(\vec{\lambda})$,
where $\vec{\lambda}$ are the parameters describing the PSR population
in our model) with:
$N^{\rm{exp}}_{\rm{PSR}}(l,b,S)=N^{\rm{model}}_{\rm{PSR}}(\vec{\lambda})\times
\omega(l,b,S)$.  We will then employ a maximum likelihood analysis
(MLA) with Poisson statistics, comparing the number of PSR candidates
from the model ($N^{\rm{exp}}_{\rm{PSR}}(l,b,S)$) and from PSR
candidates detected in the real sky ($N^{\rm{real}}(l,b,S)$), and
derive the best-fit parameters for the disk and bulge PSR populations.
This will enable us for the first time to use the LAT observations to
precisely derive the spatial distributions and luminosity functions of
the two PSR populations.

For the disk component we will start by considering the spatial
distribution given in \cite{2004IAUS..218..105L} while for the bulge
population we will use $dN/dV\propto r^{\alpha}$ where $r$ is the
radial distance from the GC \cite{Calore:2014xka}.  For the luminosity
of PSRs we will employ a luminosity function given by a PL or a broken
PL with different slopes for the disk and bulge PSR populations and
treat the indexes as free parameters.  The numbers of disk
and bulge PSRs also will be free
parameters.


\paragraph{6\ \ Plans and Schedule}
This is a one-year project.  We will start by searching for PSR-like
sources.  We will then find the efficiency of the LAT to detect
PSR-like sources using Monte Carlo simulations and we will employ a
MLA to fit models for the numbers of PSRs in the Galactic disk and in
the bulge and to find their spatial distributions and luminosity
functions.  This result will enable us to determine conclusively whether the GC
excess is a population of PSRs in the Galactic bulge.  Finally,
since we will demonstrate that our criteria are very efficient for
selecting PSR-like sources we are confident that we will provide a list of PSR
candidates that could be used as targets of future radio follow-up searches for pulsation. 
This will increase significantly
the number of $\gamma$-ray PSRs detected by the LAT in the next few
years.
%\FIXME{Is this a one or two year proposal?}


{\bf Budget}: Given the substantial data analysis effort, we request
funding support for the PI ($\sim 30\%$ for one year), for two
conferences, and publication charges. Including overheads we
request a budget of 55k\$. Co-Is Di Mauro and Charles, will be
directly in charge of the analysis, while co-Is Digel, Wood and Caputo will
provide technical and scientific input.

\begin{multicols}{2}
\begin{flushleft}
\small
\begin{thebibliography}{10}

\bibitem[Acero et al.(2015)]{Acero:2015hja}
Acero, F. et al. 2015, ApJS, 218, 2, 23

\bibitem[Ajello et al.(2016)]{TheFermi-LAT:2015kwa}
Ajello, M. et al. 2016, ApJ, 819, 1, 44

\bibitem[Lee et al.(2015)]{Lee:2014mza}
Lee, S. et al. 2015, JCAP, 1505, 5, 56

\bibitem[Bartels et al.(2016)]{Bartels:2015aea}
Bartels, R. et al. 2016, PRL, 116, 5

\bibitem[Cholis et al.(2014)]{Cholis:2014lta}
Cholis, I. et al. 2015, JCAP, 1506, 6, 43

\bibitem[Abdo et al.(2013)]{TheFermi-LAT:2013ssa}
Abdo, A.~A. et al. 2013, ApJS, 208, 17

\bibitem[Manchester et al.(2005)]{2005AJ....129.1993M}
Manchester, R.~N. et al. 2005, AJ, 129, 4

\bibitem[Atwood et al.(2013)]{2013arXiv1303.3514A}
Atwood, W. et al. 2013, ArXiv: 1303.3514

\bibitem[Lorimer(2004)]{2004IAUS..218..105L}
Lorimer, D.~R. 2004, IAU, 218, 105

\bibitem[Calore et al.(2015)]{Calore:2014xka}
Calore, F. et al. 2015, JCAP, 1503, 38






\end{thebibliography}
\end{flushleft}
\end{multicols}


%{\footnotesize
%\begin{multicols}{2}
%\bibliographystyle{ieeetr}
%\bibliography{psr}
%\end{multicols}
%}

\end{document}
% LocalWords:  Frenk NFW GeV GC PSRs SED PSR unassociated 3FGL extragalactic V6
% LocalWords:  blazars 4FGL PLE observables html Signif blazar MSP P8R2 Fermipy
% LocalWords:  ROI IEM thresholding gll iem v06 iso txt significances J1730 MeV
% LocalWords:  FSRQ J1659 J1737 J1656 J1653 luminosities MLA Digel IEMs MSPs
% LocalWords:  ROIs SEDs 55k spectrally
