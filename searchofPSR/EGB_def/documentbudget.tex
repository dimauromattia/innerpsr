\documentclass[12pt,twoside,letterpaper,onecolumn,english]{article}

\usepackage[top=1.00in, bottom=1.00in, left=1.in, right=1.in]{geometry}
\usepackage{graphicx}
\usepackage[square,numbers,sort&compress]{natbib}   % short numerical refs
\usepackage[small,compact]{titlesec}   % to squeeze the doc a bit
\setlength{\parskip}{0in}
\setlength{\parindent}{1em}
\setlength{\parsep}{0pt}
\setlength{\headsep}{0pt}
\setlength{\topskip}{0pt}
\setlength{\topmargin}{0pt}
\setlength{\topsep}{0pt}
\setlength{\partopsep}{0pt}
\setlength{\bibsep}{0.0pt}
\usepackage{hyperref}
\pdfoutput=1
\usepackage{titlesec}

\usepackage{multicol}

%\titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}


\def \aap  {A\&A}
\def \aaps  {A\&AS}
\def \aj  {AJ}
\def \apj  {ApJ}
\def \apjs  {ApJS}
\def \apjl  {ApJL}
\def \apss  {AP\&SS}
\def \araa  {ARA\&A}
\def \jcap  {JCAP}
\def \prd {Phy. Rev. D}
\def \ssr {SSRv}
\def \mnras {MNRAS}
\def \nat {Nature}
\def \physrep {Phys. Rept.}
\def \pasj {PASJ}
\def \etal {et~al.~}
\def \rmxaa {RMxAA}
\def \jgr {JGR}
\def \pasp {PASP}
\def \qjras {QJRAS}

\font\myfont=cmr20 at 19pt
\font\author=cmr20 at 13pt
\font\captionf=cmr10 at 10pt


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a few author defined macros like:
\def\beq{\begin{equation}}
\def\eeq{\end{equation}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\title{Determining the contribution of point sources to the IGRB with an energy-dependent photon fluctuation analysis.}
%\title{{\myfont Determining the contribution of point sources to the IGRB with an energy dependent photon fluctuation analysis}. \\ {\author Mattia Di Mauro, Marco Ajello, Eric Charles, Keith Bechtol, Seth Digel} }%Title in 16pt
%\author{Mattia Di Mauro, Marco Ajello, Eric Charles, Keith Betchol, Seth Diegel.}
% We don't need title or author list in the narrative
\title{\vspace{-0.5 in}}
\date{\vspace{-13ex}}


\begin{document}

\maketitle

%\address{W. W. Hansen Experimental Physics Laboratory, Kavli Institute for Particle Astrophysics and Cosmology, Department of Physics and SLAC National Accelerator Laboratory, Stanford University \\
%Stanford, CA 94305, USA \\
%E-mail: mdimauro@slac.stanford.edu}


%\begin{abstract}
%\end{abstract}

%%%%%%%%%%%%%%%%% now a standard article style for the most part

\paragraph{1\ \ Scientific rationale}
\label{sec:intro}

The existence of an isotropic component in the $\gamma$-ray sky, called Isotropic Diffuse $\gamma$-ray background (IGRB), was first observed in 1972 by the OSO-3 satellite \cite{1972ApJ...177..341K}. 
%The spectrum of this irreducible residual has been then confirmed and measured by the SAS-2 \cite{1975ApJ...198..163F} and EGRET \cite{1998ApJ...494..523S} telescopes.
The spectrum of the IGRB has been precisely measured by the {\it Fermi}-LAT in the 0.1--820 GeV energy range at $|b|>20^{\circ}$ \cite{Ackermann:2014usa}.
The LAT also measured the Extragalactic $\gamma$-ray background (EGB), the sum of the IGRB and the flux from detected sources. 
%For the first time a deviation from a power-law trend consistent with an exponential cut off with energy break of 250 GeV has been observed.
The origin of the IGRB is one of the most intriguing problems in astrophysics and in the last decades two solutions have been mainly considered: unresolved point sources or diffuse processes.


The reason for the first interpretation is that since {\it Fermi}-LAT catalogs contain thousands of $\gamma$-ray objects \cite{Acero:2015hja}, many more sources should have fluxes below the detection threshold of the LAT. 
%Indeed the extragalactic source population of galaxie  studies in X-ray, infrared and radio bands applied to extragalactic sources brought to the conclusion that the unresolved population in $\gamma$-ray should be numerous (see e.g. \cite{DiMauro:2013xta,2012ApJ...755..164A}).
The {\it Fermi}-LAT detects at $|b|>20^{\circ}$ mostly extragalactic sources belonging to blazar, misaligned AGN, and Star-Forming Galaxy (SFG) populations. Different authors have shown that the unresolved emission from these sources represents a viable explanation for the origin of the IGRB (for a review see \cite{Fornasa:2015qua}). 

%The unresolved emission from AGNs and SFGs represents a viable explanation for the origin of the IGRB in fact the $\gamma$-ray emission from these extragalactic sources are able to fully explain the spectrum of the IGRB (see for a review \cite{Fornasa:2015qua}). 

A complementary or alternative possibility is that the IGRB is explained by diffuse processes such as the interaction of Ultra High-Energy Cosmic Rays (UHECRs) with the Extragalactic Background Light (EBL) or annihilation or decay of Dark Matter (DM) particles \cite{Fornasa:2015qua}.
The detection of DM particles is one of the most challenging problems in astroparticle physics and the IGRB has been widely used in the last decade to constrain Galactic and extragalactic DM structures.
Very stringent bounds are derived on the DM annihilation cross section and the decay time by estimating the unresolved emission from AGN and SFG sources, adding on top of them a DM contribution of $\gamma$ rays and finally comparing the total flux to the IGRB or the EGB data (see e.g. \cite{Ajello:2015mfa}).
Therefore, a robust estimation of the $\gamma$-ray emission from extragalactic sources would also have an important impact on the knowledge of DM particle interactions and distribution in the Universe.
%However, large uncertainties are associated to these processes and usually the contribution from point sources is used to constrain them.
%This method is widely used to 
%The nature and the existence of DM particles is on one of the most studied problem in astroparticle physics and the IGRB has been widely used in the past decades to constrain the annihilation cross section or the decay time of these exotic particles.


%Blazars are AGN with the jet aligned with the line of sight and they costitute the most numerous source population in Fermi-LAT catalogs. On the other hand AGNs with jet misaligned with respect to the line of sight (MAGN) have weaker luminosities due to Doppler boosting effects and only a dozen of MAGNs have been detected in Fermi catalogs. 

The contribution of AGNs and SFGs to the IGRB is usually derived with population studies applied on a sample of hundreds of {\it Fermi}-LAT catalog objects. In particular this method consists of deriving the flux ($S$) distribution $dN/dS$, also called source count distribution, from the detected sources and integrating this quantity down to 0.
The main uncertainty of this procedure is that it is based on an extrapolation of the source count distribution much below the flux threshold of the LAT. 
%This method is based on the hypothesis that the unresolved population has the same energy spectrum and cosmological evolution of the detected sources and its main uncertainty is that it includes an extrapolation of the source count distribution much below the flux threshold of the LAT. 
%This method brought different authors to recently find that blazars account for about 20-50 \% of the IGRB (see for a review \cite{Fornasa:2015qua}). 
%The situation for SFGs and MAGNs is even worse because {\it Fermi}-LAT catalogs contain a very small sample of these objects and so correlations between $\gamma$-ray and radio or infrared luminosities are employed \cite{DiMauro:2013xta,2012ApJ...755..164A}.

%On the other hand radio and infrared (for SFGs) experiments detect thousands of SFGs and MAGNs. Therefore, usually correlations between $\gamma$-ray and radio or infrared luminosities correlations between $\gamma$-ray and radio or infrared luminosities are used to derive the contribution of MAGNs and SFGs to the IGRB (see e.g. \.


{\bf We propose to use for the first time photon fluctuation analysis (PFA) in different energy bins at $E>1$ GeV to derive the contribution of undetected point sources to the IGRB as a function of energy.}
PFA is able to measure the shape of the source count distribution well below the threshold of the LAT, thus avoiding a need to extrapolate the $dN/dS$. 
%We propose here to use photon fluctuation analysis (PFA) in different energy bins at $E>1$ GeV, avoinding so the strong contamination from Galactic diffuse emission (GDE), to derive the contribution of undetected point sources to the IGRB as a function of energy.  
%{\it SD: Maybe say why this energy range was chosen, and that the approach of analyzing in bands is new.}
%This new technique would permit us to observe or constrain the $\gamma$-ray emission from faint source populations or DM substructures that have $\gamma$-ray spectrum and $dN/dS$ different from blazars.
%The PFA is therefore a way to measure the contribution of unresolved point sources using the observed data of the LAT directly. 
We will show in the next sections that this technique applied to $E>10$ GeV would lower the flux sensitivity by a factor of 30 with respect to the present {\it Fermi}-LAT catalogs and increase the fraction of EGB resolved as due to point sources from 50\% to about 90\%. This result would leave only a little room for exotic mechanisms such as the $\gamma$-ray emission from DM particle interactions.

%SFGs are objects where the star formation rate is intense and $\gamma$ rays are produced from the interaction of cosmic rays with the interstellar medium or interstellar radiation field. SFG $\gamma$-ray emission is very dim and the LAT has detected so far only nine individual galaxies citazione.
%Unresolved emission from MAGN, blazars and SFGs provide a viable explanation of the IGRB (see e.g. \cite{Fornasa:2015qua}).
%This kind of results are usually derived for blazars with population studies of the observed sources while for MAGN and SFGs which are less numerous deriving some correlation between $\gamma$-ray luminosity and radio for MAGN or infrared bands for SFGs.
%More precisely the main observable used is the source count distribution $dN/dS$ and the contribution of each source population is derived integrating this quantity below the sensitivity threshold of the LAT.
%The main drawback of these analysis is that they are calibrated on the observed population of point sources and their results are therefore a conseguence of large extrapolation below the sensitivity of the LAT.
%Diffuse processes, such as interaction of ultra high-energy cosmic rays with the extragalactic background light or annihilation or decay of dark matter particles, have been considered as alternative explanation of the IGRB.
%However it's very difficult to constrain such processes as explained in citazioni.
%We propose to use an energy dependent photon fluctuation analysis to derive the contribution of point sources to the IGRB.







\paragraph{2\ \ Photon fluctuation analysis}
\label{sec:analysis}

PFA is a statistical tool employed to constrain the source count distribution below the sensitivity of a catalog of sources and it permits deriving the number of faint sources that contribute a few photons each to the observed counts.
%The idea of this tool is to derive the number of pixels $N_{\rm{pixel}}$ with different number of photons $N_{\rm{photon}}$. 

This method consists of creating simulations of the $\gamma$-ray sky including the Galactic diffuse emission (GDE), the isotropic diffuse emission (IDE) and the $\gamma$-ray flux from a population of point sources.
The flux of point sources is extracted from the source count distribution. Above the catalog flux threshold the shape of the $dN/dS$ is fixed to the observed flux distribution while below the catalog threshold sensitivity simulations are created using different assumed shapes.
The distribution of the number of pixels $N_{\rm{pixel}}$ with different numbers of photons $N_{\rm{photon}}$ is then derived for the real sky and the simulations.
Finally, from comparing simulations and the real sky, the flux distribution is derived very precisely  much below (about one order of magnitude) the flux threshold of the catalog and for sources which contribute as few as 1 photon per pixel.

This method has been successfully employed at other wavelengths. In X-ray for example PFA revealed, one decade before Chandra actually observed those sources, that up to $\approx 90 \%$ of the X-ray background is due to AGN \cite{1993A&A...275....1H}. The PFA is also used in radio astronomy (see e.g. \cite{1982MNRAS.198..221W}) and substitutes for source detection in the analysis of infrared maps \cite{2010ApJS..191..222V}.

%As said already, this analysis is sensitive to fluxes at which the source population contributes on average 1 count per pixel (or beam) and thus a factor 20–50 lower fluxes (depending on the source projected density) below the level of single detectable sources

%The PFA was used in $\gamma$ rays for the first time by Ref.~\cite{2011ApJ...738..181M} to derive the source counts distribution, finding good agreement with the results of the 1FGL catalog Abdo et al. (2010a). 
%However in that paper they have not derived the number of sources below the sensitivity of the catalog.
%On the other hand, recently, Ref.~\cite{Zechlin:2015wdz} used the same method to derive the flux distribution below the sensitivity of the 3FGL catalog in the energy range between 1-10 GeV finding that the slope below the threshold of the LAT is about $2.0$ for $dN/dS\propto S^{-\alpha}$ .
Recently, the {\it Fermi}-LAT Collaboration has applied this method to the 2FHL, a catalog of sources at $E>50$ GeV derived with 80 months of data \cite{Ackermann:2015uya}, in order to derive the contribution of blazars to the EGB \cite{TheFermi-LAT:2015ykq}. 
In that analysis the shape of the $dN/dS$ was constrained up to one order of magnitude below the sensitivity of the LAT, thus indicating that for the range $E>50$ GeV blazars explain $86^{+16}_{-14}$ \% of the EGB.

%In that analysis the PFA has been applied to the 2FHL catalog \cite{Ackermann:2015uya} making possible to constrain the source count distribution up to about one order of magnitude below the sensitivity of the LAT. The 2FHL catalog is mostly made of blazars and with this analysis it was possible to infer that the source population of these objects explains $86^{+16}_{-14}$ \% of the EGB.

%{\bf We propose to employ the PFA for the first time in different energy bins at $E>10$ GeV in order to derive a precise estimation for the contribution of point sources to the IGRB.}






%\section{Details of the analysis}
%This part should be filled with Brendan model.




%\paragraph{3\ \ Target Selection and Analysis Details}
\paragraph{3\ \ Data Selection and Analysis Details}
\label{sec:details}

%The third {\it Fermi}-LAT source catalog (3FGL) of sources contains, in the 100 MeV-300 GeV range and using 4 years of exposure, about 2000 objects detected at $|b|>20^{\circ}$ with the Pass 7 REP event classifications \cite{Acero:2015hja}. 
We plan to apply the PFA %at $E<50$ GeV 
to the same energy bins as the 3FGL, a {\it Fermi}-LAT catalog for the $0.1-300$ GeV energy range using 4 years of Pass 7 REP data  \cite{Acero:2015hja}. 
%The 3FGL contains about 2000 objects at $|b|>20^{\circ}$ \cite{Acero:2015hja}. 
For the energy range $E>50$ GeV we will use the energy bins and source characteristics from the 2FHL.
We will first calculate the flux $dN/dS$ and photon index ($\Gamma$) distribution $dN/d\Gamma$ using 3FGL catalog sources at $|b| > 20^{\circ}$. 
We focus our analysis at $|b| > 20^{\circ}$ because the IGRB is an isotropic component and we are thus interested in extragalactic sources.
From the observed index and flux distribution we plan to produce precise simulations of the $\gamma$-ray sky.
The {\it Fermi}-LAT Collaboration has recently released Pass 8, a new event-level analysis that increases the acceptance by about 25\% above 1 GeV \cite{Atwood:2013dra}. 
{\bf We propose to use Pass 8 and the PFA to derive the intrinsic source count distribution of point sources well below the sensitivitity limit of the 3FGL and 2FHL catalogs and estimate their contribution to the IGRB.}
%These improvements are also quite significant at very high energy where at 50 GeV and 500 GeV the acceptance increases by a factor of 20\% and 50\% respectively. 
%This catalog contains about 360 sources and at Galactic latitude $|b| > 10^{\circ}$ their are almost all blazars. 
%Taking into account this catalog and the PFA method the {\it Fermi}-LAT Collaboration has demonstrated that about $86^{+16}_{-14}$ \% of the EGB at $E>50$ GeV is explained but the $\gamma$-ray emission of blazars \cite{TheFermi-LAT:2015ykq}.
%Given the sensitivity of Pass 8 and with the goal of filling the gap with the Cherenkov telescope measurements, the {\it Fermi}-LAT Collaboration is planning to create a new catalog of hard sources at $E>10$ GeV, dubbed as 3FHL, with at least 90 months of exposure. The preliminary version of this catalog, made with 80 months of data, includes about 1500 sources that at $|b|>20^{\circ}$ are mostly associated to blazars.
%Cross correlating the position of the 3FHL seeds with the position of 3FGL and 2FHL catalog objects this catalog contains about 900 AGN (to check this number) at $|b| > 20^{\circ}$.
%The 3FHL flux sensitivity is about $2\times 10^{-11}$ ph cm$^{-2}$ s$^{-1}$, 4 times better than before with the 1FHL \cite{Ackermann:2013fwa}. Adding the PFA we could reach a sensitivity of about $2\times 10^{-11}$ ph cm$^{-2}$ s$^{-1}$ with an improvement of a factor of 20 with respect to the 1FHL catalog.}
We will employ the P8R2\_SOURCE\_V6 instrument response functions.
The GDE and the IDE will be simulated using the gll\_iem\_v06.fits and iso\_P8R2\_SOURCE\_V6\_v06.txt templates (see \hyperref[fermi.gsfc.nasa.gov]{http://fermi.gsfc.nasa.gov/ssc/data/access/lat/BackgroundModels.html}). 
The flux from point sources will be extracted from the source count distribution modeled with  a broken power law where $\alpha_1$ ($\alpha_2$) are the slope above (below) the break flux $S_b$. 
The value of $\alpha_1$ will be fixed by the source flux distribution of the 3FGL catalog while the shape of the $dN/dS$ below the sensitivity of the LAT will be derived with the PFA.
We are going to generate simulations for different values of $S_b$ and $\alpha_2$.
For each configuration of $(\alpha_1,\alpha_2,S_b)$ we will generate about 30 simulations of the sky and we will compare, with a chi-square ($\chi^2$) analysis, the pixel counting distributions ($N_{\rm{pixel}}(N_{\rm{photon}})$) of the simulations, averaged over 10 simulations, and the one of the real sky. For an example of $N_{\rm{pixel}}(N_{\rm{photon}})$ derived with PFA see the result we have derived for the 2FHL in Fig.~5 of \cite{TheFermi-LAT:2015ykq}. The $\chi^2$ analysis will permit us to derive the intrinsic shape of the $dN/dS$ below the LAT sensitivity limit.

Then we will generate 10 simulations of a population of point sources with the flux distribution that we found.
The simulations will be analyzed in the same way as for the catalogs (see \cite{Acero:2015hja} for more details).
%This starts from detecting source candidates using a sliding-cell algorithm and a wavelet analysis then analyzing each with the standard {\it Fermi} Science Tools, in order to derive the $\gamma$-ray properties of detectable sources. 
%We plan to check that the average number of detected sources is compatible with the number of sources present in the 3FHL catalog.
%Taking into account the flux distribution of the detected sources sources in the simulations (with $TS>25$, $|b|>20^{\circ}$ and $N_{\rm{pred}}>?$) we will derive the detection efficiency $\omega(S)$.
Taking into account the flux distribution of the detected sources in the simulations we will estimate the detection efficiency $\omega(S)$: the probability for the LAT to detect a source with a given flux $S$ calculated in each flux bin as the ratio of detected sources and simulated objects in that bin.
% $\omega(S) = N_{\rm{meas}}/N_{\rm{true}}$.

Once the detection efficiency is calculated we can correct the observed source count distribution and derive the intrinsic flux distribution of point sources. This would be a robust estimation of the $dN/dS$ for a wide flux range that is also reliable well below the flux threshold of the LAT.
%not only for the flux range of the catalog but also for about one order of magnitude below the threshold of the LAT as we have found for the 2FHL catalog in Ref.~\cite{TheFermi-LAT:2015ykq}.
Finally, by integrating the intrinsic $dN/dS$ we will find the contribution of point sources to the IGRB defined as $I = \int_0^{S_{\rm{max}}} \omega(S) S (dN/dS) dS$. 
%It's possible with the same procedure to derive the contribution of undetected point sources to the EGB without including $\omega(S)$ in the integration.
%The method we propose is not affected by any correlation between the photon index and the $\gamma$-ray flux and so no correction, as the one in Ref.~\cite{2012ApJ...753...45S}, is needed for this effect.
We will perform this analysis in the energy bins of the 3FGL catalog ($E>1~GeV$) and for the 2FHL ($E>$50~GeV) in order to have an energy-dependent estimation of $I(E)$.

{\bf We point out again that the advantage of PFA for deriving the $dN/dS$ well below the flux sensitivity of the catalog is that it is much less dependent on extrapolation than population studies.}



%Using the results of the 1FHL and 3FHL catalog sounds reasonable to suppose the following bins: 10-22, 22-38, 38-60, 60-200, 200-600 and 600-3000. 
%This permits us to have in practice 6 energy bins and to have values of a photon index between each couple of near points. In practice is like to have three catalogs in only one.
%Given this framework we can use each sub-catalog to make the same photon fluctuation analysis made for the 2FHL catalog and derive the contribution of point sources for three differerent energy bins: 10-38 GeV, 38-200 and 200-3000 GeV. 
%We call this method multi-color photon fluctuation analysis because we are proposing to add a new variable in the analysis which is the energy. 
%This analysis represents the first attempt to derive an energy bin calculation of the unresolved emission of points sources. As it is defined this method is much more sensitivity to the contribution of unresolved point sources to the IGRB and is less dependent on extrapolation below the sensitivity since this enable usually to infer the shape of the source count distribution about a factor of 10 below the threshold of the catalog.







\paragraph{4\ \ Feasibility}
%We have already applied the method explained above for the 2FHL catalog in Ref.~\cite{TheFermi-LAT:2015ykq}. The flux threshold of the 2FHL catalog is about $8\times10^{-12}$ ph cm$^{-2}$ s$^{-1}$ and using the PFA technique we were able to observe the existence of a break with a change of slope above and below of about $\Delta \alpha \approx 0.9$.
%The PFA analysis applied to this catalog permitted us to derive the $dN/dS$ about one order of magnitude below the threshold of the 2FHL.
%We also constrained a steep re-steepening of the source count distribution, as might occur for the presence of a faint population of sources such as SFGs, to be below about $7\times10^{-13}$ ph cm$^{-2}$ s$^{-1}$.
%In the right panel of Fig.\ref{fig:results}  we show the main result of this analysis. The vertical line is a representation of the flux sensitivity of the PFA analysis and the solid black line is the inferred source count distribution which converge at faint fluxes to the $85\%$ level of the EGB.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.495\columnwidth]{logNlogS_1FHL_proposal.pdf}
	\includegraphics[width=0.495\columnwidth]{unresolvedEGB_3FHL_proposal.pdf} 
\vspace*{-10mm}
\caption{{\captionf Left Panel: $dN/dS$ of the 1FHL $>$10 GeV catalog (\cite{Ackermann:2013fwa}, black points) and best fit (black line) and $1\sigma$ band (grey band) for the result derived with the PFA. The red line indicates $90\%$ of the EGB at $>10$ GeV. Right Panel: best fit (black line) and $1\sigma$ band (grey) for the unresolved fraction of the EGB above 10 GeV for sources with integral flux greater than $S_{\rm min}$. The 1FHL (green line) and PFA (orange line) sensitivity flux and unresolved EGB fraction are also shown.}}
\label{fig:results} 
\end{figure}

In order to show the potential of the PFA, we applied this analysis at $E>10$ GeV and 80 months of exposure. 
For this scope we considered the observed flux distribution and photon index of the 1FHL, the first {\it Fermi}-LAT catalog of sources detected above 10 GeV \cite{Ackermann:2013fwa}. This catalog was based on three years of Pass 7 data. 
The 1FHL flux limit at $|b|>20^{\circ}$ is about $4\times10^{-11}$ ph cm$^{-2}$ s$^{-1}$.

We used the same analysis procedure as in Ref.~\cite{TheFermi-LAT:2015ykq} and the results are displayed in Fig.~\ref{fig:results}. The PFA method permits us to verify the existence of a break in the source count distribution in the range $[4,6]\times10^{-11}$ ph cm$^{-2}$ s$^{-1}$. Moreover we have deduced that the slope below this break is $\alpha_2\in[1.75,1.85]$. We also found that the sensitivity of the PFA is around $1.7\times 10^{-12}$ ph cm$^{-2}$ s$^{-1}$ (see \cite{TheFermi-LAT:2015ykq} for more details) permitting us to constrain the source count distribution a factor of 30 below the threshold of the catalog. 
Integrating the derived source count distribution down to zero we calculated that about $90\%$ of the EGB is explained above 10 GeV with point sources. We point out that similar results have been derived with the 2FHL at $E>50$ GeV \cite{TheFermi-LAT:2015ykq}.
In the right panel of Fig.\ref{fig:results} we show the average and $1\sigma$ uncertainty for the fraction of the EGB intensity that remains unresolved after we subtract the emission from point sources as derived in our analysis.
%We display also the 90\% level, the sensitivity limit of the PFA and the flux threshold of the 1FHL catalog \cite{Ackermann:2013fwa}.

%The vertical line is a representation of the flux sensitivity of the PFA analysis and the solid black line is the inferred source count distribution which converge at faint fluxes to the $85\%$ level of the EGB.

{\bf These preliminary results show that a PFA applied at $E>10$ GeV, with Pass 8, to derive the source count distribution would be able to lower the flux threshold by a factor of about 30 with respect to the 1FHL catalog \cite{Ackermann:2013fwa}. This translates in an increasing fraction of EGB resolved with point sources from about 50\% with the 1FHL up to about 90\% with a PFA.}
%Add that this improvement is much more than expected only from more years of data taking..







\paragraph{5\ \ Plans and Schedule}
%We propose to use the future 3FHL catalog at $E>10$ GeV to perform the first energy dependent PFA ever done.
%We propose to perform the first energy dependent PFA ever done at $E>10$ GeV.
%We plan to divide this energy range in at least four energy bins and to apply the PFA in each of them, enabling the derivation of a robust and energy-dependent estimation for the contribution of point sources to the IGRB. This is a reasonable number of bins since the 2FHL catalog at $>50$ GeV includes 3 energy bins.
We propose to perform the first energy-dependent PFA ever done in the 1--2000~GeV energy range.
We plan to divide this energy range into at least six energy bins and to apply the PFA in each of them, enabling a robust energy-dependent estimation for the contribution of point sources to the IGRB. 
%This is a reasonable number of bins since the 3FGL catalog in the range 1--10 GeV is divided into 2 bins and the 2FHL catalog at $>50$ GeV includes 3 energy bins. 
We will use the 3FGL, 2FHL and any future {\it Fermi}-LAT catalogs that are released in the next two years.


This analysis would have important consequences in astroparticle physics.
Given the energy dependence of the unresolved EGB fraction we could provide a precise constraint for the DM contribution to the IGRB which in turn translates into bounds for the annihilation cross section or the decay time of DM particles. These constraints would be more robust than the ones derived in the past with the IGRB (see e.g. \cite{Ackermann:2015tah}).
We plan to constrain also DM substructures. 
The $\gamma$-ray spectrum and the $dN/dS$ of DM subhalos are different from the one of blazars. We would benefit from the energy dependence of PFA to constrain DM substructures that for some energy bands could emerge in the measured blazar $dN/dS$.
%Given the energy dependence of the unresolved EGB fraction we could provide a precise constraint for the DM contribution to the IGRB which in turn translates into bounds for the annihilation cross section or the decay time of DM particles.
%These limits would be the most robust ever done. 
%These constraints would be more robust than the ones derived in the past with the IGRB (see e.g. \cite{Ackermann:2015tah}).
%Usually in fact these limits are derived with two different approximations: {\bf a conservative case} where point sources do not contribute to the IGRB and {\bf an optimistic case} where point sources also provide a fit to the IGRB spectrum and the DM contribution is added on top of them (see e.g. \cite{Ackermann:2015tah}). 
%Our results could provide bounds on DM that would be about a factor 10 stronger than the conservative approach.
%Since the $\gamma$-ray spectrum from a DM contribution is different from the one of blazars, we would benefit from the energy dependence of PFA and constrain DM substructures that for some energy bands could emerge in the measured $dN/dS$.
We mention also that we could derive a precise estimation for the maximum contribution of other diffuse processes such as the interaction of UHECRs with the EBL.
Finally, since mostly blazars contribute to the source count distribution, our results would constrain the presence of a faint population of sources that might manifest as a re-steepening of the $dN/dS$. Moreover given the energy dependence of the PFA we could have information about the $\gamma$-ray spectrum this unresolved source population has. SFGs for instance are a population of faint $\gamma$-ray sources and our results place a severe bound on the flux distribution of these objects, thus constraining their contribution to the IceCube astrophysical neutrino flux \cite{Bechtol:2015uqb}.


\newpage
\begin{multicols}{2}
\bibliographystyle{ieeetr}
\bibliography{egb_prel}
\end{multicols}

\end{document}