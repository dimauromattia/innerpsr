\documentclass[preprint]{aastex}  
%\documentclass[iop]{emulateapj}
%\usepackage{booktabs,caption,fixltx2e}
\usepackage{natbib}
\bibliographystyle{aa}

\usepackage{graphicx,color,rotating}
\usepackage{footnote,lineno}
\usepackage{ulem} 
\usepackage{xspace}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\usepackage{txfonts}
\usepackage{graphicx,amssymb,amsmath,amsfonts,times,hyperref}
%\usepackage{rotating} 

%\linenumbers

\newcommand{\fermipy}{\texttt{Fermipy}\xspace}
\newcommand{\FIXME}[1]{{\em Comment:  }{\color{red}{#1}}}
\newcommand{\mypar}[1]{\noindent \textbf{#1}}
\newcommand{\review}[1]{\noindent #1}
\newcommand{\reply}[1]{{\noindent \it Reply:  }{\color{blue}{#1}}}



%\usepackage{epsfig,epstopdf}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{document}
%

We thank the journal referee for carefully reading our paper
and providing insightful and useful comments.  We 
address each comment here and have updated the text accordingly.  For
convenience, all new or modified text is set in {\color{blue}{blue}}
in the revised draft.


\mypar{General Comments}

\review{First, this is a very nice paper on a topic of great interest
  and certainly merits publication in ApJ. The authors are to be
  congratulated on their extensive work.}

\reply{Thank you for the kind comments.}

\review{I do have some concerns about the reliability of the results,
  as I detail below, specifically about the IEMs used and whether they
  form a "complete basis". I would really like to see these addressed,
  as I think they can help settle the technical issues in interpreting
  the GC excess. Once we finally have a model we can believe, we can
  argue about what it means. On the other hand, will those authors
  publishing sloppy results be swayed by excellent technical work?
  Probably not, so while I encourage you take these steps to improve
  the paper, I'm not going to argue too strongly.  Below, I intermix
  some comments with the larger arguments. Sorry about that, but I'll
  try to label appropriately.}

\reply{We understand the overall concern about the effect of the
  uncertainties and biases of the IEMs on the results.  We have tried
  to address these concerns in detail below.}


\mypar{Abstract}

\review{Tweak: I would invert the ratio of disk pulsars to bulge
  pulsars. You're arguing for detection of a bulge population, so say
  the model prefers 1 bulge pulsar for every 3 disk pulsars.}

\reply{Thank you; we have adopted this suggestion.}

\review{Serious: you adopt particular spatial profiles for the bulge
  and the disk, and this represents some uncertainty that isn't really
  folded into the budget. Why should gamma-ray pulsars necessarily
  follow the same disk estimated for radio pulsars? In particular,
  it's not clear they would have the same scale height, since
  gamma-ray pulsars probably shut off before they can reach a 1 kpc
  scale height, if produced close to the plane. Fortunately, this bias
  would make a disk look even less like a bulge, but still, it's worth
  pondering and discussing.}

\reply{The distribution of disk pulsars that we consider is derived
  from the radio distribution of pulsars corrected for observational
  biases. Therefore the distribution of pulsars of the disk from
  Lorimer (2004) should be the real distribution of all radio pulsars.
  As the referee points out, gamma-ray pulsars
  might have a distribution more strongly peaked toward the
  plane, as they tend to be younger and so have not migrated as far from
  the disk.  However, 1) as the referee points out, this would further
  increase the difference between the bulge and the disk and 2) in
  many of the fits reported in Table 2 we have left the scale height
  of the Galactic disk as a free parameter, typically finding best-fit 
  values of $\sim 0.70 \pm 0.20$~kpc.  We have added some text to Section
  8 to comment on this point.}


\mypar{Section 1}

\review{Tweak: I morally object to a sentence with a semicolon and a
  colon!}

\reply{We have split the offending sentence.}

\review{Tweak: for your readers' convenience, you might give the NFW
  profile inline.}

\reply{We have added an equation with the NFW profile.}
  
\review{Tweak: typo, "confirm the existence the GC excess"}

\reply{We have corrected the text.}

\review{Should probably give at least one citation to defend "known to
  be DM dominated"}

\reply{We have added a reference to Strigari et al. (2008).}

\review{The paragraph on radio detections of bulge pulsars seems out
  of place in the development; I'd move it before the "Recently,
  evidence" para.}

\reply{We have moved the paragraph as suggested.}


\mypar{Section 2}

\review{Serious concern about IEM \#1: the residual limb contamination
  obviously depends strongly the zenith angle cuts *AND* on the
  rocking angle history of the observatory. The contribution prepared
  for the 4-year analysis will not necessarily provide a good
  description for the data set considered here.}

\reply{While it is true that the residual limb contamination depends
  on both the zenith angle cuts and the rocking angle, we do not think
  that the limb contamination has a significant effect in this
  analysis.  For this analysis we have applied a cut that rejects events
  with zenith angles $> 90^\circ$.  For the energy range used
  in this analysis ($E > 300$ MeV) this has been shown to reduce the limb
  contamination to negligible levels for point-source analysis.  This
  is discussed in the event selection recommendations provided on the FSSC Web site\footnote{\url{https://fermi.gsfc.nasa.gov/ssc/data/analysis/documentation/Cicerone/Cicerone\_Data\_Exploration/Data\_preparation.html}}.  The
  IEM \#1 was generated by re-scaling the Pass 7 IEM to account for the
  effects of differences in energy dispersion in Pass 8.  The original Pass 7 IEM was generated
  with zenith angle cuts that also minimized the effect of residual
  limb contamination, which was less of a problem in Pass 7, because
  the effective area at the edges of the field of view, where the vast
  majority of the limb exposure occurs, was much smaller.  We have
  reworded the sentence about the zenith angle cut to clarify 
  that this reduces the limb contamination to negligible levels.  We
  will also work with the FSSC to ensure that the documentation clarifies
  the point that the ``Official'' IEM does not suffer from significant
  limb contamination.}
  
\review{Serious concern about the IEM \#2: you adopt two IEMs and
  perform parallel analyses. This is a great idea for getting an
  estimate of systematic effects, but you don't know that *either*
  model is a good description of reality. Indeed, your clustering
  analysis suggests the model is problematic precisely in the areas of
  highest expected signal. What I would really like to see is some
  type of goodness of fit analysis. Energy-resolved residuals maps is
  a good place to start, and this will work well for $\le 1$ GeV, which
  is where I'm most concerned about the IEMs. If one of your models is
  markedly worse than the other, unfortunately I'd argue that this
  means you don't have a good "spanning basis" for your
  systematics. Likewise, if neither is very good, but they both suffer
  similar issues (e.g. similar patterns) then you also don't have a
  good basis. Unless you confident that the models do a good job of
  encapsulating the uncertainty and aren't *too* wrong, then all of
  the results, and those presented in parallel, become much less
  robust.}

\reply{While it is certainly true that the IEMs are both imperfect,
  what we are concerned with here is the degree to which those
  imperfections can impact point-source analysis, and even more
  specifically, our PSR selection criteria.  This can be caused by
  over-prediction of the model masking real sources (or, more
  specifically, decreasing the efficiency to detect sources of a given
  flux), by under-predictions in the model resulting in spurious
  sources (or, more specifically, by making it more likely that
  background photons are incorrectly attributed to sources, thus
  inflating their significance) or by changing the observed spectral
  properties of sources.  Goodness-of-fit, by itself, does not really
  quantify the magnitude of these effects, for several reasons: 1)
  Both models have been fit to the LAT data and include residuals that were
  fed back into the model; 2) Our point-source finding approach 
  continues to add sources to the model until the residuals are
  uniformly below the source sensitivity threshold, this will
  artificially improve the goodness-of-fit;  3) We are specifically
  looking for a population of sources most of which are below the
  detection threshold; thus, we expect to see many sub-threshold
  excesses across the $40^\circ \times 40^\circ$ ROI;  4) We are most
  concerned with features in the IEMs that can either emulate or mask
  point sources, or cause us to mis-estimate their spectra, not in the
  large-scale diffuse structures whose mis-modeling dominates the 
  goodness-of-fit metrics.

  To quantify the degree to which our source list and PSR selection
  might be affected by biases of the IEMs we have undertaken a number
  of additional studies.  These are described in a new appendix
  ``Systematic biases in source-finding efficiency and spectral
  parameter estimation from uncertainties in the IEMs''.  In short we
  have found that away from the Galactic plane (i.e., for $|b| >
  2.5^\circ$) the differences between the results obtained with the
  two IEMs are attributable to the thresholding effects combined with
  systematic uncertainties in the IEMs that are similar in magnitude to the
  statistical uncertainties.  Accordingly, we have redone the maximum
  likelihood analysis described in Section~8 with a mask applied along 
  the Galactic plane (i.e., removing the region $|b| < 2.5^\circ$ from the analysis).
  Even with the Galactic plane masked the data still favor the 
  existence of the Galactic bulge population at the level of 6.2 standard 
  deviations.  This study is described in the newly added Table 3.}


\review{Serious concern about the IEM \#3: much of the paper results
  rest on the statistics of sources with particular spectral shapes,
  in specifically high-energy cutoffs and hard low-energy
  spectra. These inferences depend strongly on the reliability of the
  low-energy portion of the spectrum. As you point out, deficits
  (excesses) in the IEM can lead to clusters (or absences!) of
  sources, but they will also affect the spectral shape. I'd encourage
  you to look at seds for each source and see if the low-energy bins
  are in tension with their models, and if those trends have structure
  on the sky. I bet they do. Thus, pulsars in your 40x40 ROI might
  cluster differently in index and cutoff than those in the rest of
  the sky $<\sigma v> = 3\cdot 10^{-23}$ cm$^3$ s$^{-1}$. }

\reply{As mentioned in the new appendix, the correlation between the
  spectral parameters and the background model appears to be modest
  (the correlation coefficient between the normalization of the
  Galactic diffuse and the spectral parameters for the new sources
  ranges from $-0.25$ to $0.10$).  In addition, we have followed the
  referee's suggestion and looked at the agreement between the energy
  fluxes in the low-energy bins and the spectral models, and found no
  evidence of significant biases.  In Figure~\ref{fig:signif_bin}
  (Figure~20 in the new draft) we histogram the residual of the single
  energy bin fluxes with respect to the spectral models, for two
  different energy bins.  For the lowest-energy bin the distribution
  of pulls is very nearly normal ($\mu = 0.05$, $\sigma = 1.0$).  For
  the energy bin near 2~GeV there is a slight bias for the model to
  overestimate the data ($\mu = -0.2$, $\sigma = 1.0$).  Overall we
  believe that these results indicate that the spectra are not
  significantly biased by the backgrounds.  We have added a few
  sentences to the appendix to indicate that we find no evidence of
  systematic deviations from the spectral models in the lowest-energy
  bins.  A more important effect is the difference in the
  $TS_{\rm curv}$ evaluated using the two diffuse emission models and
  this is discussed in the new appendix.}
  

\begin{figure}[!ht]
  \centering
\includegraphics[width=0.55\columnwidth]{../plot/histo_diff.pdf}
\caption{Distribution of the significance of the residual for the
  energy flux in a single bin with respect to the fitted spectral
  model for the PSR candidates found with the Off. IEM analysis.
  This is shown for the lowest-energy bin (black), and for an energy bin
  near the peak of the statistical power of the analysis ($\sim
  2$~GeV, blue).  The solid curves are best-fit Gaussians
  for the two histograms; the best-fit parameters are given in the text 
  of this reply.}
\label{fig:signif_bin} 
\end{figure}



\mypar{Section 3}

\review{It's probably not a worry, but since your ROIs are quite
  small, there will be plenty of sources around the edge losing a
  large fraction of their photons out of the ROI at low energies.}

\reply{We use ROIs with an overlap of 3 degrees between adjacent
  ROIs. Then if a source is found in two ROIs we keep the source that
  is the closest to the center of its ROI in order to counter possible
  edge effects. Furthermore, the LAT likelihood analysis does properly
  treat the leakage of photons out of the ROI.}


\review{I think the 290/469 statistic is a little frightening. Only
  60\% of the sources are "reliable"? And this means the differences
  between the IEMs are so large, the difference to the truth means an
  even smaller number may be real.}

\reply{This is largely attributable to threshold effects; many of the
  sources are faint and are detected with TS near the threshold of
  25.  On the other hand, the requirement $TS_{\rm curv} > 9$ in
  the PSR selection criteria effectively sets a somewhat higher flux threshold,
  as measuring spectral curvature for a faint source
  near the detection threshold is difficult.

  As discussed in the new appendix many sources are detected ($TS>25$)
  with one IEM but are not with the other. To demonstrate this effect
  we show in Figure~\ref{fig:ts_correl} (Figure~15 in the new draft)
  the approximate significance (i.e., the $\sqrt{TS}$) as measured
  with the two different IEMs, considering all source candidates with
  $TS > 16$ in either IEM.  We note the high degree of
  correlation at $|b| > 2.5^\circ$ between the significance as found
  with the two different IEMs; the scatter (i.e., the RMS of the
  difference) is about $1.7 \sigma$ ($4.6 \sigma$ for sources near the
  plane).  Assuming the scatter in significance follows a Gaussian
  distribution, then combined with the observed distribution of
  significance, we would expect about 79\% (of sources detected with
  either IEM to be detected with both.  However, we observe that about
  64\% of sources detected with either IEM are not detected with the
  other IEM.  (These numbers are 58\% and 56\% for sources near the
  plane.)  Since the scatter away from the plane is small compared to
  the $5.0\sigma$ detection threshold, we believe that ratio between
  the 79\% overlap found in our toy simulation and the 64\% overlap
  found in the two cases sets a lower bound that $64/76 = 0.81$ of the
  sources found away from the plane with either IEM are in fact real.
  Along the plane, on the other hand, the scatter is comparable in
  magnitude to the detection threshold.  There we believe that the
  56\% overlap of sources found with both IEMs is a lower bound on the
  fraction of real sources.  We have added a few sentences to make
  these points in the text.  We have added this figure and some
  discussion to the new Appendix~C.}

\begin{figure}[!ht]
  \centering
\includegraphics[width=0.495\columnwidth]{../plot/sqrtTS_correl_mask.pdf}
\includegraphics[width=0.495\columnwidth]{../plot/sqrtTScurv_correl_mask.pdf}
\caption{Left: correlation between the significance ($\sqrt{TS}$)
  values derived using the Off. and Alt. IEMs. The black (red) points
  are for sources with $|b| > 2.5^\circ$ ($|b| < 2.5^\circ$).  Sources
  that are detected with $TS > 16$ when using only one of the two IEMs
  are assigned a $TS$ value near zero for the other IEM for plotting
  purposes.  Right: same, but for $\sqrt{TS_{\rm curv}}$; only sources
  detected with both IEMs are shown.}
\label{fig:ts_correl} 
\end{figure}


\review{I'm also not convinced about this clustering analysis. You've
  identified positive excesses, but there must also be the equivalent
  *negative* excesses. Do you mask these regions out in your
  subsequent likelihood analyses? I propose that using a
  goodness-of-fit metric is a better way to identify problem areas,
  which you can then mask when considering whether or not you should
  find sources there under your various hypotheses. Indeed, as a
  systematic check you might consider fully masking either the entire
  Galactic plane or at least the longitudes where there are arms in
  projection and the IEMs tend not to do a good job.}

\reply{The effect of ``negative'' excesses, i.e., regions where the
  IEM over-predicts the data is quite interesting.  The main effect of
  these regions would be to degrade the source sensitivity within them,
  effectively causing us to underestimate the number of sources in those
  regions. 
  
  As discussed in response to the concerns about the IEMs, it is
  unlikely that under-predictions in the IEMs will manifest themselves as
  single isolated sources but rather as a region with many sources 
  that fill in the under-prediction.  Thus we believe that the clustering
  analysis is the best way to study this effect.  This is further discussed
  in the new appendix, where we show that at the end of the data analysis pipeline 
  the residuals are very nearly normally distributed, and do not seem
  to indicate that there are large regions where the model over-predicted
  the data with both IEMs and effectively masked counts.  

  In any case, we have also performed a version of the source counts likelihood
  analysis with the Galactic plane masked, and even for that case we find that
  the existence of a Galactic bulge PSR population is favored with $TS=44$, 
  corresponding to a level of 6.2 standard deviations.}
  


\begin{figure}[!ht]
  \centering
\includegraphics[width=0.55\columnwidth]{../plot/systmap.pdf}
\caption{The color scale shows the statistical significance of the
  difference between the between the predicted model counts using the
  two IEMs integrated over the range $E = [0.3,500]$~GeV.  Sources
  found with both IEMs are indicated with 'x' markers.  Square
  (circle) markers show sources found only with the Off. (Alt.) IEM.
  The cyan markers show sources that have a corresponding candidate
  with $TS > 16$ using the other IEM.  Green (white) circles and
  squares show sources with $TS < 49$ ($TS \ge 49$).}
\label{fig:systmap} 
\end{figure}

\mypar{Section 4}

\review{Minor -- there are many publication delving into
  characterizing pulsar and blazar populations, some for the dedicated
  purpose of radio searches and some for broader cataloging and
  classification efforts. It would be charitable to cite some.}

\reply{We have added a total of five references on the characterization of
  pulsar and blazar populations.}

\review{Moderate -- I return to my low-energy concern here. With the
  poorer quality of IEMs, do sources in your ROI follow the same
  trends as sources in the rest of the sky, where the background is
  more certain?  Or might e.g. the contamination be higher from
  mimicking a higher TS\_PLE from poor spectral estimation?}

\reply{As we discussed earlier in this reply, the spectral fits agree well
  with single-bin energy flux measurements for the lowest energy bins, 
  so we do not believe that the spectral parameters are significantly biased
  by the IEMs.}

\review{Figure 2 -- it would be nice to see the full population of
  blazars on here for interest, perhaps as faint symbols.}

\reply{In this figure we only show blazars with $TS_{\rm curv} > 9$
  since blazars without a measured spectral curvature are excluded in our search for
  pulsars.   Many of those sources are simply faint (i.e., they may
  have spectral curvature, but it can not be significantly detected).  
  For faint sources the quantities plotted in this figure
  ($\Gamma$ and $E_{\rm cut}$) have large uncertainties and would
  blur the picture.  Although we could, in principle, 
  plot blazars down to some lower value of $TS_{\rm curv}$ we would prefer
  to leave the figure as it is.}


\mypar{Section 5}

\review{"Of the 210 identified gamma-ray pulsars for which we have
  good distance estimates" -- whaaaat? There aren't good distance
  estimates for anywhere near that many gamma-ray pulsars.}

\reply{Yes, the wording here was ambiguous and misleading.  Our intended meaning was ``Of the 
  \textbf{fraction} of the 210 identified $\gamma$-ray pulsars for which we have
  good distance estimates...''.  We have clarified the text.}

\review{Minor -- is the 1FGL dN/dS really the most up to date? Why is
  this the adopted one?}

\reply{In fact, the dN/dS from the 1FGL is the only dN/dS that has been derived
  in the energy range used in this analysis.}


\review{You might compare the numbers here to those obtained in the
  papers from Kistler et al.}

\reply{Thank you; we have added some references to the works by
  Kistler et al.  However, a direct comparison is difficult, in
  particular b/c the luminosity function used in Kistler et al.
  is expressed in terms of the spin-down luminosity.}



\mypar{Section 6.1}

\review{I think this is a reasonable approach to the luminosity
  function. The selection bias truly is formidable. It's worth
  comparing the different luminosities for the faintest detected radio
  pulsars vs. the faintest detected blind search pulsars.}

\reply{The faintest $\gamma$-ray pulsar is PSR J0437-4715 with
  $L=3.55\times10^{31} {\rm erg}/{\rm s}$, and the faintest
  $\gamma$-ray pulsar found in a blind search is PSR J1741-2054 with
  $L=1.51\times10^{33} {\rm erg}/{\rm s}$.   We have added a sentence
  giving that information for reference.}


\mypar{Section 7}

\review{Serious: these sensitivities are very important for your
  calculations, but could be subject to the real flaws in the IEM, and
  there don't seem to be any estimates for their uncertainties. One
  quick and dirty check is to see how much your sensitivity estimates
  change if you use one of your IEMs to do the simulation and the
  other to do the fit.}

\reply{In fact, these ``sensitivity estimates'' are measurements of
  the efficiency selection criteria that are derived from numerous
  simulations of a great many sources in the ROI.  Therefore, although
  it would be interesting, re-computing these efficiencies by using one
  IEM to simulate and the other IEM to fit the data would be a very
  large amount of work.  What we have chosen to do instead is attempt
  to quantify the effect on the point source detection, and on the
  spectral parameters used to select PSR quantities, attributed to
  systematic uncertainties of the IEM, and to investigate the differences
  between the results obtained with the two IEMs.}

\review{Moderate: in your likelihood evaluation, you seem to treat the
  sensitivity as constant. However, due to source confusion, it
  actually varies with your model parameters and should be modified in
  those parts of the sky when the model predicts high number
  density. This is probably a small effect, and you might want to mask
  out those parts of the analysis in any case.}

\reply{To be clear, we do not treat the sensitivity as constant.  We
  derive the selection efficiency as a function of source flux as well
  as of direction.  This was already described in the text.  We do,
  however, make the assumption that the concept of selection
  efficiency is valid, i.e., that for a particular direction in
  the sky we will correctly reconstruct the same fraction of sources,
  independent of the local density of sources.  Based on the rest of
  this comment we think that this is the point that the referee is
  raising.  We do mention the issue of source confusion in the 7th
  paragraph of this section.  Specifically we report that source
  confusion affects the efficiency noticeably only at densities greater than 5
  sources per square degree, and that these densities exist only in
  the inner few degrees near the GC.  In all of the simulations the
  source-clustering algorithm finds a cluster of sources very close to
  the GC.  As these sources are removed from further
  consideration, this has the effect of lowering the efficiency in the
  innermost analysis bin.  We have added text to clarify these
  points.}


\mypar{Section 8}

\review{Moderate: why do you use different binning? You can directly
  evaluate the likelihood on an unbinned basis. Your measurements of
  the sensitivity, which do rely on bins, seem like the natural
  scale.}

\reply{The sensitivity (or more precisely the efficiency and 
  flux dispersion) are explicitly calculated in the bins used in likelihood 
  analysis.  That is to say, that we recompute the efficiency and 
  flux dispersion for the different binnings.  So the likelihood 
  analysis does explicitly use the ``natural'' binning, as suggested
  by the referee.  However, we felt it important to show that the 
  results were robust against the choice of this binning.  We have 
  added text to clarify that the efficiency and flux dispersion
  are computed for the binning of the likelihood analysis.}


\mypar{Section 9}

\review{I think this is really interesting! You might consider
  hammering on this point a little further in the discussion.}

\reply{Thank you.  We have expanded this section to elaborate a bit on
  the fact that attributing the GC excess flux to unresolved PSRs
  means that we should be able to deepen the limits on the DM
  annihilation cross section.  However, it is important to keep in
  mind that the DM annihilation signal is brightest in the inner few
  degrees where our analysis is most affected by source confusion.}


\mypar{Section 10}

\review{I think the large discrepancy between the predicted numbers of
  pulsars here and in the literature bears some further
  discussion. Radio pulsars certainly have a longer lifetime, but
  their beams are also tiny when they're old. Papers modeling
  gamma-ray pulsar beams typically find they are *overproduced*
  relative to the expected supernova rate. There's some expectation
  that gamma-ray beams also become smaller as pulsars age; the thinner
  gaps tend to illuminate a belt along the spin equator, but still >
  ~30\% of the sky is illuminated. 2 SN/century, lifetime of 1 MYr,
  efficiency of 30\%... this is a factor of a few higher than your
  findings. So it's not crazy, but still worth a quick discussion.}

\reply{We have added an explicit statement that the exact number of
  $\gamma$-ray pulsars quoted depends very strongly on the lower flux
  integration limit as well as on the energy band used for the
  analysis.  That accounts for the largest part of the discrepancy
  between the number of disk pulsars in our model and the other works
  in the literature.  We would prefer not to speculate on the expected
  number of $\gamma$-ray pulsars based on estimates of the Galactic
  supernova rate and beaming functions.  As the referee points out,
  such estimates are uncertain by a factor of several, while the
  normalizations of the disk and bulge populations we obtain are much
  better constrained, with uncertainties at about the 30\% level.}

\end{document}
 
% LocalWords:  ApJ IEMs GC isn kpc Lorimer NFW DM detections IEM MeV FSSC GeV
% LocalWords:  systematics aren seds ROI ROIs ve blazar PLE blazars whaaaat dN
% LocalWords:  1FGL dS Kistler et al luminosities efficiencies PSR Strigari RMS
% LocalWords:  unbinned binnings thresholding mypar circ curv sqrt
%  LocalWords:  includegraphics columnwidth ts_correl systmap textbf
