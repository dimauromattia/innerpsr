"""Make a plot of the PSR selection efficiency"""
#---------------------------------------
# Routine to make plot for the efficiency
#---------------------------------------
import os.path

import matplotlib.pyplot as pl
import numpy as np
from common import get_plot_dir, get_data_dir, init_style,\
    init_tick_params, create_figure
from labels import LABEL_ABSGLAT, LABEL_EFLUX


densHL = [0.65,0.65,0.65,0.65,0.65]
densGP = [2.80,2.80,2.80,2.80,2.80]
densGC = [14.0,14.0,14.0,14.0,14.0]
eff = [1e-4,1e-2,1e-1,1e0,1e1]

vecname = [0.15,0.2,0.3,0.5,0.7,1.0,2.0,3.0,5.0,7.0,10.0,13.0,16.0,20.0,25.0,30.0]
efficiencyvec=[ 0.54166667,  0.58333333,  0.58333333,  0.54166667,  0.58333333,
        0.54166667,  0.53846154,  0.53571429,  0.5       ,  0.44444444,
        0.375     ,  0.26086957,  0.14      ,  0.05555556,  0.        ,  0.        ]
efficiencyerrvec=[ 0.1502313 ,  0.15590239,  0.15590239,  0.1502313 ,  0.15590239,
        0.1502313 ,  0.1439099 ,  0.13832083,  0.125     ,  0.11111111,
        0.09682458,  0.07530656,  0.05291503,  0.03207501,  0.        ,  0.        ]
        

def make_confrho_plot():
    """Make a plot of the PSR selection efficiency"""
    plotdir = get_plot_dir()
    #datadir = get_data_dir()
    init_style()

    fig = create_figure()
    pl.errorbar(vecname, efficiencyvec,yerr=efficiencyerrvec, fmt="*", color="black", label=r"Simulation")
    pl.plot(densHL, eff , lw=2.0, ls='--', color="red",label=r"$b=15^{\circ}$ and $l=15^{\circ}$")
    pl.plot(densGP, eff , lw=2.0, ls='--', color="blue",label=r"$b=0^{\circ}$ and $l=15^{\circ}$")
    pl.plot(densGC, eff , lw=2.0, ls='--', color="green",label=r"$b=0^{\circ}$ and $l=0^{\circ}$")
    pl.ylabel(r'$\omega$', fontsize=22)
    pl.xlabel(r'Source density [deg$^{-2}$]', fontsize=22)
    pl.axis([0.1,30.0,5e-2,1.2e0], fontsize=22)
    pl.legend(loc=3, prop={'size': 18}, numpoints=1, scatterpoints=1)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('log')
    init_tick_params()
    fig.tight_layout(pad=0.5)
    fig.savefig(os.path.join(plotdir, 'conf_rho.png'),
                bbox_inches='tight')
    fig.savefig(os.path.join(plotdir, 'conf_rho.pdf'),
                bbox_inches='tight')
    pl.close(fig)


make_confrho_plot()
