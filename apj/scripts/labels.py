"""Various definitions to assure consistency between plots"""

LABEL_GLON = r'$l$ ${\rm [deg]}$'
LABEL_GLAT = r'$b$ ${\rm [deg]}$'
LABEL_DIST = r'$d_{\rm GC}$ ${\rm [deg]}$'
LABEL_ABSGLAT = r'$|b|$ ${\rm [deg]}$'
LABEL_COUNTS_ = r'${\rm Counts} / $[0.1^\circ\times 0.1^\circ {\rm pixel}]$'

LABEL_GAMMA = r'$\Gamma$'
LABEL_ECUT = r'$\log_{10}(E_{\rm cut} [MeV])$'

LABEL_DNDL = r'$dN/dL$ $[{\rm erg}^{-1} {\rm s}]$'
LABEL_LUMI = r'$L$ $[{\rm erg} {\rm s}^{-1}]$'

LABEL_EFLUX = r'$S$ $[{\rm MeV} {\rm cm}^{-2}{\rm s}^{-1}]$'
LABEL_EFLUX_TRUE = r'$S^{\rm{true}}$ $[{\rm MeV} {\rm cm}^{-2}{\rm s}^{-1}]$'
LABEL_EFLUX_OBS = r'$S^{\rm{obs}}$ $[{\rm MeV} {\rm cm}^{-2}{\rm s}^{-1}]$'
LABEL_FLUX = r'$S$ $[{\rm ph} {\rm cm}^{-2}{\rm s}^{-1}]$'
LABEL_NSRC_PER_FLUX = r'${\rm Number\ of\ sources}$ / $[{\rm 1/3 decade}]$'
LABEL_NSRC_PER_PSF = r'$N_{\rm sim}$ ${\rm per\ detected\ source}$'
LABEL_NSRC_PER_DEG = r'${\rm Number\ of\ sources}$ / $[1.6^\circ]$'
LABEL_NSRC_PER_DEG2 = r'${\rm Number\ of\ sources}$ / $[{\rm deg}^{2}]$'

LABEL_DNDE = r'$dN/dE$ $[{\rm GeV}^{-1}{\rm cm}^{-2}{\rm s}^{-1}{\rm sr}^{-1}]$'
LABEL_E2DNDE = r'$E^2 dN/dE$ $[{\rm GeV}{\rm cm}^{-2}{\rm s}^{-1}]$'

LABEL_E_MEV = r'${\rm Energy}$ $[{\rm MeV}]$'
LABEL_E_GEV = r'${\rm Energy}$ $[{\rm GeV}]$'

LABEL_DELTA_TS = r'$\Delta {\rm TS}$'
LABEL_TS = r'${\rm TS}$'
