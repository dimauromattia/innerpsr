"""Make a plot the counts map for the analysis region"""
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import PowerNorm
from astropy.wcs import WCS
from astropy.io import fits

from astropy.table import Table
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_GLON, LABEL_GLAT

def make_countmap():
    """Make a plot the counts map for the analysis region"""
    plotdir = get_plot_dir()
    datadir = get_data_dir()

    init_style()

    fig = plt.figure()

    hdulist = fits.open(os.path.join(datadir,
                                     'ccube_00.fits'))
    tab = Table.read(os.path.join(datadir,
                                  'GCPass8_listsources_cleaned06deg_TS10_tsmap_300MeV_fit3TS10.fits'))

    wcs = WCS(hdulist[0].header).dropaxis(2)
    ax = fig.add_subplot(111, projection=wcs)

    data = hdulist[0].data[0]
    im = ax.imshow(data, interpolation='nearest',
                   norm=PowerNorm(gamma=0.4),
                   origin='lower', vmax=200, cmap='magma')
    cb = plt.colorbar(im, ticks=[0,1,5,20,50,100,200])
    cb.set_label('Counts/Pixel')

    ax.coords.grid(color='white',linestyle=':')
    ax.set_xlabel(LABEL_GLON)
    ax.set_ylabel(LABEL_GLAT)

    data_cat = fits.open(datadir+'/catalog_300MeV_OffIEM_paper_allcat_flags.fits')
    datacat = data_cat[1].data
    glonn = datacat.field('GLON')
    glatt = datacat.field('GLAT')
    cluster = datacat.field('Cluster')
    fgl = datacat.field('3FGL flag')
    psrcand = datacat.field('PSR cand')

    psrs = (tab['class'] == 'PSR') + (tab['class'] == 'psr')
    glon_3fgl = tab['GLON'][psrs]
    glat_3fgl = tab['GLAT'][psrs]
    indexpsr3fgl = [316,210,57,301,173,177,371,53,163,295,337,69,130,124,83,42,354,267]

    #m_cand = np.zeros(len(cluster),dtype=bool)
    m_new_nocand = np.zeros(len(cluster),dtype=bool)
    m_new_cand = np.zeros(len(cluster),dtype=bool)
    m_3fgl_nocand = np.zeros(len(cluster),dtype=bool)
    m_3fgl_cand = np.zeros(len(cluster),dtype=bool)
    m_cluster = np.zeros(len(cluster),dtype=bool)
    m_3fglpsr = np.zeros(len(cluster),dtype=bool)
    for i in range(len(fgl)):
        check3fglpsr=0
        #print row['3FGL flag'],np.float(row['3FGL flag'])=='1.0'
        for u in range(len(indexpsr3fgl)):
            if i==indexpsr3fgl[u]-1:
                m_3fglpsr[i] =  True
                check3fglpsr=1
        if fgl[i]=='1.0' and psrcand[i]=='0.0':
            m_3fgl_nocand[i] = True        
        if fgl[i]=='1.0' and psrcand[i]=='1.0' and check3fglpsr==0:
            m_3fgl_cand[i] = True
        if fgl[i]=='0.0' and psrcand[i]=='1.0':
            m_new_cand[i] =  True        
        if fgl[i]=='0.0' and psrcand[i]=='0.0':
            m_new_nocand[i] =  True
        if cluster[i]=='1.0':
            m_cluster[i] =  True
        #if psrcand[i]=='1.0' and m_3fgl[i] == True:
        #    m_cand[i] =  True

    m_3fgl_nocand &= ~m_cluster
    m_3fgl_cand &= ~m_cluster
    m_new_cand &= ~m_cluster
    m_new_nocand &= ~m_cluster
    m_3fglpsr &= ~m_cluster

    marker_cand='*'
    marker_psr3fgl='*'
    marker_nocand='s'
    marker_cluster='.'
    marker='o'
    markersize=8
    markersize_psr3fgl=10
    markersize_cand=10
    markersize_nocand=6
    markersize_cluster=10
    markeredgewidth=1.0

    #print glonn[m_cluster],glatt[m_cluster]

    glonc = [-0.5,6.2,8.5,13.0]
    glatc = [0.0,0.0,0.0,0.3]

    # 3FGL Non-PSR
    ax.plot(glonn[m_3fgl_nocand],glatt[m_3fgl_nocand], transform=ax.get_transform('world'),
            marker=marker_nocand, color='w',linestyle='None',
            markersize=markersize_nocand,markeredgecolor='k',
            markeredgewidth=markeredgewidth)

    # 3FGL PSR
    ax.plot(glonn[m_3fgl_cand],glatt[m_3fgl_cand], transform=ax.get_transform('world'),
            marker=marker_cand, color='w',linestyle='None',
            markersize=markersize_cand,markeredgecolor='k',
            markeredgewidth=markeredgewidth)

    # 3FGL IDENTIFIED PSR
    ax.plot(glonn[m_3fglpsr],glatt[m_3fglpsr], transform=ax.get_transform('world'),
            marker=marker_cand, color='blue',linestyle='None',
            markersize=markersize_cand,markeredgecolor='blue',
            markeredgewidth=markeredgewidth)

    # New Non-PSR 
    ax.plot(glonn[m_new_nocand],glatt[m_new_nocand], transform=ax.get_transform('world'),
            marker=marker_nocand, color='lime',linestyle='None',
            markersize=markersize_nocand,markeredgecolor='k',
            markeredgewidth=markeredgewidth)

    # New PSR
    ax.plot(glonn[m_new_cand],glatt[m_new_cand], transform=ax.get_transform('world'),
            marker=marker_cand, color='lime',linestyle='None',
            markersize=markersize_cand,markeredgecolor='k',
            markeredgewidth=markeredgewidth)

    #ax.plot(glonn[m_cand],glatt[m_cand], transform=ax.get_transform('world'),marker=marker, color='blue',linestyle='None', markersize=markersize)
    ax.scatter(glonc[0],glatc[0], transform=ax.get_transform('world'),
               edgecolors='purple', s=600,facecolors='none', lw=2.0)
    ax.scatter(glonc[1],glatc[1], transform=ax.get_transform('world'), 
               edgecolors='purple', s=300,facecolors='none', lw=2.0)
    ax.scatter(glonc[2],glatc[2], transform=ax.get_transform('world'), 
               edgecolors='purple', s=300,facecolors='none', lw=2.0)
    ax.scatter(glonc[3],glatc[3], transform=ax.get_transform('world'),
               edgecolors='purple', s=250,facecolors='none', lw=2.0)

    # Clusters
    ax.plot(glonn[m_cluster],glatt[m_cluster], transform=ax.get_transform('world'),
            marker=marker_cluster, color='purple',linestyle='None', 
            markersize=markersize_cluster,markeredgecolor='k',
            markeredgewidth=markeredgewidth)

    #ax.plot(tab['glon'][~m],tab['glat'][~m], transform=ax.get_transform('world'),marker='o', color='w',
    #        markerfacecolor='None',markeredgecolor='w', linestyle='None')

    extent = im.get_extent()
    ax.set_xlim(extent[0], extent[1])
    ax.set_ylim(extent[2], extent[3])

    fig.savefig(os.path.join(plotdir,'countmap.png'), bbox_inches='tight')
    fig.savefig(os.path.join(plotdir,'countmap.pdf'), bbox_inches='tight')
    plt.close(fig)


make_countmap()

