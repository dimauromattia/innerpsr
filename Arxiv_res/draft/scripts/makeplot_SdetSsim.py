"""Make plots showing correlation between true and measured flux"""
#---------------------------------------
# script to analyze detected PSRs regardless TScurv
#---------------------------------------
import numpy as np
import matplotlib.pyplot as pl
from matplotlib.colors import LogNorm


from astropy.io import fits
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_EFLUX_OBS, LABEL_EFLUX_TRUE

plotdir = get_plot_dir()
datadir = get_data_dir()

init_style()


def dNdEen(En, Gamma, Ecut):
    """Compute and return the dN/dE as a function of energy"""
    En = np.power(10., En)
    return En * np.power(En, -Gamma) * np.exp(-En / Ecut) * np.log(10.) * En


kpc = 1e3 * 3.0856775 * 1e18  # cm
pigreco = 3.14159
solidangle = 27143.6
solidanlgetot = 41252.996
erg = 624.151  # GeV

data_psr = fits.open(datadir + "/Ssim_Sdet_paper.fits")
datapsr = data_psr[1].data

#glatpsr  = datapsr.field('GLAT')
eflux_sim = datapsr.field('eflux_sim')
eflux_det = datapsr.field('eflux_det')
efluxerr_det = datapsr.field('efluxerr_det')
Nmodel = np.array([ 88, 79, 78, 76, 72, 74, 179, 115])

Sline = [1e-7, 1e-6, 1e-5, 1e-4, 1e-3]

def makeplot_SdetSsim():
    """Make plot showing correlation between true and measured flux"""
    
    includeNmodel = False
    fig = create_figure()
    #pl.errorbar(eflux_sim, eflux_det, yerr=efluxerr_det, fmt="o", color="black")

    #bins = np.logspace(-6., -4., 25)
    bins = np.array([1.00000000e-06, 1.46779927e-06, 2.15443469e-06,
                    3.16227766e-06, 4.64158883e-06, 6.81292069e-06,
                    1.00000000e-05, 3.16227766e-05, 1.00000000e-04])
    
    counts, _, _ = np.histogram2d(eflux_sim, eflux_det, bins=bins)
    
    ax = fig.add_subplot(111)

    counts = (counts.T / Nmodel).T

    if includeNmodel:
        counts *= Nmodel
        vmax = 200.
        vmin = 1e-1
        ticks=[1e-1, 1, 1e1, 1e2]
    else:
        vmax = 1.0
        vmin = 1e-2
        ticks=[1e-2, 5e-2, 1e-1, 5e-1, 1]

    pl.axis([1e-6, 1e-4, 1e-6, 1e-4], fontsize=22)
    pl.yscale('log')
    pl.xscale('log')

    #pl.hist2d(eflux_sim, eflux_det, bins=bins)
    im = ax.pcolormesh(bins, bins, counts.T, norm=LogNorm(), vmin=vmin, vmax=vmax)
    cb = pl.colorbar(im, ticks=ticks)

    pl.plot(Sline, Sline, ls='--', color="blue")
    #pl.errorbar(binvec, histodet[0],yerr = np.sqrt(histodet[0]),
    #            fmt="*", color="blue",label="Detected")
    pl.legend(loc=1, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'$S^{\rm{true}}/S^{\rm{obs}}$ PSR', fontsize=18)
    pl.ylabel(LABEL_EFLUX_OBS, fontsize=22)
    pl.xlabel(LABEL_EFLUX_TRUE, fontsize=22)
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5) 
    pl.savefig(plotdir + "/SsimSdet_detected_GC.pdf")
    pl.savefig(plotdir + "/SsimSdet_detected_GC.png")
    pl.close()

makeplot_SdetSsim()
