"""Common functions for making plots for papers"""
import os
import matplotlib.pyplot as plt
import matplotlib


def get_plot_dir():
    """Return the path to the directory to write plots to"""
    return os.path.join(os.path.dirname(__file__), '../plot_temp')


def get_data_dir():
    """Return the path to the directory with plot input data"""
    return os.path.join(os.path.dirname(__file__), '../data')


def restore_default_style():
    """Restore the default plotting style"""
    matplotlib.rcParams.update(matplotlib.rcParamsDefault)


def init_style(params=None):
    """Initialize the default style for all matplotlib plots."""

    default_params = {
        #'backend': 'eps',
        'axes.labelsize': 18,
        'xtick.color': 'k',
        'ytick.color': 'k',
        'xtick.labelsize': 18,
        'ytick.labelsize': 18,
        'xtick.major.size': 7.0,
        'xtick.minor.size': 4.0,
        'xtick.major.width': 1.0,
        'xtick.minor.width': 1.0,
        'ytick.major.size': 7.0,
        'ytick.minor.size': 4.0,
        'ytick.major.width': 1.0,
        'ytick.minor.width': 1.0,
        #'xtick.major.size': 3.5,
        #'xtick.minor.size': 2.0,
        #'xtick.major.width': 0.8,
        #'xtick.minor.width': 0.6,
        #'ytick.major.size': 3.5,
        #'ytick.minor.size': 2.0,
        #'ytick.major.width': 0.8,
        #'ytick.minor.width': 0.6,
        'text.usetex': True,
        'figure.figsize': (8, 6),
        'font.serif': 'Computer Modern Roman',
        'font.size': 18,
        'legend.frameon': False,
        'legend.numpoints': 1,
        'legend.scatterpoints': 1,
        #'legend.loc' : 1,
        'legend.fontsize' : 9,
        'image.cmap' : 'viridis'
    }

    if params is not None:
        default_params.update(params)

    matplotlib.rcParams.update(default_params)

    #plt.tick_params(which='both', width=2)
    #plt.tick_params(which='major', length=7)
    #plt.tick_params(which='minor', length=4)


def init_tick_params():
    """Initialize config of tick marks or plot"""
    plt.tick_params('both', length=10, width=2, which='major', color='blue')
    plt.tick_params('both', length=6, width=1.5, which='minor', color='blue')


def create_figure(figsize=(9, 6)):
    """Create a figure.  By default this is 9x6, a reasonable size for plots in papers"""
    fig = plt.figure(figsize=figsize)
    return fig
