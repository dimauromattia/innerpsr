
import os
import numpy as np
from astropy.io import fits

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from common import get_plot_dir, get_data_dir, init_style, create_figure

from fermipy.utils import load_yaml, write_yaml
init_style()

datadir = get_data_dir()
plotdir = get_plot_dir()


def rebin_array_2d(data, factor=8):
    nx = int(data.shape[0]/factor)
    ny = int(data.shape[1]/factor)
    outdata = np.ndarray((nx,ny))
    for i in range(nx):
        x_0 = i * factor
        x_1 = x_0 + factor
        for j in range(ny):
            y_0 = j * factor
            y_1 = y_0 + factor           
            outdata[i,j] = data[x_0:x_1,y_0:y_1].sum() / factor
    return outdata


def histogram_resid(data, bins):
    #rebinned = rebin_array_2d(data)
    #histo = np.histogram(rebinned.flat, bins)
    histo = np.histogram(data.flat, bins)
    return histo[0]

def histogram_files(flist, bins):
    histo = np.zeros(len(bins)-1)
    for fname in flist:
        ff = fits.open(fname)
        histo += histogram_resid(ff[0].data, bins)
    return histo

def make_flist(roilist, suffix):
    flist = []
    for roi in roilist:
        fname = "rois/syst_signif_roi%s_%s_rebin.fits"%(roi, suffix)
        flist.append(fname)
    return flist

def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))


def fit_gauss(hist, bin_centers):
    p0 = [1.2e4, 0., 1.]
    coeff, var_matrix = curve_fit(gauss, bin_centers, hist, p0)
    return coeff, var_matrix


def plot_histos(histo_off, histo_ref, bin_centers):
    fig = create_figure()
    ax = fig.add_subplot(111)
    plt.xlabel(r'Residual [$\sigma$]')
    plt.ylabel(r'Pixels per $0.2 \sigma$')
    #plt.axis([-5, 5., 0., 1.2e4])
   
    ax.errorbar(bin_centers, histo_off, yerr=np.sqrt(histo_off), color='r', fmt='o', label="Off. IEM")
    ax.errorbar(bin_centers, histo_ref, yerr=np.sqrt(histo_ref), color='b', fmt='o', label="Alt. IEM")

    fit_off = fit_gauss(histo_off, bin_centers)
    fit_ref = fit_gauss(histo_ref, bin_centers)

    print ("Off. ", fit_off[0], np.sqrt(fit_off[1][1,1]), np.sqrt(fit_off[1][2,2]))
    print ("Alt. ", fit_ref[0], np.sqrt(fit_ref[1][1,1]), np.sqrt(fit_ref[1][2,2]))
    
    gauss_off = gauss(bin_centers, *fit_off[0])
    gauss_ref = gauss(bin_centers, *fit_ref[0])
  
    ax.plot(bin_centers, gauss_off, color='r')
    ax.plot(bin_centers, gauss_ref, color='b')

    leg = plt.legend(prop={'size': 16})
    return fig, ax, leg


def make_hist():

    remake_hist = True
    yaml_file = os.path.join(datadir, 'resid_histogram.yaml')

    if remake_hist:
        bins = np.linspace(-8.0, 8.0, 81)
        bin_centers = (bins[0:-1] + bins[1:])/2.
        
        roi_list = ['3', '11', '19', '27', '35', '43', '51', '59']
        roi_list += ['4', '12', '20', '28', '36', '44', '52', '60']
        
        flist_off = make_flist(roi_list, "off")
        flist_ref = make_flist(roi_list, "ref")
        
        histo_off = histogram_files(flist_off, bins)
        histo_ref = histogram_files(flist_ref, bins)

        d = dict(histo_off=histo_off,
                 histo_ref=histo_ref,
                 bins=bins)

        write_yaml(d, yaml_file)
    else:
        d = load_yaml(yaml_file)
        histo_off = d['histo_off']
        histo_ref = d['histo_ref']
        bins = d['bins']
        bin_centers = (np.array(bins[0:-1]) + np.array(bins[1:]))/2.
        
    plot = plot_histos(histo_off, histo_ref, bin_centers)
    
    plot[0].savefig(os.path.join(plotdir, 'resid_hist_plane_rois.png'), bbox_inches='tight')
    plot[0].savefig(os.path.join(plotdir, 'resid_hist_plane_rois.pdf'), bbox_inches='tight')


make_hist()
