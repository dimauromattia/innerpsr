"""Make plots showing results from simulated analysis"""

#---------------------------------------
# script which create xml file
#---------------------------------------
import matplotlib.pyplot as pl
import numpy as np
from scipy.integrate import romberg
from scipy.interpolate import interp1d
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_E2DNDE, LABEL_E_GEV, LABEL_DNDE, LABEL_GLAT,\
    LABEL_NSRC_PER_FLUX, LABEL_EFLUX

kpc = 1e3 * 3.0856775 * 1e18  # cm
pigreco = 3.14159
solidangle = 27143.6
solidanlgetot = 41252.996
erg = 624.151  # GeV

numsim = 100


def functheta(theta):
    """Return the cos of theta"""
    return np.cos(theta)

solidanglegc = 4. * (pigreco * 20. / 180.) * romberg(functheta,
                                                     pigreco * 0. / 180., pigreco * 20. / 180.)
solidanglegccalore = 4. * (pigreco * 20. / 180.) * \
    romberg(functheta, pigreco * 2. / 180., pigreco * 20. / 180.)

tot = 4. * (pigreco * 180. / 180.) * romberg(functheta,
                                             pigreco * 0. / 180., pigreco * 90. / 180.)

spectrum_tot_bf_best = np.array([  1.28754130e-007,   1.43245542e-007,   1.63192978e-007,
         1.88109702e-007,   2.15699553e-007,   2.40422111e-007,
         2.52800351e-007,   2.41694901e-007,   2.01191812e-007,
         1.38765636e-007,   7.50406619e-008,   3.00949632e-008,
         8.57070304e-009,   1.68752173e-009,   2.16920794e-010,
         1.57147286e-011,   5.43281527e-013,   1.03820575e-014,
         8.96993032e-017,   5.53338933e-020,   3.41452810e-025,
         1.39663054e-033,   5.23619746e-047,   2.03493027e-068,
         1.75250924e-102])

spectrum_tot_bf_low = np.array([  9.01278912e-008,   1.00271879e-007,   1.14235085e-007,
         1.31676792e-007,   1.50989688e-007,   1.68295478e-007,
         1.76960245e-007,   1.69186431e-007,   1.40834269e-007,
         9.71359448e-008,   5.25284634e-008,   2.10664742e-008,
         5.99949212e-009,   1.18126521e-009,   1.51844557e-010,
         1.10003101e-011,   3.80297070e-013,   7.26744025e-015,
         6.27895119e-017,   3.87337253e-020,   2.39016967e-025,
         9.77641376e-034,   3.66533822e-047,   1.42445119e-068,
         1.22675647e-102])


spectrum_tot_bf_up = np.array([  1.93131195e-007,   2.14868314e-007,   2.44789466e-007,
         2.82164554e-007,   3.23549330e-007,   3.60633166e-007,
         3.79200526e-007,   3.62542352e-007,   3.01787718e-007,
         2.08148454e-007,   1.12560994e-007,   4.51424447e-008,
         1.28560545e-008,   2.53128259e-009,   3.25381191e-010,
         2.35720930e-011,   8.14922288e-013,   1.55730862e-014,
         1.34548954e-016,   8.30008400e-020,   5.12179215e-025,
         2.09494581e-033,   7.85429618e-047,   3.05239540e-068,
         2.62876388e-102])

spectrum_tot_band_best = np.array([  1.57279413e-007,   1.82770220e-007,   2.14462749e-007,
         2.51193888e-007,   2.88799223e-007,   3.18372541e-007,
         3.26399465e-007,   3.00159086e-007,   2.38445251e-007,
         1.57981108e-007,   8.41906997e-008,   3.40033744e-008,
         9.44360733e-009,   1.63425860e-009,   1.65891477e-010,
         9.15530431e-012,   2.90398891e-013,   4.30103951e-015,
         1.09500186e-017,   1.00914138e-021,   6.86152757e-028,
         3.91250008e-037,   1.09447820e-051,   7.60925725e-075,
         1.19714237e-111])

spectrum_tot_band_low = np.array([  5.81933828e-008,   6.76249816e-008,   7.93512172e-008,
         9.29417386e-008,   1.06855713e-007,   1.17797840e-007,
         1.20767802e-007,   1.11058862e-007,   8.82247428e-008,
         5.84530101e-008,   3.11505589e-008,   1.25812485e-008,
         3.49413471e-009,   6.04675683e-010,   6.13798464e-011,
         3.38746259e-012,   1.07447590e-013,   1.59138462e-015,
         4.05150689e-018,   3.73382309e-022,   2.53876520e-028,
         1.44762503e-037,   4.04956933e-052,   2.81542518e-075,
         4.42942677e-112])

spectrum_tot_band_up = np.array([  2.33297796e-007,   2.71109160e-007,   3.18119745e-007,
         3.72604267e-007,   4.28385514e-007,   4.72252602e-007,
         4.84159207e-007,   4.45235978e-007,   3.53693789e-007,
         2.34338644e-007,   1.24882871e-007,   5.04383386e-008,
         1.40080175e-008,   2.42415026e-009,   2.46072357e-010,
         1.35803681e-011,   4.30758355e-013,   6.37987527e-015,
         1.62425276e-017,   1.49689304e-021,   1.01779326e-027,
         5.80354178e-037,   1.62347599e-051,   1.12870649e-074,
         1.77576118e-111])

latitudeprofile_bf_best = np.array([  6.39861433e-06,   1.52515850e-06,   9.44369320e-07,
         5.69584357e-07,   3.67868600e-07,   2.64467511e-07,
         1.82416559e-07,   1.47835179e-07,   8.49694371e-08,
         3.71386927e-08])

latitudeprofile_bf_low = np.array([  4.47903003e-06,   1.06761095e-06,   6.61058523e-07,
         3.98709049e-07,   2.57508020e-07,   1.85127258e-07,
         1.27691592e-07,   1.08159624e-07,   6.27511059e-08,
         2.59970849e-08])

latitudeprofile_bf_up = np.array([  9.59792145e-06,   2.28773774e-06,   1.41655398e-06,
         8.54376531e-07,   5.51802900e-07,   3.96701267e-07,
         2.73624837e-07,   2.35777769e-07,   1.34466655e-07,
         5.57080390e-08])
                           
latitudeprofile_band_best = np.array([   8.41030327e-06,   2.18871335e-06,   9.79827759e-07,
         6.92174757e-07,   4.00005328e-07,   3.39608834e-07,
         3.22148918e-07,   2.50557855e-07,   1.44823850e-07,
         1.26358420e-07])

latitudeprofile_band_low = np.array([  3.78463647e-06,   9.84921005e-07,   4.40922492e-07,
         3.11478641e-07,   1.80002397e-07,   1.52823975e-07,
         1.45967013e-07,   1.10951035e-07,   6.51707323e-08,
         5.68612892e-08])

latitudeprofile_band_up = np.array([  1.85026672e-05,   4.81516936e-06,   2.15562107e-06,
         1.52278447e-06,   8.80011721e-07,   7.47139435e-07,
         7.24872762e-07,   5.5427280e-07,   3.18612469e-07,
         2.77988525e-07])

NS_best = np.array([   2.36016000e+03,   1.72096000e+03,   1.25936000e+03,
         9.41120000e+02,   6.67200000e+02,   4.84160000e+02,
         3.40800000e+02,   2.60800000e+02,   1.80000000e+02,
         1.28480000e+02,   1.03360000e+02,   6.83200000e+01,
         4.20800000e+01,   2.94400000e+01,   2.12800000e+01,
         1.05600000e+01,   7.20000000e+00,   3.84000000e+00,
         2.40000000e+00,   1.12000000e+00,   3.20000000e-01,
         1.60000000e-01,   6.40000000e-01,   3.20000000e-01,
         3.20000000e-01])

NS_low = np.array([  1.18008000e+03,   8.60480000e+02,   6.29680000e+02,
          4.70560000e+02,   3.33600000e+02,   2.42080000e+02,
          1.70400000e+02,   1.30400000e+02,   9.00000000e+01,
          6.42400000e+01,   5.16800000e+01,   3.41600000e+01,
          2.10400000e+01,   1.47200000e+01,   1.06400000e+01,
          5.28000000e+00,   3.60000000e+00,   1.92000000e+00,
          1.20000000e+00,   5.60000000e-01,   1.60000000e-01,
          8.00000000e-02,   3.20000000e-01,   1.60000000e-01,
          1.60000000e-01])


NS_up = np.array([  4.02702300e+03,   2.93638800e+03,   2.14878300e+03,
         1.60578600e+03,   1.13841000e+03,   8.26098000e+02,
         5.81490000e+02,   4.44990000e+02,   3.07125000e+02,
         2.19219000e+02,   1.76358000e+02,   1.16571000e+02,
         7.17990000e+01,   5.02320000e+01,   3.63090000e+01,
         1.80180000e+01,   1.22850000e+01,   6.55200000e+00,
         4.09500000e+00,   1.91100000e+00,   5.46000000e-01,
         2.73000000e-01,   1.09200000e+00,   5.46000000e-01,
         5.46000000e-01])

#NS_psr = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 39, 66, 68, 72, 50, 45, 30,
#                   21, 10, 3, 2, 1, 0, 0, 0])
NS_psr = np.array([ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  5, 15, 36, 39, 45, 30,
        20, 10,  3,  2,  1,  0,  0,  0])
NS_psrcurv = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 39, 66, 70,
                       102, 127, 139, 55, 26, 11, 3, 2, 1, 0, 0, 0])


latitude_bin = np.arange(0., 21., 2.0)
latitude_center = np.arange(0.5, 20.5, 2.0)

datadir = get_data_dir()

data = np.loadtxt(datadir + '/caloreetal2014_GC_latitudeprofile.dat')
latitudecalore = data[:, 0]
fluxcalore = data[:, 1]

data = np.loadtxt(datadir + '/Pass8GC_GC_latitudeprofile_NFW.dat')
latitudeNFW = data[:, 0]
fluxNFW = data[:, 1] / 4.

data = np.loadtxt(datadir + '/Pass8GC_GC_latitudeprofile_sample.dat')
latitudesample = data[:, 0]
fluxsample = data[:, 1] / 4.



#tot = 4.*(pigreco*180./180.)*romberg(functheta, pigreco*0./180.,pigreco*90./180.)
print solidanglegc, tot

Enval = np.power(10., np.arange(-1., 4., 0.2))

data = np.loadtxt(datadir + '/caloreetal2014_GC.dat')
ebins = data[:, 0:2]  # Energy bins [GeV]
emeans = ebins.prod(axis=1)**0.5  # Geometric mean energy [GeV]
de = ebins[:, 1] - ebins[:, 0]  # Energy bin width [GeV]
flux = data[:, 2]  # (Average flux)*E^2 in energy bin [GeV/cm2/s/sr]
flux_err = data[:, 3]  # Flux error [GeV/cm2/s/sr]
# Only empirical component [(GeV/cm2/s/sr)^2]
empirical_variance = data[:, 4:28]
# Variance as it enters the spectral fit [(GeV/cm2/s/sr)^2]
full_variance = data[:, 28:]
empirical_sigma = np.sqrt(np.diagonal(empirical_variance))  # Diagonal elements
full_sigma = np.sqrt(np.diagonal(full_variance))  # Diagonal elements

data = np.loadtxt(datadir + '/Pass8GC_GC_excess_lowband.dat')
energylow = data[:, 0]
fluxpass8low = data[:, 1]

data = np.loadtxt(datadir + '/Pass8GC_GC_excess_upband.dat')
energyup = data[:, 0]
fluxpass8up = data[:, 1]

data = np.loadtxt(datadir + '/Pass8GC_GC_excess_mean.dat')
energyav = data[:, 0]
fluxpass8av = data[:, 1]

fup = interp1d(energyup, fluxpass8up)
flow = interp1d(energylow, fluxpass8low)
energyp8 = np.power(10., np.arange(np.log10(1.21e-1), np.log10(7e2), 0.1))
fintup = np.zeros(len(energyp8))
fintlow = np.zeros(len(energyp8))
for r in range(len(energyp8)):
    fintup[r] = fup(energyp8[r])
    fintlow[r] = flow(energyp8[r])


plotdir = get_plot_dir()
init_style()


def make_excess_spectrum_plot_bestfitbulgedisk():
    """Make plot showing the spectrum of the GC excess w.r.t. simulations """

    fig = create_figure()
    #pl.plot(Enval, solidanglegccalore*spectrum_tot*np.power(Enval,2.)/numsim,
    #        ls='--', lw='2.0', color="black",label=r'SIM')
    #pl.plot(Enval, spectrum_tot_allsky*np.power(Enval,2.)/1.,
    #        ls='-', lw='2.0', color="magenta",label='All sky')
    pl.errorbar(energyav, fluxpass8av, fmt="o",
                color="red", label='Ajello et al. (2017)')
    pl.fill_between(energyp8, fintlow, fintup, color="#FF8C00", alpha=0.3)
    # pl.plot(energyup, fluxpass8up*0.01, ls='-', lw='6.0', color="#FF8C00",
    #         label=r'Fermi Pass 8 Systematics')
    #pl.plot(energylow, fluxpass8low, ls='--', lw='2.0', color="orange")
    pl.errorbar(emeans, flux * solidanglegccalore, xerr=de / 2.,
                yerr=flux_err, fmt="o", color="blue", label='Calore et al. 2015b')
    pl.fill_between(emeans, (flux - full_sigma) * solidanglegccalore,
                    (flux + full_sigma) * solidanglegccalore, color="#00FFFF", alpha=0.5)
    # BEST FIT
    pl.plot(Enval, spectrum_tot_bf_best, ls='--', lw=3.0, color="black",
            label=r'$N_{\rm{bulge}}=1330$ and $\alpha=2.60$')
    pl.fill_between(Enval, spectrum_tot_bf_low,
                    spectrum_tot_bf_up, color="#A9A9A9", alpha=0.5)
    #pl.plot(Enval, solidanglegccalore*spectrum_tot_bf*np.power(Enval,2.)/numsim,
    #    ls='--', lw=3.0, color="black",label=r'$N_{\rm{bulge}}=810$ and $\alpha=2.60$')
    #pl.fill_between(Enval,
    #    solidanglegccalore*(spectrum_tot_bf-spectrum_tot_bf*0.3)*
    #    np.power(Enval,2.)/numsim,solidanglegccalore*
    #    (spectrum_tot_bf+spectrum_tot_bf*0.5)*np.power(Enval,2.)/numsim,
    #    color="#A9A9A9", alpha=0.5)
    pl.legend(loc=1, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'$\gamma$-ray spectrum of bulge PSR population', fontsize=20)
    pl.ylabel(LABEL_E2DNDE, fontsize=22)
    pl.xlabel(LABEL_E_GEV, fontsize=22)
    pl.axis([0.3, 100., 4e-9, 5e-6], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('log')
    pl.xticks(fontsize=26)
    pl.yticks(fontsize=26)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)

    pl.savefig(plotdir + "/spectrum_GCexcess_bulge_bestfitbulgedisk.pdf")
    pl.savefig(plotdir + "/spectrum_GCexcess_bulge_bestfitbulgedisk.png")


def make_excess_spectrum_plot_bandbulgedisk():
    """Make plot showing the spectrum of the GC excess w.r.t. best-fit simulations """

    fig = create_figure()
    #pl.plot(Enval, solidanglegccalore*spectrum_tot*
    #        np.power(Enval,2.)/numsim, ls='--', lw='2.0', color="black",label=r'SIM')
    #pl.plot(Enval, spectrum_tot_allsky*np.power(Enval,2.)/1.,
    #        ls='-', lw='2.0', color="magenta",label='All sky')
    pl.errorbar(energyav, fluxpass8av, fmt="o",
                color="red", label='Ajello et al. (2017)')
    pl.fill_between(energyp8, fintlow, fintup, color="#FF8C00", alpha=0.3)
    #pl.plot(energyup, fluxpass8up*0.01, ls='-', lw='6.0',
    #        color="#FF8C00",label=r'Fermi Pass 8 Systematics')
    #pl.plot(energylow, fluxpass8low, ls='--',
    #        lw='2.0', color="orange")
    pl.errorbar(emeans, flux * solidanglegccalore, xerr=de / 2.,
                yerr=flux_err, fmt="o", color="blue", label='Calore et al. 2015b')
    pl.fill_between(emeans, (flux - full_sigma) * solidanglegccalore,
                    (flux + full_sigma) * solidanglegccalore, color="#00FFFF", alpha=0.5)
    # BEST FIT
    #pl.plot(Enval, solidanglegccalore*spectrum_tot_band*
    #        np.power(Enval,2.)/numsim, ls='--', lw=3.0,
    #        color="black",label=r'$N_{\rm{bulge}}=810$ and $\alpha=2.60$')
    #pl.fill_between(Enval, solidanglegccalore*
    #                (spectrum_tot_band-spectrum_tot_band*0.3)*np.power(Enval,2.)/numsim,
    #                solidanglegccalore*(spectrum_tot_band+spectrum_tot_band*0.5)*
    #                np.power(Enval,2.)/numsim, color="#A9A9A9", alpha=0.5)

    # BAND
    pl.plot(Enval, spectrum_tot_band_best, ls='--', lw=2.0, color="black",
            label=r'$N_{\rm{bulge}}=1700$, $\alpha=2.60$, $\beta=1.7$')
    pl.fill_between(Enval, spectrum_tot_band_low,
                    spectrum_tot_band_up, color="#A9A9A9", alpha=0.7)
    pl.legend(loc=1, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'$\gamma$-ray spectrum of bulge PSR population', fontsize=20)
    pl.ylabel(LABEL_E2DNDE, fontsize=22)
    pl.xlabel(LABEL_E_GEV, fontsize=22)
    pl.axis([0.3, 100., 4e-9, 5e-6], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('log')
    pl.xticks(fontsize=26)
    pl.yticks(fontsize=26)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)
    pl.savefig(plotdir + "/spectrum_GCexcess_bulge_bandbulgedisk.pdf")
    pl.savefig(plotdir + "/spectrum_GCexcess_bulge_bandbulgedisk.png")



def make_excess_lat_profile_plot():
    """Make plot showing the latitude profile of the GC excess w.r.t. simulations """


    fig = create_figure()
    #pl.plot(latitude_center,latitudeprofile, ls='--',
    #        lw='2.0', color="black",label=r'$|b|,|l|<20^{\circ}$')
    #pl.plot(latitude_center,latitudeprofile, ls='--',
    #        lw=3.0, color="black",label=r'$N_{\rm{bulge}}=810$ and $\alpha=2.60$')
    pl.plot(latitude_center, latitudeprofile_band_best, ls='--', lw=3.0,
            color="black", label=r'$N_{\rm{bulge}}=1700$, $\alpha=2.60$, $\beta=1.7$')
    #pl.fill_between(latitude_center,(latitudeprofile-latitudeprofile*0.60),
    #                (latitudeprofile+latitudeprofile*0.80), color="#A9A9A9", alpha=0.5)
    #pl.fill_between(latitude_center,(latitudeprofile_band-latitudeprofile_band*0.3),
    #                (latitudeprofile_band+latitudeprofile_band*0.50),
    pl.fill_between(latitude_center, latitudeprofile_band_low, latitudeprofile_band_up,
                    color="#A9A9A9", alpha=0.5)  # BEST FIT
    #pl.fill_between(latitude_center,(latitudeprofile*0.38),
    #                (latitudeprofile*1.8), color="#A9A9A9", alpha=0.5) #BAND
    #pl.errorbar(latitude_center,latitudeprofile,
    #            fmt="o", color="black", label='Sim')
    pl.fill_between(latitudecalore, 0.75*fluxcalore, 1.25*fluxcalore,
                    color="#00FFFF", alpha=0.5, label='Calore et al. 2015b')
    #pl.plot(latitudecalore, 0.001 * fluxcalore,
    #        color="#00FFFF", lw=4., label='Calore et al. 2015b')
    pl.plot(latitudeNFW, fluxNFW, color='red', lw=2,
            ls='-', label='Ajello et al. (2017): NFW')
    pl.plot(latitudesample, fluxsample, color="blue",
            lw=2.0, ls='-', label='Ajello et al. (2017): Sample')
    pl.legend(loc=1, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'Latitude profile of $\gamma$-ray spectrum for bulge PSR population at 2 GeV',
    #         fontsize=16)
    pl.ylabel(LABEL_DNDE, fontsize=22)
    pl.xlabel(LABEL_GLAT, fontsize=22)
    pl.axis([0.0, 15., 6e-8, 3e-5], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('linear')
    pl.xticks(fontsize=26)
    pl.yticks(fontsize=26)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)
    pl.savefig(plotdir + "/Latitudeprofile_GCexcess_bulge_bandbulgedisk.pdf")
    pl.savefig(plotdir + "/Latitudeprofile_GCexcess_bulge_bandbulgedisk.png")


def make_excess_lat_profile_plot_best_fit():
    """Make plot showing the latitude profile of the GC excess w.r.t. best-fit simulations """

    fig = create_figure()
    #pl.plot(latitude_center,latitudeprofile, ls='--',
    #        lw='2.0', color="black", label=r'$|b|, |l|<20^{\circ}$')
    pl.plot(latitude_center, latitudeprofile_bf_best, ls='--', lw=3.0,
            color="black", label=r'$N_{\rm{bulge}}=1330$ and $\alpha=2.60$')
    #pl.plot(latitude_center,latitudeprofile, ls='--',
    #        lw=3.0, color="black",label=r'$N_{\rm{bulge}}=900$ and $\alpha=2.60$')
    #pl.fill_between(latitude_center,(latitudeprofile_bf-latitudeprofile_bf*0.60),
    #                (latitudeprofile_bf+latitudeprofile_bf*0.80),
    #                color="#A9A9A9", alpha=0.5)
    pl.fill_between(latitude_center, latitudeprofile_bf_low,
                    latitudeprofile_bf_up, color="#A9A9A9", alpha=0.5)  # BEST FIT
    #pl.fill_between(latitude_center,(latitudeprofile*0.38),
    #                (latitudeprofile*1.8),
    #                color="#A9A9A9", alpha=0.5) #BAND
    pl.fill_between(latitudecalore, 0.75 * fluxcalore, 1.25 * fluxcalore,
                    color="#00FFFF", alpha=0.5, label='Calore et al. 2015b')
    #pl.plot(latitudecalore, 0.001 * fluxcalore,
    #        color="#00FFFF", lw=4., label='Calore et al. 2015b')
    pl.plot(latitudeNFW, fluxNFW, color='red', lw=2,
            ls='-', label='Ajello et al. (2017): NFW')
    pl.plot(latitudesample, fluxsample, color="blue",
            lw=2.0, ls='-', label='Ajello et al. (2017): Sample')
    pl.legend(loc=1, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'Latitude profile of $\gamma$-ray spectrum for bulge PSR population at 2 GeV',
    #         fontsize=16)
    pl.ylabel(LABEL_DNDE, fontsize=22)
    pl.xlabel(LABEL_GLAT, fontsize=22)
    pl.axis([0.0, 15., 6e-8, 3e-5], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('linear')
    pl.xticks(fontsize=26)
    pl.yticks(fontsize=26)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)
    pl.savefig(plotdir + "/Latitudeprofile_GCexcess_bulge_bestfitbulgedisk.pdf")
    pl.savefig(plotdir + "/Latitudeprofile_GCexcess_bulge_bestfitbulgedisk.png")


def make_nsource_plot():
    """Make a plot of the simulated number of sources per energy flux bin"""
    # CHECK WITH ENFLUX
    bin_en = 25
    en_binss = np.zeros(int(bin_en + 1))
    for k in range(bin_en + 1):
    #flux_bins[k] = fhl_flux.min() +\
    #   k*((fhl_flux.max()-fhl_flux.min())/bin_flux)
    #flux_bins[k] = np.power(10.,np.log10(fhl_flux.min()) +\
    #   k*((np.log10(fhl_flux.max())-np.log10(fhl_flux.min()))/bin_flux) )
        en_binss[k] = np.power(10., np.log10(5.6444041007134983e-11) + k * (
            np.log10(0.040539261899714886) - np.log10(5.6444041007134983e-11)) / bin_en)
        en_vall = np.zeros(int(bin_en))
        en_deltaa = np.zeros(int(bin_en))
    for k in range(bin_en):
        en_vall[k] = np.power(10., (np.log10(en_binss[k]) +
                                   np.log10(en_binss[k + 1])) / 2.)
        en_deltaa[k] = en_binss[k + 1] - en_binss[k]

    bin_en = 25
    en_bins = np.zeros(int(bin_en+1))
    en_val = np.zeros(int(bin_en))
    en_delta = np.zeros(int(bin_en))

    en_bins = np.zeros(int(bin_en+1))
    for k in range(bin_en+1):	
    	en_bins[k] = np.power(10.,np.log10(1.e-7) + k*(np.log10(0.01188)-np.log10(1.e-7))/bin_en) 

    for k in range(bin_en):
    	en_val[k] = np.power(10.,(np.log10(en_bins[k]) + np.log10(en_bins[k+1]))/2.)
    	en_delta[k] = en_bins[k+1] - en_bins[k]

    fig = create_figure()
    pl.fill_between(en_vall, NS_psr, NS_psrcurv, color="#00FFFF", alpha=0.5)
    pl.plot(en_val * 1.0, NS_best, ls='--', lw=2.0,
            color="black", label=r'$N_{\rm{disk}}=7900$ and $\beta=1.7$')
    print en_val * 1.0, NS_low, NS_up
    pl.fill_between(en_val * 1.0, NS_low, NS_up, color="#A9A9A9", alpha=0.5)
    #pl.errorbar(en_val, histoenb[0], yerr =np.sqrt(histoenb[0]),
    #            fmt="o", color="black",label="Bulge")
    #pl.errorbar(en_val*0.96, histoend[0]/numsim, yerr=np.sqrt(histoend[0]/numsim),
    #            fmt="*", color="blue",label="Disk SIM")
    pl.errorbar(en_vall * 0.98, NS_psr, yerr=np.sqrt(NS_psr),
                fmt="^", color="red", label="3FGL PSR", lw=3.0, markersize=5)
    #pl.errorbar(en_val*1.00, histoen3fgl_3sign[0], yerr =np.sqrt(histoen3fgl_3sign[0]),
    #            fmt="<", color="red",label="3FGL $\sigma_{curv}>3$")
    #pl.errorbar(en_val*1.02, histoen3fgl_5sign[0], yerr =np.sqrt(histoen3fgl_5sign[0]),
    #            fmt=">", color="orange",label="3FGL $\sigma_{curv}>5$")
    pl.errorbar(en_vall * 1.04, NS_psrcurv, yerr=np.sqrt(NS_psrcurv),
                fmt="x", color="black", label=r"3FGL tot ($\texttt{Signif\_Curve}>3$)", lw=3.0, markersize=5)
    pl.legend(loc=1, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'$N(S)$ of 3FGL PSR+3FGL and SIM PSRs', fontsize=20)
    pl.ylabel(LABEL_NSRC_PER_FLUX, fontsize=22)
    pl.xlabel(LABEL_EFLUX, fontsize=22)
    pl.axis([1e-7, 3e-3, 0, 220], fontsize=22)
    pl.grid(True)
    pl.yscale('linear')
    pl.xscale('log')
    pl.xticks(fontsize=26)
    pl.yticks(fontsize=26)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)
    pl.savefig(plotdir + "/NS_psrdisk_sim_bandbulgedisk.pdf")
    pl.savefig(plotdir + "/NS_psrdisk_sim_bandbulgedisk.png")

make_excess_spectrum_plot_bestfitbulgedisk()
make_excess_spectrum_plot_bandbulgedisk()
make_excess_lat_profile_plot()
make_excess_lat_profile_plot_best_fit()
make_nsource_plot()
