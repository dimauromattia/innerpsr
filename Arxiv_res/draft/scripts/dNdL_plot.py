"""Make a plot of dN/dL for PSR"""

#---------------------------------------
# TAKE ALL CATALOGS
#---------------------------------------
import matplotlib.pyplot as pl
import numpy as np
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_DNDL, LABEL_LUMI

dndLyoung = np.array([1.17647059e-32, 4.30684240e-33, 3.24126228e-33,
                      5.85437601e-34, 4.11218443e-34, 2.65265634e-34,
                      6.92067205e-35, 1.44232328e-35])
dndLerryoung = np.array([8.31890331e-33, 3.04539747e-33, 1.44953656e-33,
                         3.38002556e-34, 1.55425962e-34, 6.84912923e-35,
                         1.91944907e-35, 4.80774427e-36])
dndLmsp = np.array([1.17647059e-32, 2.58410544e-32, 9.07553438e-33,
                    3.51262560e-33, 6.46200410e-34, 2.47581259e-34,
                    3.72651572e-35, 1.60258142e-36])
dndLerrmsp = np.array([8.31890331e-33, 7.45966985e-33, 2.42553859e-33,
                       8.27933795e-34, 1.94836755e-34, 6.61688747e-35,
                       1.40849055e-35, 1.60258142e-36])
dndLmy = np.array([2.35294118e-32, 4.40172260e-32, 2.88261747e-32,
                   1.35920153e-32, 5.53851985e-33, 4.97429175e-33,
                   2.82323930e-33, 1.25155908e-33, 4.76873280e-34,
                   4.68444983e-34, 1.47253008e-34, 1.11434418e-34,
                   3.14360787e-35, 7.05839426e-36])
dndLerrmy = np.array([1.17647059e-32, 1.55624395e-32, 9.11563682e-33,
                      4.53067177e-33, 2.09336374e-33, 1.43595434e-33,
                      7.83025698e-34, 3.77359261e-34, 1.68600165e-34,
                      1.20951975e-34, 4.90843359e-35, 3.09063467e-35,
                      1.18817209e-35, 4.07516583e-36])
lum_vals = np.array([7.74596669e+31, 3.64520874e+32, 1.21089703e+33,
                     4.02246273e+33, 1.33621654e+34, 4.43875992e+34,
                     1.47450574e+35, 4.89814099e+35])
lum_val = np.array([7.74596669e+31, 2.76313962e+32, 5.27410168e+32,
                    1.00668632e+33, 1.92149755e+33, 3.66762990e+33,
                    7.00053407e+33, 1.33621654e+34, 2.55048346e+34,
                    4.86819740e+34, 9.29209943e+34, 1.77361567e+35,
                    3.38536255e+35, 6.46176046e+35])

deltalum = np.array([1.70000000e+32, 1.81747028e+32, 3.46906938e+32,
                     6.62153463e+32, 1.26387558e+33, 2.41240373e+33,
                     4.60463978e+33, 8.78903774e+33, 1.67759452e+34,
                     3.20208360e+34, 6.11192949e+34, 1.16660546e+35,
                     2.22674083e+35, 4.25025847e+35])

deltalum_low = np.array([4.74596669e+31, 7.63139619e+31, 1.45663141e+32,
                         2.78032355e+32, 5.30690126e+32, 1.01294689e+33,
                         1.93344732e+33, 3.69043885e+33, 7.04407031e+33,
                         1.34452645e+34, 2.56634488e+34, 4.89847266e+34,
                         9.34988688e+34, 1.78464576e+35])
deltalum_up = np.array([1.22540333e+32, 1.05433066e+32, 2.01243798e+32,
                        3.84121108e+32, 7.33185456e+32, 1.39945684e+33,
                        2.67119245e+33, 5.09859890e+33, 9.73187487e+33,
                        1.85755715e+34, 3.54558461e+34, 6.76758193e+34,
                        1.29175214e+35, 2.46561271e+35])

deltalums = np.array([1.70000000e+32, 4.64377336e+32, 1.54260889e+33,
                      5.12437192e+33, 1.70225828e+34, 5.65470911e+34,
                      1.87843029e+35, 6.23993256e+35])

deltalums_low = np.array([4.74596669e+31, 1.64520874e+32, 5.46519698e+32,
                          1.81547651e+33, 6.03080723e+33, 2.00336582e+34,
                          6.65495424e+34, 2.21070038e+35])
deltalums_up = np.array([1.22540333e+32, 2.99856463e+32, 9.96089189e+32,
                         3.30889541e+33, 1.09917756e+34, 3.65134329e+34,
                         1.21293487e+35, 4.02923217e+35])


def make_dNdL_plot():
    """Make a plot of dN/dL for PSR"""

    plotdir = get_plot_dir()
    #datadir = get_data_dir()

    init_style()

    par0 = 1.54
    par1 = 1.10

    #fig = pl.figure(figsize=(8, 6))
    fig = create_figure()
    # pl.errorbar(lum_val, dndL, xerr = deltalum/2., yerr=dndLerr,
    #            fmt="o", color="blue",label="3FGL")
    pl.errorbar(lum_vals * 0.95, dndLyoung, xerr=[deltalums_low, deltalums_up],
                yerr=dndLerryoung, fmt="*", color="red", label="Young PSR")
    pl.errorbar(lum_vals * 1.05, dndLmsp,
                xerr=[deltalums_low, deltalums_up], yerr=dndLerrmsp, fmt="x", color="blue", label="MSP")
    pl.errorbar(lum_val * 1.00, dndLmy, xerr=[deltalum_low, deltalum_up],
                yerr=dndLerrmy, lw=2.0, ms=10, fmt="+", color="black", label="PSR")
    #pl.plot(Lmax, dndLmax, lw=2.0, ls='--', color="red",label="Brightest PSR")
    # pl.plot(lum_val, 2.248717741918127e-31*np.power(lum_val/1e32,-1.10),
    #        lw=2.0, ls='-', color="black",label=r"$dN/dL\propto L^{-1.1}$")
    pl.plot(lum_val, par0 * 1e-33 * np.power(lum_val / 1e34, -par1),
            lw=2.0, ls='-', color="black", label=r"$dN/dL\propto L^{-1.1}$")
    # pl.plot(lum_val, 2.548717741918127e-31*np.power(lum_val/1e32,-1.10), lw=2.0,
    #        ls='-', color="brown",label="FIT")
    # pl.errorbar(flux, 5.07203e-08*np.power(flux,-2.41308e+00)*np.power(flux,2.),
    #            fmt="-", color="green",label="fit two")
    # pl.errorbar(flux, 6.30387e-09*np.power(flux,-2.55822e+00)*np.power(flux,2.),
    #            fmt="-", color="orange",label="fit three")
    # pl.errorbar(flux, 4.65648e-07*np.power(flux,-2.25882e+00 )*np.power(flux,2.),
    #            fmt="--", color="red",label="fit one")
    # pl.errorbar(flux, 6.23782e-08*np.power(flux,-2.39617e+00)*np.power(flux,2.),
    #            fmt="-", color="green",label="fit two")
    # pl.errorbar(flux, 3.18874e-08*np.power(flux,-2.44224e+00)*np.power(flux,2.),
    #            fmt="-", color="orange",label="fit three")
    pl.legend(loc=3, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'$dN/dL$ of 3FGL PSR', fontsize=18)
    pl.ylabel(LABEL_DNDL, fontsize=22)
    pl.xlabel(LABEL_LUMI, fontsize=22)
    pl.axis([1e31, 3e36, 1e-36, 1e-31], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('log')
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)
    pl.savefig(plotdir + "/dndL_psr_paper.pdf")
    pl.savefig(plotdir + "/dndL_psr_paper.png")
    pl.close(fig)

make_dNdL_plot()
