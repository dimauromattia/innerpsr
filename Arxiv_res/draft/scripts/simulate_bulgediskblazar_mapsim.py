"""Make a map of the location of simulated sources"""
#---------------------------------------
# TAKE ALL CATALOGS
#---------------------------------------
import os.path

from astropy.io import fits
import matplotlib.pyplot as pl
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_GLON, LABEL_GLAT

def make_sim_map():
    """Make a map of the location of simulated sources"""

    plotdir = get_plot_dir()
    datadir = get_data_dir()

    init_style()

    data = fits.open(datadir + '/simblazar.fits')
    dataa = data[1].data
    glonbl_earth = dataa.field('GLON')
    glatbl_earth = dataa.field('GLAT')

    data = fits.open(datadir + '/simdisk.fits')
    dataa = data[1].data
    glond_earth = dataa.field('GLON')
    glatd_earth = dataa.field('GLAT')

    data = fits.open(datadir + '/simbulge.fits')
    dataa = data[1].data
    glonb_earth = dataa.field('GLON')
    glatb_earth = dataa.field('GLAT')

    fig = create_figure(figsize=(8, 8))
    pl.errorbar(glonbl_earth, glatbl_earth, fmt="<", ms=6,
                color="cyan", label='Blazar', alpha=1.0)
    pl.errorbar(glonb_earth, glatb_earth, fmt="o", ms=6,
                color="black", label='Inner Galaxy', alpha=0.6)
    pl.errorbar(glond_earth, glatd_earth, fmt="*",
                ms=6, color="red", label='Disk')

    #pl.errorbar(luminosity_radio_core_ul, luminosity_gamma_core_ul,
    #            uplims=np.array(luminosity_gamma_core_ul)*0.2,
    #            fmt="o", color="red", label="UL")
    #pl.plot(Lradio, Lgamma, ls='--',
    #        color="blue", label='Di Mauro et al. 2013')
    #pl.errorbar(flux/1e4, 1.34961e-14*np.power(flux/1e4,-2.50)/(solidangle),
    #            fmt="-", color="black",label=r'SIM')
    #pl.legend(loc=1,prop={'size':16},
    #          numpoints=1, scatterpoints=1)
    #pl.title(r'Position of bulge and disk PSRs and Blazars from Earth', fontsize=16)

    pl.ylabel(LABEL_GLAT, fontsize=22)
    pl.xlabel(LABEL_GLON, fontsize=22)
    pl.axis([-20., +20., -20., +20.], fontsize=22)
    pl.grid(True)
    pl.yscale('linear')
    pl.xscale('linear')
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    #pl.tick_params('both', length=10, width=2, which='major',color='blue')
    #pl.tick_params('both', length=6, width=1.5, which='minor',color='blue')
    fig.tight_layout(pad=0.5)

    pl.savefig(plotdir + "/position_earth_bulgediskblazar.pdf")
    pl.savefig(plotdir + "/position_earth_bulgediskblazar.png")
    pl.close()

make_sim_map()
