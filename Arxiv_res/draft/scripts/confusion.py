"""Make a plot of showing the number of sources with the localization radius"""
#---------------------------------------
# script which create xml file
#---------------------------------------
#import os
import numpy as np
import matplotlib.pyplot as pl
from common import get_plot_dir, init_style, create_figure
from labels import LABEL_NSRC_PER_PSF, LABEL_DIST

distval = np.array([0.05, 0.15, 0.25, 0.35, 0.45, 0.55, 0.70, 0.90, 1.10,
                    1.40, 1.80, 2.25, 2.75, 3.50, 4.50, 6.00, 8.50])
numdouble = np.array([  3.06191827e+02,   2.33604013e+02,   2.74311638e+01,
         1.57952941e+01,   1.18573913e+01,   9.22444445e+00,
         4.48000000e+00,   1.85714285e+00,   2.69230769e+00,
         1.40000000e+00,   8.40000000e-01,   8.19512196e-01,
         8.63829788e-01,   4.73846153e-01,   7.77777778e-02,
         1.90625000e-01,   2.67672957e-01])
distdelta = np.array([0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.10, 0.10, 0.10,
                      0.20, 0.20, 0.25, 0.25, 0.50, 0.50, 1.00, 1.50])


def make_confusion_plot():
    """Make a plot of showing the number of sources with the localization radius"""
    plotdir = get_plot_dir()
    #datadir = get_data_dir()

    init_style()

    fig = create_figure()
    fig.add_subplot(111)

    pl.errorbar(distval, numdouble,
                xerr=distdelta, yerr=np.sqrt(numdouble),
                fmt="o", ms=10, color="black")
    pl.legend(loc=1, prop={'size':16}, numpoints=1, scatterpoints=1)
    #pl.title(r'Position of PSRs detected with $TS>25$, $TS^{PLE}_{Curv}>9$', fontsize=14)
    pl.ylabel(LABEL_NSRC_PER_PSF, fontsize=22)
    pl.xlabel(LABEL_DIST, fontsize=22)
    pl.axis([0.03, +5., 0.1, +400.], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    #pl.xscale('log')
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)
    pl.savefig(plotdir + "/confusion_paper.pdf")
    pl.savefig(plotdir + "/confusion_paper.png")
    pl.close(fig)


make_confusion_plot()
