"""Make a plot the counts map for the analysis region"""
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import PowerNorm
from matplotlib.colors import SymLogNorm 
from astropy.wcs import WCS
from astropy.io import fits

from astropy.table import Table
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_GLON, LABEL_GLAT
from fermipy.utils import load_yaml

from scipy.ndimage.filters import gaussian_filter
init_style()


def mask_from_list(nameList, maskList):
    """Mask a mask of all the source that have a TS 16 counterpart"""
    mask = np.zeros((len(nameList)),'?')
    for i,name in enumerate(nameList):
        if name in maskList:
            mask[i] = True
        else:
            mask[i] = False
    return mask


def make_mask_dict(tab, ts16_dict):
    """Make the masks for the different source types"""
    ts_cut = 49

    mask_psr = (tab['TS_curv'] > 9)*(tab['Spectral_Index']<2)*(tab['Cutoff']<1e4)
    
    mask_off_cls = (tab['IEM'] == 'Off.')*(tab['Cluster_Off'] != 0)
    mask_alt_cls = (tab['IEM'] == 'Alt.')*(tab['Cluster_Alt'] != 0)
    mask_both_cls = (tab['IEM'] == 'Alt./Off.')*((tab['Cluster_Alt'] != 0) + (tab['Cluster_Off'] != 0))

    mask_off_all = (tab['IEM'] == 'Off.')*(tab['Cluster_Off'] == 0)
    mask_alt_all = (tab['IEM'] == 'Alt.')*(tab['Cluster_Alt'] == 0)
    mask_16_off = mask_from_list(tab['Source_Name'], ts16_dict['names_alt'])
    mask_16_alt = mask_from_list(tab['Source_Name'], ts16_dict['names_off'])

    mask_off = (tab['IEM'] == 'Off.')*(tab['Cluster_Off'] == 0)*(tab['TS'] >ts_cut)*(~mask_16_off)
    mask_alt = (tab['IEM'] == 'Alt.')*(tab['Cluster_Alt'] == 0)*(tab['TS'] >ts_cut)*(~mask_16_alt)
    mask_off_thresh = (tab['IEM'] == 'Off.')*(tab['Cluster_Off'] == 0)*(tab['TS'] <ts_cut)*(~mask_16_off)
    mask_alt_thresh = (tab['IEM'] == 'Alt.')*(tab['Cluster_Alt'] == 0)*(tab['TS'] <ts_cut)*(~mask_16_alt)

    save_off = (tab['IEM'] == 'Off.')*(tab['Cluster_Off'] == 0)*mask_16_off
    save_alt = (tab['IEM'] == 'Alt.')*(tab['Cluster_Alt'] == 0)*mask_16_alt

    mask_off_psr = (mask_off+mask_off_thresh)*mask_psr
    mask_alt_psr = (mask_alt+mask_alt_thresh)*mask_psr

    save_off_psr = (save_off*mask_psr)
    save_alt_psr = (save_alt*mask_psr)
    
    mask_both = (tab['IEM'] == 'Alt./Off.')*(tab['Cluster_Alt'] == 0)*(tab['Cluster_Off'] == 0)
    mask_both_psr = mask_both*mask_psr

    mask_dict = dict(mask_off=mask_off,
                     mask_off_thresh=mask_off_thresh,
                     save_off=save_off,
                     mask_alt=mask_alt,
                     mask_alt_thresh=mask_alt_thresh,
                     save_alt=save_alt,
                     mask_both=mask_both)

    return mask_dict


def add_markers(ax, glon, glat, mask_dict):
    """Add the markers corresponds to various masks to an image"""
    
    marker_off='o'
    marker_alt='s'
    marker_both='x'
    markersize=6
    markersize_both=2
    #markeredgewidth=1.0

    mask_off = mask_dict['mask_off']
    mask_off_thresh = mask_dict['mask_off_thresh']
    save_off = mask_dict['save_off']
    mask_alt = mask_dict['mask_alt']
    mask_alt_thresh = mask_dict['mask_alt_thresh']
    save_alt = mask_dict['save_alt']
    mask_both = mask_dict['mask_both']
 
    # OFF
    ax.plot(glon[mask_off],glat[mask_off], transform=ax.get_transform('world'),
            marker=marker_off, color='w',linestyle='None',
            markersize=markersize)

    ax.plot(glon[mask_off_thresh],glat[mask_off_thresh], transform=ax.get_transform('world'),
            marker=marker_off, color='g',linestyle='None',
            markersize=markersize)
 
    ax.plot(glon[save_off],glat[save_off], transform=ax.get_transform('world'),
            marker=marker_off, color='cyan',linestyle='None',
            markersize=markersize)

    # ALT
    ax.plot(glon[mask_alt],glat[mask_alt], transform=ax.get_transform('world'),
            marker=marker_alt, color='w',linestyle='None',
            markersize=markersize)

    ax.plot(glon[mask_alt_thresh],glat[mask_alt_thresh], transform=ax.get_transform('world'),
            marker=marker_alt, color='g',linestyle='None',
            markersize=markersize)

    ax.plot(glon[save_alt],glat[save_alt], transform=ax.get_transform('world'),
            marker=marker_alt, color='cyan',linestyle='None',
            markersize=markersize)


    # Both
    ax.plot(glon[mask_both],glat[mask_both], transform=ax.get_transform('world'),
            marker=marker_both, color='w',linestyle='None',
            markersize=markersize_both)



def make_figure(wcs, data, tab, ts16_dict, mask_dict):
    """ Make a figure with an image and the markers overlaid"""
    glon = tab['GLON']
    glat = tab['GLAT']

    fig = plt.figure()
    ax = fig.add_subplot(111, projection=wcs)    
    im = ax.imshow(data, interpolation='nearest',
                   origin='lower', vmin=-5., vmax=5., cmap='inferno',
                   norm=SymLogNorm(linthresh=1))
    cb = plt.colorbar(im)
    cb.set_label('Signif')

    ax.coords.grid(color='white',linestyle=':')
    ax.set_xlabel(LABEL_GLON)
    ax.set_ylabel(LABEL_GLAT)

    extent = im.get_extent()
    ax.set_xlim(extent[0], extent[1])
    ax.set_ylim(extent[2], extent[3])

    add_markers(ax, glon, glat, mask_dict)
    return fig, ax, im


def make_syst_maps():
    """Make a plot the counts map for the analysis region"""
    plotdir = get_plot_dir()
    datadir = get_data_dir()

    init_style()

    ts16_dict = load_yaml(os.path.join(datadir,'ts_dict_95.yaml'))
    tab = Table.read(os.path.join(datadir, '2FIG_Pass8_Arxiv.fits'))

    mask_dict = make_mask_dict(tab, ts16_dict)
    
    full_roi = fits.open(os.path.join(datadir, 'systmap.fits'))
    wcs_full_roi = WCS(full_roi[0].header).dropaxis(2)
    data_full_roi = full_roi[0].data.sum(0)

    diff_roi43 = fits.open(os.path.join(datadir, 'signif_mcube_roi43_diffuse.fits'))
    wcs_diff_roi43 = WCS(diff_roi43[0].header).dropaxis(2)
    data_diff_roi43 = diff_roi43[0].data.sum(0)

    signif_roi43_off = fits.open(os.path.join(datadir, 'syst_signif_roi43_off.fits'))
    wcs_roi43_off = WCS(signif_roi43_off[0].header)
    data_roi43_off = signif_roi43_off[0].data
    data_roi43_off = gaussian_filter(data_roi43_off, 2.)

    signif_roi43_ref = fits.open(os.path.join(datadir, 'syst_signif_roi43_ref.fits'))
    wcs_roi43_ref = WCS(signif_roi43_ref[0].header)
    data_roi43_ref = signif_roi43_ref[0].data
    data_roi43_ref = gaussian_filter(data_roi43_ref, 2.)

    fig_full_roi, ax_full_roi, im_full_roi = make_figure(wcs_full_roi, data_full_roi, tab, ts16_dict, mask_dict)
    fig_diff_roi43, ax_diff_roi43, im_diff_roi43 = make_figure(wcs_diff_roi43, data_diff_roi43, tab, ts16_dict, mask_dict)
    fig_roi43_off, ax_roi43_off, im_roi43_off = make_figure(wcs_roi43_off, data_roi43_off, tab, ts16_dict, mask_dict)
    fig_roi43_ref, ax_roi43_ref, im_roi43_ref = make_figure(wcs_roi43_ref, data_roi43_ref, tab, ts16_dict, mask_dict)

    fig_full_roi.savefig(os.path.join(plotdir,'systmap.png'), bbox_inches='tight')
    fig_full_roi.savefig(os.path.join(plotdir,'systmap.pdf'), bbox_inches='tight')

    fig_diff_roi43.savefig(os.path.join(plotdir,'signif_mdiff_diffuse_roi43.png'), bbox_inches='tight')
    fig_diff_roi43.savefig(os.path.join(plotdir,'signif_mdiff_diffuse_roi43.pdf'), bbox_inches='tight')

    fig_roi43_off.savefig(os.path.join(plotdir,'signif_resid_roi43_off.png'), bbox_inches='tight')
    fig_roi43_off.savefig(os.path.join(plotdir,'signif_resid_roi43_off.pdf'), bbox_inches='tight')

    fig_roi43_ref.savefig(os.path.join(plotdir,'signif_resid_roi43_ref.png'), bbox_inches='tight')
    fig_roi43_ref.savefig(os.path.join(plotdir,'signif_resid_roi43_ref.pdf'), bbox_inches='tight')



make_syst_maps()

