#---------------------------------------
#Script to 
#---------------------------------------
import os
import numpy as np

import matplotlib.pyplot as pl
import matplotlib.patheffects as PathEffects
from matplotlib.colors import PowerNorm

from common import get_plot_dir, get_data_dir, init_style, create_figure
from fermipy.utils import load_yaml

datadir = get_data_dir()
plotdir = get_plot_dir()
init_style()


def make_sqrt_ts_correl_mask(d):
    """ Make a plot of sqrt(TS) correclation """
    fig = create_figure()
    ax = fig.add_subplot(111)

    pl.axis([0., 25., 0., 25.])

    pl.ylabel(r'$\sqrt{TS}_{\rm Off}$')
    pl.xlabel(r'$\sqrt{TS}_{\rm Alt}$')

    pl.yscale('linear')
    pl.xscale('linear')
    pl.grid(True)

    mask = np.abs(d['glat']) > 2.5

    pl.scatter(np.sqrt(d['tscomb_off'])[~mask], np.sqrt(d['tscomb_alt'])[~mask], marker='x', color='r', label=r'$|b| < 2.5^\circ$')
    pl.scatter(np.sqrt(d['tscomb_off'])[mask], np.sqrt(d['tscomb_alt'])[mask], marker='.', label=r'$|b| > 2.5^\circ$')
    
    abs_diff = np.sqrt(d['tscomb_off']) - np.sqrt(d['tscomb_alt'])
    rel_diff = abs_diff / ((np.sqrt(d['tscomb_off']) + np.sqrt(d['tscomb_alt']) ) / 2.)

    leg = pl.legend(loc=4, prop={'size': 16})

    fig.savefig(os.path.join(plotdir,"sqrtTS_correl_mask.pdf"), bbox_inches='tight')
    fig.savefig(os.path.join(plotdir,"sqrtTS_correl_mask.png"), bbox_inches='tight')
 

def make_sqrt_tscurv_correl_mask(d):
    """ Make a plot of sqrt(TS) correclation """
    fig = create_figure()
    ax = fig.add_subplot(111)

    pl.axis([0., 20., 0., 20.])

    pl.ylabel(r'$\sqrt{TS}_{\rm Curv,Off}$')
    pl.xlabel(r'$\sqrt{TS}_{\rm Curv,Alt}$')

    pl.yscale('linear')
    pl.xscale('linear')
    pl.grid(True)

    mask = np.abs(d['glat']) > 2.5

    tscurv_off = np.array(d['tscurvcomb_off'])
    tscurv_alt = np.array(d['tscurvcomb_alt'])
    tscurv_off = np.where(tscurv_off==-np.inf, 0.1, tscurv_off)
    tscurv_alt = np.where(tscurv_alt==-np.inf, 0.1, tscurv_alt)
    curv_mask = (tscurv_off > 1)*(tscurv_alt > 1)

    mean = (np.sqrt(tscurv_off) + np.sqrt(tscurv_alt))/2.
    abs_diff = np.sqrt(tscurv_off) - np.sqrt(tscurv_alt)
    rel_diff = abs_diff / mean

    print ("Curve Abs Mask ", abs_diff[curv_mask*mask].mean(), "+/-", abs_diff[curv_mask*mask].std())
    print ("Curve Rol Mask ", rel_diff[curv_mask*mask].mean(), "+/-", rel_diff[curv_mask*mask].std())
    print ("Curve Abs Not Mask ", abs_diff[curv_mask*~mask].mean(), "+/-", abs_diff[curv_mask*~mask].std())
    print ("Curve Rel Not Mask ", rel_diff[curv_mask*~mask].mean(), "+/-", rel_diff[curv_mask*~mask].std())

    pl.scatter(np.sqrt(tscurv_off)[~mask], np.sqrt(tscurv_alt)[~mask], marker='x', color='r', label=r'$|b| < 2.5^\circ$')
    pl.scatter(np.sqrt(tscurv_off)[mask], np.sqrt(tscurv_alt)[mask], marker='.', label=r'$|b| > 2.5^\circ$')

    leg = pl.legend(loc=4, prop={'size': 16})

    fig.savefig(os.path.join(plotdir,"sqrtTScurv_correl_mask.pdf"), bbox_inches='tight')
    fig.savefig(os.path.join(plotdir,"sqrtTScurv_correl_mask.png"), bbox_inches='tight')
 

def sim_sqrt_ts(d):
    """ """
    match_mask = (np.array(d['tscomb_off']) > 1) * (np.array(d['tscomb_alt']) > 1)
    mask = np.abs(d['glat'])[match_mask] > 2.5

    sqrt_off = np.sqrt(d['tscomb_off'])[match_mask]
    sqrt_off_nogal = sqrt_off[mask]
    sqrt_off_gal = sqrt_off[~mask]
    
    abs_diff = np.sqrt(d['tscomb_off'])[match_mask] - np.sqrt(d['tscomb_alt'])[match_mask]
    mean = (np.sqrt(d['tscomb_off'])[match_mask] + np.sqrt(d['tscomb_alt'])[match_mask] ) / 2.
    
    rel_diff = abs_diff / mean
    print ("Abs ", abs_diff.mean(), "+/-", abs_diff.std())
    print ("Rel ", rel_diff.mean(), "+/-", rel_diff.std())
    print ("Abs Mask ", abs_diff[mask].mean(), "+/-", abs_diff[mask].std())
    print ("Rel Mask ", rel_diff[mask].mean(), "+/-", rel_diff[mask].std())
    print ("Abs Not Mask ", abs_diff[~mask].mean(), "+/-", abs_diff[~mask].std())
    print ("Rel Not Mask ", rel_diff[~mask].mean(), "+/-", rel_diff[~mask].std())
        
    use_absolute = True

    if use_absolute:
        #delta = np.random.normal(loc=abs_diff.mean(), scale=abs_diff.std(), size=sqrt_off.shape)
        #delta_nogal = np.random.normal(loc=abs_diff[mask].mean(), scale=abs_diff[mask].std(), size=sqrt_off.shape)
        #delta_gal = np.random.normal(loc=abs_diff[~mask].mean(), scale=abs_diff[~mask].std(), size=sqrt_off.shape)
        delta = np.random.normal(loc=0.0, scale=abs_diff.std(), size=sqrt_off.shape)
        delta_nogal = np.random.normal(loc=0.0, scale=abs_diff[mask].std(), size=sqrt_off.shape)
        delta_gal = np.random.normal(loc=0.0, scale=abs_diff[~mask].std(), size=sqrt_off.shape)

        exp_pass = (sqrt_off + delta) > 5.
        exp_pass_nogal = (sqrt_off + delta_nogal) > 5.
        exp_pass_gal = (sqrt_off + delta_gal) > 5.
    else:
        delta = np.random.normal(loc=1.0, scale=0.22, size=sqrt_off.shape)
        delta_nogal = np.random.normal(loc=1.0, scale=0.16, size=sqrt_off.shape)
        delta_gal = np.random.normal(loc=1.0, scale=0.31, size=sqrt_off.shape)
        
        exp_pass = (sqrt_off * delta) > 5.
        exp_pass_nogal = (sqrt_off * delta_nogal) > 5.
        exp_pass_gal = (sqrt_off * delta_gal) > 5.

    frac_pass = float(exp_pass.sum()) / float(exp_pass.size)
    frac_pass_nogal = float(exp_pass_nogal.sum()) / float(exp_pass_nogal.size)
    frac_pass_gal = float(exp_pass_gal.sum()) / float(exp_pass_gal.size)
    print frac_pass, frac_pass_nogal, frac_pass_gal
    

d = load_yaml(os.path.join(datadir,'ts_dict_95.yaml'))
#d = load_yaml(os.path.join(datadir,'catalog_both_wmap_merged.yaml'))

make_sqrt_ts_correl_mask(d)
make_sqrt_tscurv_correl_mask(d)
sim_sqrt_ts(d)
