"""Make all the plots for this paper"""

from analyzeTS25_Shisto import plot_nsource_comparison
from confusion import make_confusion_plot
from countmap import make_countmap
from dNdL_plot import make_dNdL_plot
from efficiency import make_effic_plot
from localization import make_localization_plot
from make_IndexEcut import make_cutoff_index_3FGL_plot, make_cutoff_index_plot,\
    make_cutoff_index_sim_fsrq_plot
from makeplot_SdetSsim import makeplot_SdetSsim
from sed import make_sed_plot
from simulate_bulgediskblazar_mapsim import make_sim_map
from simulate_plots import make_excess_spectrum_plot_bestfitbulgedisk,\
    make_excess_spectrum_plot_bandbulgedisk,\
    make_excess_lat_profile_plot, make_nsource_plot
from tsmap import make_tsmap_plot


plot_nsource_comparison()
make_confusion_plot()
make_countmap()
make_dNdL_plot()
make_effic_plot()
make_localization_plot()
make_cutoff_index_3FGL_plot()
make_cutoff_index_plot()
make_cutoff_index_sim_fsrq_plot()
makeplot_SdetSsim()
make_sed_plot()
make_sim_map()
make_excess_spectrum_plot_bestfitbulgedisk()
make_excess_spectrum_plot_bandbulgedisk()
make_excess_lat_profile_plot()
make_nsource_plot()
make_tsmap_plot()



