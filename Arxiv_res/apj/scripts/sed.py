"""Make a plot of an example SED"""

import os

import matplotlib.pyplot as plt
from astropy.table import Table
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_E_MEV,LABEL_E_GEV, LABEL_E2DNDE

def make_sed_plot():
    """Make a plot of an example SED"""
    plotdir = get_plot_dir()
    datadir = get_data_dir()

    init_style()

    tab0 = Table.read(os.path.join(datadir, '3fgl_j1730.6-0357_sed.fits'))
    tab1 = Table.read(os.path.join(datadir,
                                   '3fgl_j1730.6-0357_sed.fits'), 'MODEL_FLUX')

    e2dnde = tab0['norm'] * tab0['ref_dnde'] * tab0['e_ref']**2
    e2dnde_err_lo = tab0['norm_errn'] * tab0['ref_dnde'] * tab0['e_ref']**2
    e2dnde_err_hi = tab0['norm_errp'] * tab0['ref_dnde'] * tab0['e_ref']**2
    e2dnde_ul = tab0['norm_ul'] * tab0['ref_dnde'] * tab0['e_ref']**2

    energy_err_lo = (tab0['e_ref'] - tab0['e_min'])
    energy_err_hi = (tab0['e_max'] - tab0['e_ref'])

    m = tab0['ts'] > 4

    fig = create_figure()
    ax = fig.add_subplot(111)

    color = 'k'

    e2 = tab1['energy']**2

    ax.plot(tab1['energy']/1e3, tab1['dnde'] * e2, color=color)
    ax.plot(tab1['energy']/1e3, tab1['dnde_lo'] * e2, color=color, linestyle='--')
    ax.plot(tab1['energy']/1e3, tab1['dnde_hi'] * e2, color=color, linestyle='--')

    ax.fill_between(tab1['energy']/1e3,
                    tab1['dnde_lo'] * e2,
                    tab1['dnde_hi'] * e2,
                    alpha=0.5, color=color, zorder=-1)

    ax.errorbar(tab0['e_ref'][m]/1e3, e2dnde[m],
                xerr=(energy_err_lo[m]/1e3, energy_err_hi[m]/1e3),
                yerr=(e2dnde_err_lo[m], e2dnde_err_hi[m]),
                color='k', marker='o', linestyle='None')

    ax.errorbar(tab0['e_ref'][~m]/1e3, e2dnde_ul[~m],
                xerr=(energy_err_lo[~m]/1e3, energy_err_hi[~m]/1e3),
                yerr=e2dnde_ul[~m] * 0.2,
                color='k', marker='o', linestyle='None', uplims=True)

    ax.set_xlabel(LABEL_E_GEV)
    ax.set_ylabel(LABEL_E2DNDE)

    ax.set_xscale('log')
    ax.set_yscale('log')

    ax.set_ylim(1E-7, 5E-5)
    ax.set_xlim(tab0['e_min'][0]/1e3, tab0['e_max'][-1]/1e3)

    fig.savefig(os.path.join(plotdir, 'sed.pdf'))
    fig.savefig(os.path.join(plotdir, 'sed.png'))
    plt.close(fig)


make_sed_plot()
