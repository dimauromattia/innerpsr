"""Make a plot of the PSR selection efficiency"""
#---------------------------------------
# Routine to make plot for the efficiency
#---------------------------------------
import os.path

import matplotlib.pyplot as pl
import numpy as np
from common import get_plot_dir, get_data_dir, init_style,\
    init_tick_params, create_figure
from labels import LABEL_ABSGLAT, LABEL_EFLUX


binFvec = np.array([5.01187234e-07, 7.94328235e-07, 1.25892541e-06,
                    1.99526231e-06, 3.16227766e-06, 5.01187234e-06,
                    7.94328235e-06, 1.25892541e-05, 1.99526231e-05,
                    3.16227766e-05, 5.01187234e-05, 7.94328235e-05,
                    1.25892541e-04, 1.99526231e-04])
distgc = [1., 3., 5., 7.5, 10.5, 14., 18.]

efficiency = np.zeros(shape=(len(binFvec), len(distgc)))

efficiency[:, 0] = [0.00354610, 0.00411523, 0.01162791, 0.00854701, 0.05857741,
                    0.17112299, 0.22916667, 0.28140704, 0.32989691, 0.42405063,
                    0.46601942, 0.60000000, 0.80000000, 1.00000000]

efficiency[:, 1] = [0.00892857, 0.01785714, 0.06493506, 0.15789474, 0.30909091,
                    0.40816327, 0.62162162, 0.76595745, 0.70833333, 0.81159420,
                    0.88636364, 0.90000000, 0.94339623, 1.00000000]

efficiency[:, 2] = [0.01190476, 0.01086957, 0.11627907, 0.38095238, 0.69411765,
                    0.83950617, 0.87671233, 0.90647059, 0.91769231, 0.92444444,
                    0.95444444, 0.97444444, 1.00000000, 1.00000000]

efficiency[:, 3] = [0.00000000, 0.04166667, 0.14782609, 0.39622642, 0.71739130,
                    0.74712644, 0.89189189, 0.85915493, 0.85365854, 0.90058824,
                    0.92647059, 0.95476190, 1.00000000, 1.00000000]

efficiency[:, 4] = [0.00925926, 0.06603774, 0.20408163, 0.50980392, 0.85294118,
                    0.84615385, 0.88505747, 0.92222222, 0.94366197, 0.95522388,
                    0.94339623, 0.95000000, 1.00000000, 1.00000000]

efficiency[:, 5] = [0.00000000, 0.05590062, 0.19298246, 0.52307692, 0.79032258,
                    0.85600000, 0.85964912, 0.90740741, 0.90774194, 0.89156627,
                    0.92461538, 0.89500000, 1.00000000, 1.00000000]

efficiency[:, 6] = [0.02142857, 0.05263158, 0.20833333, 0.55905512, 0.71052632,
                    0.76237624, 0.80357143, 0.87878788, 0.92391304, 0.95555556,
                    0.93750000, 0.95000000, 1.00000000, 1.00000000]



def make_effic_plot():
    """Make a plot of the PSR selection efficiency"""
    plotdir = get_plot_dir()
    #datadir = get_data_dir()
    init_style()

    fig = create_figure()
    ax = fig.add_subplot(111)
    levels = np.linspace(0.00, 1.00, 41)
    cs = pl.contourf(distgc, binFvec, efficiency, label=r"$\omega$",
                     levels=levels, cmap="hot", interpolation="linear",
                     vmin=0., vmax=1.)
    fig.colorbar(cs, ax=ax, format="%.2f", label=r'$\omega$')
    #pl.title(r'Sensitivity map for detection of pulsars ($TS=25$)', fontsize=16)
    pl.xlabel(LABEL_ABSGLAT, fontsize=22)
    pl.ylabel(LABEL_EFLUX, fontsize=22)
    pl.axis([1.0, 17., 1e-6, 2.e-4], fontsize=22)
    pl.legend(loc=1, prop={'size': 9}, numpoints=1, scatterpoints=1)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('linear')
    init_tick_params()
    fig.tight_layout(pad=0.5)
    fig.savefig(os.path.join(plotdir, 'omega_detectedsim_TScurv9_contourb.png'),
                bbox_inches='tight')
    fig.savefig(os.path.join(plotdir, 'omega_detectedsim_TScurv9_contourb.pdf'),
                bbox_inches='tight')
    pl.close(fig)


make_effic_plot()
