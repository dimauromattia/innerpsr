"""Make a plot of an example TSMAP """

import os
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.patheffects as PathEffects
from matplotlib.colors import PowerNorm
from astropy.wcs import WCS
from astropy.io import fits
from astropy.table import Table
from astropy.coordinates import SkyCoord
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_GLON, LABEL_GLAT


def make_tsmap_plot():
    """Make a plot of an example TSMAP """
    plotdir = get_plot_dir()
    datadir = get_data_dir()

    init_style()

    fig = create_figure(figsize=(8,6))
    fig.subplots_adjust(bottom=0.12, left=0.12, right=0.95, top=0.95)

    hdulist = fits.open(os.path.join(datadir, 'tsmap.fits'))
    wcs = WCS(hdulist[0].header)
    ax = fig.add_subplot(111, projection=wcs)
    data = hdulist[0].data 

    tab = Table.read(os.path.join(datadir,'fit3TS10.fits'))
    tab = tab[tab['ts'] > 25.0]

    name = []
    glon = []
    glat = []
    ts  = []

    m = np.zeros(len(tab),dtype=bool)
    for i, row in enumerate(tab):
        if '3FGL' in row['name']:
            m[i] = True
    
    tab1 = Table.read(os.path.join(datadir, 'fitrel.fits'))

    im = ax.imshow(data, interpolation='bicubic',
                   vmax=25.0, vmin=0.0, origin='lower', norm=PowerNorm(0.5))
    #ax.plot(glon,glat, transform=ax.get_transform('world'),marker='*',
    #        lw=3.0, color='w',linestyle='None', markersize=100)
    cb = plt.colorbar(im,ticks=[0,1,4,9,16,25])
    cb.set_label('TS')
    cs = ax.contour(data, levels=[9.0, 25.0, 49.0],
                    colors=['k'],linewidths=[1.0],linestyles=['-'])
    cs.levels = ['%.0f' % val for val in cs.levels]
    plt.clabel(cs, inline=1, fontsize=8)
    ax.coords.grid(color='w',linestyle=':',linewidth=0.5)
    ax.set_xlabel(LABEL_GLON)
    ax.set_ylabel(LABEL_GLAT)

    skydir = SkyCoord(tab['glon'], tab['glat'], frame='galactic', unit='deg')
    pix = skydir.to_pixel(wcs)

    path_effect = PathEffects.withStroke(linewidth=2.0, foreground="black")

    p = ax.plot(tab1['glon'], tab1['glat'],
                transform=ax.get_transform('world'),
                marker='+', color='w', linestyle='None',
                markersize=8, clip_on=True, markeredgecolor='w',
                markerfacecolor='None')
    plt.setp(p, path_effects=[path_effect])

    p = ax.plot(tab['glon'][m],tab['glat'][m],
                transform=ax.get_transform('world'),
                marker='+',color='w', linestyle='None',
                markersize=8, clip_on=True, markeredgecolor='w',
                markerfacecolor='None')
    plt.setp(p, path_effects=[path_effect])

    p = ax.plot(tab['glon'][~m],tab['glat'][~m],
                transform=ax.get_transform('world'),
                marker='+',color='w', linestyle='None',
                markersize=8, clip_on=True, markeredgecolor='w',
                markerfacecolor='None')
    plt.setp(p, path_effects=[path_effect])

    for i, row in enumerate(tab):    
        name = row['name'].replace('PS','2FIG')        
        t = ax.annotate(name, xy=[pix[0][i], pix[1][i]],
                        xytext=(5.0, 5.0), textcoords='offset points',
                        color='w', size=10, clip_on=True)
        plt.setp(p, path_effects=[path_effect])

    fig.savefig(os.path.join(plotdir, 'tsmap.pdf'))
    fig.savefig(os.path.join(plotdir, 'tsmap.png'))
    plt.close(fig)


make_tsmap_plot()
