"""Make a plot of dN/dL for PSR"""

#---------------------------------------
# TAKE ALL CATALOGS
#---------------------------------------
import matplotlib.pyplot as pl
import numpy as np
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_DNDL, LABEL_LUMI

dndLyoung = np.array([  1.17647059e-32,   3.92228131e-33,   2.76253050e-33,
         4.66967511e-34,   2.63114629e-34,   6.17720599e-35,
         1.74028516e-35])
dndLerryoung = np.array([  8.31890331e-33,   2.77347171e-33,   1.23544120e-33,
         2.69603818e-34,   1.07416098e-34,   2.76253050e-35,
         7.78279186e-36])
dndLmsp = np.array([  1.17647059e-32,   2.94171099e-32,   6.63007320e-33,
         2.95746090e-33,   2.63114629e-34,   0.00000000e+00,
         0.00000000e+00])
dndLerrmsp = np.array([  8.31890331e-33,   7.59546510e-33,   1.91393727e-33,
         6.78488064e-34,   1.07416098e-34,   0.00000000e+00,
         0.00000000e+00])
dndLmy = np.array([  2.26470588e-32,   3.51348987e-32,   2.46448946e-32,
         1.15245497e-32,   4.94006206e-33,   2.73010954e-33,
         8.83845246e-34,   2.29615248e-34,   8.58989219e-35,
         3.02105208e-35,   4.69593117e-36])
dndLerrmy = np.array([  1.41885342e-32,   1.24220626e-32,   7.11436826e-33,
         3.32685094e-33,   1.48948475e-33,   7.57196149e-34,
         2.94615082e-34,   1.02687061e-34,   4.29494609e-35,
         2.04548276e-35,   4.69593117e-36])
lum_vals = np.array([  7.74596669e+31,   3.76804281e+32,   1.33748061e+33,
         4.74743646e+33,   1.68511997e+34,   5.98139512e+34,
         2.12311813e+35])
lum_val = np.array([  7.74596669e+31,   2.92470115e+32,   6.25438332e+32,
         1.33748061e+33,   2.86016109e+33,   6.11636640e+33,
         1.30796612e+34,   2.79704525e+34,   5.98139512e+34,
         1.27910292e+35,   2.73532221e+35])

deltalum_low = np.array([  4.74596669e+31,   9.24701147e+31,   1.97744492e+32,
          4.22870506e+32,   9.04295553e+32,   1.93380819e+33,
          4.13538925e+33,   8.84340254e+33,   1.89113439e+34,
          4.04413266e+34,   8.64825313e+34])
deltalum_up = np.array([  1.22540333e+32,   1.35223725e+32,   2.89171772e+32,
          6.18384927e+32,   1.32239712e+33,   2.82790551e+33,
          6.04738884e+33,   1.29321548e+34,   2.76550147e+34,
          5.91393971e+34,   1.26467779e+35])

deltalums = np.array([1.70000000e+32, 4.64377336e+32, 1.54260889e+33,
                      5.12437192e+33, 1.70225828e+34, 5.65470911e+34,
                      1.87843029e+35, 6.23993256e+35])

deltalums_low = np.array([  4.74596669e+31,   1.76804281e+32,   6.27573278e+32,
         2.22759436e+33,   7.90692783e+33,   2.80659302e+34,
         9.96210482e+34])
deltalums_up = np.array([  1.22540333e+32,   3.33103051e+32,   1.18236149e+33,
         4.19683545e+33,   1.48968213e+34,   5.28768133e+34,
         1.87688187e+35])


def make_dNdL_plot():
    """Make a plot of dN/dL for PSR"""

    plotdir = get_plot_dir()
    #datadir = get_data_dir()

    init_style()

    par0 = 1.14
    par1 = 1.20

    
    lumval1 = np.power(10.,np.arange(28.,33,0.01))
    lumval2 = np.power(10.,np.arange(33.,37,0.01))

    #fig = pl.figure(figsize=(8, 6))
    fig = create_figure()
    # pl.errorbar(lum_val, dndL, xerr = deltalum/2., yerr=dndLerr,
    #            fmt="o", color="blue",label="3FGL")
    pl.errorbar(lum_vals * 0.95, dndLyoung, xerr=[deltalums_low, deltalums_up],
                yerr=dndLerryoung, fmt="*", color="red", label="Young PSR")
    pl.errorbar(lum_vals * 1.05, dndLmsp,
                xerr=[deltalums_low, deltalums_up], yerr=dndLerrmsp, fmt="x", color="blue", label="MSP")
    pl.errorbar(lum_val * 1.00, dndLmy, xerr=[deltalum_low, deltalum_up],
                yerr=dndLerrmy, lw=2.0, ms=10, fmt="+", color="black", label="PSR")
    #pl.plot(Lmax, dndLmax, lw=2.0, ls='--', color="red",label="Brightest PSR")
    # pl.plot(lum_val, 2.248717741918127e-31*np.power(lum_val/1e32,-1.10),
    #        lw=2.0, ls='-', color="black",label=r"$dN/dL\propto L^{-1.1}$")
    pl.plot(lumval2, par0 * 1e-33 * np.power(lumval2 / 1e34, -par1),
            lw=2.0, ls='-', color="black", label=r"$dN/dL\propto L^{-1.2}$")
    pl.plot(lumval1, par0 * 1e-33 * np.power(lumval1 / 1e34, -par1),
            lw=2.0, ls='--', color="black")
    # pl.plot(lum_val, 2.548717741918127e-31*np.power(lum_val/1e32,-1.10), lw=2.0,
    #        ls='-', color="brown",label="FIT")
    # pl.errorbar(flux, 5.07203e-08*np.power(flux,-2.41308e+00)*np.power(flux,2.),
    #            fmt="-", color="green",label="fit two")
    # pl.errorbar(flux, 6.30387e-09*np.power(flux,-2.55822e+00)*np.power(flux,2.),
    #            fmt="-", color="orange",label="fit three")
    # pl.errorbar(flux, 4.65648e-07*np.power(flux,-2.25882e+00 )*np.power(flux,2.),
    #            fmt="--", color="red",label="fit one")
    # pl.errorbar(flux, 6.23782e-08*np.power(flux,-2.39617e+00)*np.power(flux,2.),
    #            fmt="-", color="green",label="fit two")
    # pl.errorbar(flux, 3.18874e-08*np.power(flux,-2.44224e+00)*np.power(flux,2.),
    #            fmt="-", color="orange",label="fit three")
    pl.legend(loc=3, prop={'size': 18}, numpoints=1, scatterpoints=1)
    #pl.title(r'$dN/dL$ of 3FGL PSR', fontsize=18)
    pl.ylabel(LABEL_DNDL, fontsize=22)
    pl.xlabel(LABEL_LUMI, fontsize=22)
    pl.axis([1e31, 1e36, 1e-35, 1e-31], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('log')
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    pl.tick_params('both', length=10, width=2, which='major')
    pl.tick_params('both', length=6, width=1.5, which='minor')
    fig.tight_layout(pad=0.5)
    pl.savefig(plotdir + "/dndL_psr_3kpc_paper.pdf")
    pl.savefig(plotdir + "/dndL_psr_3kpc_paper.png")
    pl.close(fig)

make_dNdL_plot()
