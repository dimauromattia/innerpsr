
import os
import numpy as np

import astropy.io.fits as fits
import matplotlib.pyplot as pl

from common import get_plot_dir, get_data_dir, init_style, create_figure

init_style()

datadir = get_data_dir()
plotdir = get_plot_dir()

diffvec = np.arange(-3.0,3.1,0.1)    
bindiff = np.arange(-3.0,3.1,0.4)
valdiff = np.arange(-3.0+0.20,3.2,0.4)

histo_diff_one=[ 1,  2,  1,  5,  4,  4,  9, 22, 12, 11,  3,  6,  1,  1,  1]
histo_diff_two=[ 1,  0,  4,  1,  8, 13, 19, 11,  9,  7,  8,  1,  0,  0,  1]
gaussdiff_E1=[  1.17479063e-04,   2.62179216e-04,   5.69133372e-04,
                1.20173262e-03,   2.46819608e-03,   4.93093722e-03,
                9.58202490e-03,   1.81118630e-02,   3.33002091e-02,
                5.95537077e-02,   1.03597352e-01,   1.75293784e-01,
                2.88511004e-01,   4.61887550e-01,   7.19263691e-01,
                1.08947690e+00,   1.60518806e+00,   2.30044485e+00,
                3.20682851e+00,   4.34828153e+00,   5.73505491e+00,
                7.35758882e+00,   9.18145382e+00,   1.11446235e+01,
                1.31582265e+01,   1.51114911e+01,   1.68808881e+01,
                1.83426158e+01,   1.93867609e+01,   1.99309154e+01,
                1.99309154e+01,   1.93867609e+01,   1.83426158e+01,
                1.68808881e+01,   1.51114911e+01,   1.31582265e+01,
                1.11446235e+01,   9.18145382e+00,   7.35758882e+00,
                5.73505491e+00,   4.34828153e+00,   3.20682851e+00,
                2.30044485e+00,   1.60518806e+00,   1.08947690e+00,
                7.19263691e-01,   4.61887550e-01,   2.88511004e-01,
                1.75293784e-01,   1.03597352e-01,   5.95537077e-02,
                3.33002091e-02,   1.81118630e-02,   9.58202490e-03,
                4.93093722e-03,   2.46819608e-03,   1.20173262e-03,
                5.69133372e-04,   2.62179216e-04,   1.17479063e-04,
                5.12036237e-05]
gaussdiff_E2=[  1.20173262e-03,   2.46819608e-03,   4.93093722e-03,
                9.58202490e-03,   1.81118630e-02,   3.33002091e-02,
                5.95537077e-02,   1.03597352e-01,   1.75293784e-01,
                2.88511004e-01,   4.61887550e-01,   7.19263691e-01,
                1.08947690e+00,   1.60518806e+00,   2.30044485e+00,
                3.20682851e+00,   4.34828153e+00,   5.73505491e+00,
                7.35758882e+00,   9.18145382e+00,   1.11446235e+01,
                1.31582265e+01,   1.51114911e+01,   1.68808881e+01,
                1.83426158e+01,   1.93867609e+01,   1.99309154e+01,
                1.99309154e+01,   1.93867609e+01,   1.83426158e+01,
                1.68808881e+01,   1.51114911e+01,   1.31582265e+01,
                1.11446235e+01,   9.18145382e+00,   7.35758882e+00,
                5.73505491e+00,   4.34828153e+00,   3.20682851e+00,
                2.30044485e+00,   1.60518806e+00,   1.08947690e+00,
                7.19263691e-01,   4.61887550e-01,   2.88511004e-01,
                1.75293784e-01,   1.03597352e-01,   5.95537077e-02,
                3.33002091e-02,   1.81118630e-02,   9.58202490e-03,
                4.93093722e-03,   2.46819608e-03,   1.20173262e-03,
                5.69133372e-04,   2.62179216e-04,   1.17479063e-04,
                5.12036237e-05,   2.17079557e-05,   8.95189882e-06,
                3.59078506e-06]


def plot_histo():

    fig = create_figure()
    ax = fig.add_subplot(111)
 
    pl.axis([-3.0,+3.,0.,30.])

    pl.xscale('linear')
    pl.yscale('linear', nonposy='clip')

    pl.xlabel(r'$(dN/dE_{\rm{data}}-dN/dE_{\rm{model}})/\sigma_{\rm{data}}$', fontsize=16)
    pl.ylabel(r'$N$', fontsize=18)

    pl.errorbar(valdiff, histo_diff_one, yerr=np.sqrt(histo_diff_one), color='black', fmt='d', label=r'$E=0.35$ GeV')
    pl.errorbar(valdiff, histo_diff_two, yerr=np.sqrt(histo_diff_two), color='blue', fmt='*', label=r'$E=2$ GeV')

    pl.plot(diffvec, gaussdiff_E1, color='black', lw=2.0, ls='--')
    pl.plot(diffvec, gaussdiff_E2, color='blue', lw=2.0, ls='--')

    #pl.xticks(fontsize=20)
    #pl.yticks(fontsize=20)
    #pl.tick_params('both', length=10, width=2, which='major')
    #pl.tick_params('both', length=6, width=1.5, which='minor')

    pl.legend(loc=1,prop={'size':18}, numpoints=1, scatterpoints=1)
    fig.tight_layout()

    pl.savefig(os.path.join(plotdir, 'histo_diff.png'), bbox_inches='tight')
    pl.savefig(os.path.join(plotdir, 'histo_diff.pdf'), bbox_inches='tight')


plot_histo()
