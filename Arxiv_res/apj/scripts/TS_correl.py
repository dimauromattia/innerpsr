#---------------------------------------
#Script to 
#---------------------------------------
import astropy.io.fits as fits

from math import *

import numpy as np
import os.path

from common import get_plot_dir, get_data_dir, init_style, create_figure
from fermipy.utils import write_yaml

#TS CORRELATION FOR ALL SOURCES FOUND WITH BOTH IEM

datadir = get_data_dir()

datapsrcand = fits.open(os.path.join(datadir,"All_OffIEM_Sept2017_replyjournal_TSCURVALL.fits"))
#mask = datapsrcand[1].data.field('TS') > 25
mask = np.ones(len(datapsrcand[1].data.field('TS')),'?')
data_psrcand = datapsrcand[1].data[mask]

glat_off  = data_psrcand.field('GLAT')
glon_off  = data_psrcand.field('GLON')
ts_off  = data_psrcand.field('TS')
tscurv_off  = data_psrcand.field('TS_curv')
name_off  = data_psrcand.field('Source_Name')
poserr68_off  = data_psrcand.field('r68')
poserr95_off  = data_psrcand.field('r95')
index_off  = data_psrcand.field('Spectral_Index')
indexerr_off  = data_psrcand.field('Unc_Spectral_Index')
cutoff_off  = data_psrcand.field('Cutoff')
cutofferr_off  = data_psrcand.field('Unc_Cutoff')

datapsrcand = fits.open(os.path.join(datadir,"All_RefIEM_Sept2017_replyjournal_TSCURVALL.fits"))
#mask = datapsrcand[1].data.field('TS') > 25
mask = np.ones(len(datapsrcand[1].data.field('TS')),'?')
data_psrcand = datapsrcand[1].data[mask]

glat_alt  = data_psrcand.field('GLAT')
glon_alt  = data_psrcand.field('GLON')
ts_alt  = data_psrcand.field('TS')
tscurv_alt  = data_psrcand.field('TS_curv')
name_alt  = data_psrcand.field('Source_Name')
poserr68_alt  = data_psrcand.field('r68')
poserr95_alt  = data_psrcand.field('r95')
index_alt  = data_psrcand.field('Spectral_Index')
indexerr_alt  = data_psrcand.field('Unc_Spectral_Index')
cutoff_alt  = data_psrcand.field('Cutoff')
cutofferr_alt  = data_psrcand.field('Unc_Cutoff')

tscomb_off = []
tscomb_alt = []
tscurvcomb_off = []
tscurvcomb_alt = []
names_off = []
names_alt = []
glat = []
nmissed_off = 0
nmissed_alt = 0

for t in range(len(glat_off)):
    distance = np.power( np.power(glat_off[t]-glat_alt,2.) + np.power(glon_off[t]-glon_alt,2.), 0.5)
    distance_sort = sorted(distance)
    indeces = [i[0] for i in sorted(enumerate(distance), key=lambda x:x[1])]
    if distance_sort[0]< np.sqrt( np.power(poserr95_off[t],2.) + np.power(poserr95_alt[indeces[0]],2.)):
        tscomb_alt.append(ts_alt[indeces[0]])
        tscurvcomb_alt.append(tscurv_alt[indeces[0]])
        tscomb_off.append(ts_off[t])
        tscurvcomb_off.append(tscurv_off[t])
        names_off.append(name_off[t])
        names_alt.append(name_alt[indeces[0]])
        glat.append(glat_off[t])
    else:
        tscomb_alt.append(0.005)
        tscurvcomb_alt.append(0.005)
        tscomb_off.append(ts_off[t])
        tscurvcomb_off.append(tscurv_off[t])
        names_off.append(name_off[t])
        names_alt.append('None')        
        glat.append(glat_off[t])
        nmissed_off += 1

for t in range(len(glat_alt)):
    distance = np.power( np.power(glat_alt[t]-glat_off,2.) + np.power(glon_alt[t]-glon_off,2.), 0.5)
    distance_sort = sorted(distance)
    indeces = [i[0] for i in sorted(enumerate(distance), key=lambda x:x[1])]
    if distance_sort[0]< np.sqrt( np.power(poserr95_alt[t],2.) + np.power(poserr95_off[indeces[0]],2.)):
        pass
    else:
        tscomb_off.append(0.005)
        tscurvcomb_off.append(0.005)
        tscomb_alt.append(ts_alt[t])
        tscurvcomb_alt.append(tscurv_alt[t])
        names_alt.append(name_alt[t])
        names_off.append('None')        
        glat.append(glat_alt[t])
        nmissed_off += 1
        

outdict = {'tscomb_off':tscomb_off,
           'tscomb_alt':tscomb_alt,
           'tscurvcomb_off':tscurvcomb_off,
           'tscurvcomb_alt':tscurvcomb_alt,
           'names_off':names_off,
           'names_alt':names_alt,
           'glat':glat}


write_yaml(outdict,os.path.join(datadir,'ts_dict_95.yaml'))
