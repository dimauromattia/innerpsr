"""Make plots of the number of PSR candidates """

#---------------------------------------
# THIS IS FOR THE DIST>2DEG FROM GC
#---------------------------------------
import os.path

import matplotlib.pyplot as pl
import numpy as np

from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_NSRC_PER_DEG, LABEL_NSRC_PER_DEG2, LABEL_DIST

init_style()
plotdir = get_plot_dir()
datadir = get_data_dir()

Nhisto_dbb = np.array(
    [0, 15, 18, 2, 4, 15, 29, 41, 62, 55, 44, 29, 9, 7, 2])
Nhisto_db = np.array(
    [0, 0, 0, 2, 3, 6, 14, 37, 44, 37, 26, 17, 13, 5, 2])
Nhisto_bdm = np.array(
    [0, 0, 0, 0, 2, 5, 14, 38, 54, 37, 33, 13, 11, 5, 2])
fluxval = np.array([1.41253754e-11, 2.81838293e-11, 5.62341325e-11,
                    1.12201845e-10, 2.23872114e-10, 4.46683592e-10,
                    8.91250938e-10, 1.77827941e-09, 3.54813389e-09,
                    7.07945784e-09, 1.41253754e-08, 2.81838293e-08,
                    5.62341325e-08, 1.12201845e-07, 2.23872114e-07])

histo_real_min = np.array([0., 0., 0., 0.,
                           0., 4.35424869, 10.25834261, 34.59687576,
                           67.28220211, 46.65153077, 45.71989011, 21.80384758,
                           2.76393202, 2., 0.])
histo_real_max = np.array([0., 0., 0., 0.,
                           2., 10.82842712, 30., 50.63324958,
                           92.11043358, 67.74596669, 63.48331477, 35.47722558,
                           7.23606798, 7.23606798, 3.41421356])

distval = np.array([0.75, 2.25, 3.75, 5.25, 6.75, 8.25, 9.75, 11.25,
                    12.75])

edges = np.array([0.00, 1.50, 3.00, 4.50, 6.00, 7.50, 9.00, 10.50, 12.00, 13.50])
areas = np.pi*edges*edges
areas_rings = areas[1:] - areas[0:-1]

histo_real_min_2 = np.array([12., 10.25834261, 13.75735931, 15.52786405,
                             15.52786405, 20.90098049, 16.41742431, 23.61483519, 19.10102051])
histo_real_max_2 = np.array([23.35889894, 21.12310563, 23.35889894, 26.69041576,
                             25.58257569, 34.38516481, 34.38516481, 35.47722558, 31.09901951])
histo_dbb = np.array([11, 15, 18, 21, 25, 25, 21, 25, 26])
histo_db = np.array([3, 9, 10, 10, 8, 15, 10, 18, 13])
histo_dbm = np.array([19, 17, 11, 11, 11, 14, 10, 24, 15])
distdelta = np.array(
    [0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75])

def plot_nsource_comparison():
    """Make plot of the number of PSR candidates as a function of flux """

    fig = create_figure()
    pl.fill_between(fluxval, histo_real_min, histo_real_max,
                    color="#A9A9A9", alpha=0.5, label='Data')
    #pl.plot(fluxval, -histo_real_min, color="#A9A9A9",
    #        alpha=0.5, lw=5.0, label='Real (Off.+Alt.)')
    #pl.errorbar(fluxval*1.03, histooff[0],yerr = np.sqrt(histooff[0]),
    #            lw=2.0, ms=5, fmt="o", color="black",label="Official IEM")
    #pl.errorbar(fluxval*0.97, historef[0],yerr = np.sqrt(historef[0]),
    #            lw=2.0, ms=5, fmt="o", color="brown",label="Alternate IEM")
    pl.errorbar(fluxval * 1.06, Nhisto_dbb, yerr=np.sqrt(Nhisto_dbb),
                fmt="*", lw=2.0, color="blue", label="Disk+Inner PSR and Blazars")
    pl.errorbar(fluxval * 0.94, Nhisto_db, yerr=np.sqrt(Nhisto_db),
                fmt=">", lw=2.0, color="red", label="Disk PSR and Blazars")
    pl.errorbar(fluxval * 0.90, Nhisto_bdm, yerr=np.sqrt(Nhisto_bdm),
                fmt="<", lw=2.0, color="green", label="Disk PSR, Blazars and DM")
    #pl.errorbar(binvec, histosim_psrb[0], yerr = np.sqrt(histosim_psrb[0]),
    #            fmt="o", color="red",label="Sim PSR bulge")
    #pl.errorbar(0.99*binvec, histodet_psrb[0], yerr = np.sqrt(histodet_psrb[0]),
    #            fmt="*", color="red",label="Detected PSR bulge")
    #pl.errorbar(1.01*binvec, histodetnoconf_psrb[0], yerr = np.sqrt(histodetnoconf_psrb[0]),
    #            fmt=">", color="red",label="Detected PSR bulge (no source confusion)")
    pl.legend(loc=1, prop={'size': 14}, numpoints=1, scatterpoints=1)
    #pl.title(r'$N(S)$ for real sky and simulations using sources with $TS>25$', fontsize=18)
    pl.ylabel(r'${\rm Number\ of\ sources\ per\ bin}$', fontsize=22)
    pl.xlabel(r'$S$ $[{\rm ph\ }{\rm cm}^{-2}{\rm s}^{-1}]$', fontsize=22)
    pl.axis([1e-10, 5e-7, 0, 100], fontsize=22)
    pl.grid(True)
    pl.yscale('linear')
    pl.xscale('log')
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    fig.tight_layout(pad=0.5)
    pl.savefig(os.path.join(plotdir, "NS_comaparison_paper.pdf"))
    pl.savefig(os.path.join(plotdir, "NS_comaparison_paper.png"))
    pl.close(fig)


def plot_nsource_comparison_dd():
    """Make plot of the number of PSR candidates as a function of dist to GC """

    fig = create_figure()
    pl.ylabel(LABEL_NSRC_PER_DEG2, fontsize=22)
    pl.xlabel(LABEL_DIST, fontsize=22)
    pl.axis([0., 14., 0., 40.], fontsize=22)
    pl.grid(True)
    pl.yscale('linear')
    pl.xscale('linear')
    pl.fill_between(distval - 0.05, histo_real_min_2, histo_real_max_2,
                    color="#A9A9A9", alpha=0.5, label='Data')
    pl.errorbar(distval - 0.10, histo_dbb, yerr=np.power(histo_dbb, 0.5),
                xerr=distdelta, fmt="*", lw=2.0, color="blue", label="Sim Disk+Bulge+Blazars")
    #pl.plot(distval - 0.05, -histo_real_min_2, color="#A9A9A9",
    #        alpha=0.5, lw=5.0, label='Real (Off.+Alt.)')
    #pl.errorbar(distval+0.12, histo_off[0], yerr = np.power(histo_off[0],0.5),
    #            xerr=distdelta, fmt="o",lw=2.0, color="black",label="Official IEM")
    #pl.errorbar(distval+0.08, histo_ref[0], yerr = np.power(histo_ref[0],0.5),
    #            xerr=distdelta, fmt="o",lw=2.0, color="brown",label="Alternate IEM.")
    pl.errorbar(distval + 0.05, histo_db, yerr=np.power(histo_db, 0.5),
                xerr=distdelta, fmt=">", lw=2.0, color="red", label="Sim Disk+Blazars")
    pl.errorbar(distval + 0.10, histo_dbm, yerr=np.power(histo_dbm, 0.5),
                xerr=distdelta, fmt=">", lw=2.0, color="green", label="Sim Disk+Blazars+DM")
    pl.legend(loc='best', prop={'size': 14}, numpoints=1, scatterpoints=1)
    #pl.title(r'$dN/dS$ for real sky and simulations ($TS>25$)', fontsize=22)
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    fig.tight_layout(pad=0.5)
    pl.savefig(os.path.join(plotdir, "dNdd_comaparison_paper.pdf"))
    pl.savefig(os.path.join(plotdir, "dNdd_comaparison_paper.png"))
    pl.close(fig)

def plot_nsource_comparison_ddoverr2():
    """Make plot of the number of PSR candidates as a function of dist to GC """

    fig = create_figure()
    pl.ylabel(LABEL_NSRC_PER_DEG2, fontsize=22)
    pl.xlabel(LABEL_DIST, fontsize=22)
    pl.axis([0., 14., 0.05, 5.], fontsize=22)
    pl.grid(True)
    pl.yscale('log')
    pl.xscale('linear')
    pl.fill_between(distval - 0.05, histo_real_min_2/areas_rings, histo_real_max_2/areas_rings,
                    color="#A9A9A9", alpha=0.5, label='Data')
    pl.errorbar(distval - 0.10, histo_dbb/areas_rings, yerr=np.power(histo_dbb, 0.5)/areas_rings,
                xerr=distdelta, fmt="*", lw=2.0, color="blue", label="Sim Disk+Bulge+Blazars")

    #pl.fill_between(distval - 0.05, histo_real_min_2/np.power(distval,2.), histo_real_max_2/np.power(distval,2.),
    #                color="#A9A9A9", alpha=0.5, label='Data')
    #pl.errorbar(distval - 0.10, histo_dbb/np.power(distval,2.), yerr=np.power(histo_dbb, 0.5)/np.power(distval,2.),
    #            xerr=distdelta, fmt="*", lw=2.0, color="blue", label="Sim Disk+Bulge+Blazars")
    #pl.plot(distval - 0.05, -histo_real_min_2, color="#A9A9A9",
    #        alpha=0.5, lw=5.0, label='Real (Off.+Alt.)')
    #pl.errorbar(distval+0.12, histo_off[0], yerr = np.power(histo_off[0],0.5),
    #            xerr=distdelta, fmt="o",lw=2.0, color="black",label="Official IEM")
    #pl.errorbar(distval+0.08, histo_ref[0], yerr = np.power(histo_ref[0],0.5),
    #            xerr=distdelta, fmt="o",lw=2.0, color="brown",label="Alternate IEM.")
    #pl.errorbar(distval + 0.05, histo_db/np.power(distval,2.), yerr=np.power(histo_db, 0.5)/np.power(distval,2.),
    #            xerr=distdelta, fmt=">", lw=2.0, color="red", label="Sim Disk+Blazars")
    #pl.errorbar(distval + 0.10, histo_dbm/np.power(distval,2.), yerr=np.power(histo_dbm, 0.5)/np.power(distval,2.),
    #            xerr=distdelta, fmt=">", lw=2.0, color="green", label="Sim Disk+Blazars+DM")
    #pl.legend(loc='best', prop={'size': 14}, numpoints=1, scatterpoints=1)
    pl.errorbar(distval + 0.05, histo_db/areas_rings, yerr=np.power(histo_db, 0.5)/areas_rings,
                xerr=distdelta, fmt=">", lw=2.0, color="red", label="Sim Disk+Blazars")
    pl.errorbar(distval + 0.10, histo_dbm/areas_rings, yerr=np.power(histo_dbm, 0.5)/areas_rings,
                xerr=distdelta, fmt=">", lw=2.0, color="green", label="Sim Disk+Blazars+DM")
    pl.legend(loc='best', prop={'size': 14}, numpoints=1, scatterpoints=1)
    #pl.title(r'$dN/dS$ for real sky and simulations ($TS>25$)', fontsize=22)
    pl.xticks(fontsize=22)
    pl.yticks(fontsize=22)
    fig.tight_layout(pad=0.5)
    pl.savefig(os.path.join(plotdir, "dNddoverr2_comaparison_paper.pdf"))
    pl.savefig(os.path.join(plotdir, "dNddoverr2_comaparison_paper.png"))
    pl.close(fig)


plot_nsource_comparison()
#plot_nsource_comparison_dd()
plot_nsource_comparison_ddoverr2()
