"""Make plots showing the source localization procedure"""
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patheffects as PathEffects
from matplotlib.patches import Ellipse
from astropy.wcs import WCS
from astropy.io import fits
from astropy.table import Table
from astropy.coordinates import SkyCoord
from common import get_plot_dir, get_data_dir, init_style, create_figure
from labels import LABEL_GLON, LABEL_GLAT

def plot_error_ellipse(fit, xy, cdelt, **kwargs):
    """Add an error ellipse to a countmap image"""
    ax = kwargs.pop('ax', plt.gca())
    color = kwargs.pop('color', 'k')
    colname = kwargs.pop('colname', 'r68')
    sigma = np.abs(np.array(fit['sigma']))
    sigmax = np.abs(np.array(fit['sigma_semimajor']))
    sigmay = np.abs(np.array(fit['sigma_semiminor']))
    theta = fit['theta']
    radius = np.array(fit[colname])

    width = 2.0 * sigmax / np.abs(cdelt[0]) * radius / sigma
    height = 2.0 * sigmay / np.abs(cdelt[1]) * radius / sigma

    e0 = Ellipse(xy=(float(xy[0]), float(xy[1])),
                 width=width, height=height,
                 angle=-np.degrees(theta),
                 facecolor='None', edgecolor=color, **kwargs)

    ax.add_artist(e0)


def make_localization_plot():
    """Make plots showing the source localization procedure"""

    plotdir = get_plot_dir()
    datadir = get_data_dir()

    init_style()

    fig = create_figure(figsize=(8,6))
    fig.subplots_adjust(bottom=0.12, left=0.12, right=0.95, top=0.95)
    

    hdulist = fits.open(os.path.join(datadir, '3fgl_j1709.5-0335_loc.fits'))
    tab0 = Table.read(os.path.join(datadir, '3fgl_j1709.5-0335_loc.fits'))
    tab1 = Table.read(os.path.join(datadir, 'fitrel.fits'))
 
    wcs = WCS(hdulist['TSMAP'].header)
    ax = fig.add_subplot(111, projection=wcs)
    
    data = hdulist['TSMAP'].data[:15,:15]
    data -= np.max(data)

    im = ax.imshow(data, interpolation='bicubic',vmax=0.0,vmin=-25.0, origin='lower')
    cb = plt.colorbar(im)
    #cb.set_label('2$\\times\Delta\ln$L')
    cb.set_label('$\Delta$ TS')

    cs = ax.contour(data, levels=[-9.21, -5.99, -2.3],
                    colors=['k'],linewidths=[1.0],linestyles=['-'])
    #cs.levels = ['%.1f' % val for val in cs.levels]
    cs.levels = ['99\%','95\%','68\%',]
    plt.clabel(cs, inline=1, fontsize=8)

 
    ax.coords.grid(color='w', linestyle=':', linewidth=0.5)
    ax.set_xlabel(LABEL_GLON)
    ax.set_ylabel(LABEL_GLAT)

    m = tab1['name'] == '3FGL J1709.5-0335'

    color = 'w'
    path_effect = PathEffects.withStroke(linewidth=2.0, foreground="black")

    # Marker for 3FGL position
    skydir0 = SkyCoord(17.334425, 20.697418, unit='deg', frame='galactic')
    pix0 = skydir0.to_pixel(wcs)
    p = ax.plot(17.334425, 20.697418, transform=ax.get_transform('world'),
                marker='+', color=color, linestyle='None', markersize=8)
    plt.setp(p, path_effects=[path_effect])

    # Marker for localized position
    skydir1 = SkyCoord(tab0['glon'], tab0['glat'], unit='deg', frame='galactic')
    pix1 = skydir1.to_pixel(wcs)
    p = ax.plot(tab0['glon'], tab0['glat'], transform=ax.get_transform('world'),
                marker='x', color='black', linestyle='None', markersize=8)
    plt.setp(p, path_effects=[path_effect])

    t = ax.annotate('3FGL J1709.5-0335', xy=pix0,
                    xytext=(5.0, 5.0), textcoords='offset points',
                    color=color, size=12)
    plt.setp(t, path_effects=[path_effect])

    # plot_error_ellipse(tab0,pix1,wcs.wcs.cdelt,colname='r68',color='k')
    # plot_error_ellipse(tab0,pix1,wcs.wcs.cdelt,colname='r95',color='k')
    # plot_error_ellipse(tab0,pix1,wcs.wcs.cdelt,colname='r99',color='k')

    # plt.gca().set_xlim(0.,15.0)
    # plt.gca().set_ylim(0.,15.0)

    #fig.savefig(os.path.join(plotdir,'localization.png'), bbox_inches='tight')
    #fig.savefig(os.path.join(plotdir,'localization.pdf'), bbox_inches='tight')
    fig.savefig(os.path.join(plotdir, 'localization.pdf'))
    fig.savefig(os.path.join(plotdir, 'localization.png'))
    plt.close(fig)


make_localization_plot()
